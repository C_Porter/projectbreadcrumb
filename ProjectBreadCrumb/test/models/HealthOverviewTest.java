/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package models;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author qrk13cdu
 */
public class HealthOverviewTest {
    
    public HealthOverviewTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of poundsToKgs method, of class HealthOverview.
     */
    @Test
    public void testPoundsToKgs() {
        System.out.println("poundsToKgs");
        HealthOverview instance = new HealthOverview(187,88,"metric");
        double expResult = 88;
        double result = instance.poundsToKgs();
        assertEquals(expResult, result, 0.0);
         HealthOverview instance2= new HealthOverview(187,194,"imperial");
        expResult = 88;
        result = instance2.poundsToKgs();
        assertEquals(expResult, result, 0.5);       
    }

    /**
     * Test of calcBMI method, of class HealthOverview.
     */
    @Test
    public void testCalcBMI() {
        System.out.println("calcBMI");
        HealthOverview instance = new HealthOverview(187,88,"metric");
        instance.calcBMI();
        assertEquals(instance.getBmi(),25.1,0.5);
        
        HealthOverview instance2= new HealthOverview(72,182,"imperial");
        instance2.calcBMI();
        assertEquals(instance2.getBmi(), 24.6, 0.5);       
    }


}
