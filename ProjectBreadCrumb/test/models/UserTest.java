/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import db.DatabaseAccessor;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author akc13dyu
 */
public class UserTest {

    public UserTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() throws ServletException {
        Connection connection = DatabaseAccessor.getConnection();

        try {
            PreparedStatement pstmt = connection.prepareStatement(
                    "DELETE FROM account WHERE email = 'TEST@TEST.COM' OR email ='TEST@TEST.COM2'");
            pstmt.executeUpdate();

            PreparedStatement pstmt2 = connection.prepareStatement(
                    "DELETE FROM siteuser WHERE email = 'TEST@TEST.COM'");
            pstmt2.executeUpdate();

            PreparedStatement pstmt3 = connection.prepareStatement(
                    "DELETE FROM usergroups WHERE groupName = 'testGroupName'");
            pstmt3.executeUpdate();

            PreparedStatement pstmt4 = connection.prepareStatement(
                    "DELETE FROM groupMembers WHERE userId = '-1'");
            pstmt4.executeUpdate();

            PreparedStatement pstmt5 = connection.prepareStatement(
                    "DELETE FROM healthoverview WHERE userId = '-1'");
            pstmt5.executeUpdate();

            PreparedStatement pstmt6 = connection.prepareStatement(
                    "DELETE FROM exercises WHERE userId = '-1'");
            pstmt6.executeUpdate();

            PreparedStatement pstmt7 = connection.prepareStatement(
                    "DELETE FROM groupMemberGoalWeight WHERE userId = '-1'");
            pstmt7.executeUpdate();

            connection.close();

        } catch (SQLException ex) {
            Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Before
    public void setUp() throws ServletException {
        Connection connection = DatabaseAccessor.getConnection();

        try {
            PreparedStatement pstmt = connection.prepareStatement(
                    "DELETE FROM account WHERE email = 'TEST@TEST.COM' OR email ='TEST@TEST.COM2'");
            pstmt.executeUpdate();

            PreparedStatement pstmt2 = connection.prepareStatement(
                    "DELETE FROM siteuser WHERE email = 'TEST@TEST.COM'");
            pstmt2.executeUpdate();

            PreparedStatement pstmt3 = connection.prepareStatement(
                    "DELETE FROM usergroups WHERE groupName = 'testGroupName'");
            pstmt3.executeUpdate();

            PreparedStatement pstmt4 = connection.prepareStatement(
                    "DELETE FROM groupMembers WHERE userId = '-1'");
            pstmt4.executeUpdate();

            PreparedStatement pstmt5 = connection.prepareStatement(
                    "DELETE FROM healthoverview WHERE userId = '-1'");
            pstmt5.executeUpdate();

            PreparedStatement pstmt6 = connection.prepareStatement(
                    "DELETE FROM exercises WHERE userId = '-1'");
            pstmt6.executeUpdate();

            PreparedStatement pstmt7 = connection.prepareStatement(
                    "DELETE FROM groupMemberGoalWeight WHERE userId = '-1'");
            pstmt7.executeUpdate();

            connection.close();

        } catch (SQLException ex) {
            Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of updateGoals method, of class User. This method simulates a user
     * creating an account and adding 2 exercises. It then simulates a user
     * completing these 2 goals (1 for weight, 1 for exercise). Will be
     * successful if the goals are correctly calculated as completed and flagged
     * within the database.
     */
    @Test
    public void testUpdateGoals() throws Exception {
        Connection connection = DatabaseAccessor.getConnection();
        //populate test data
        PreparedStatement pstmt = connection.prepareStatement(
                "INSERT INTO account VALUES('TEST@TEST.COM', 'TESTPW', 1)");
        pstmt.executeUpdate();
        PreparedStatement pstmt1 = connection.prepareStatement(
                "INSERT INTO siteuser VALUES('-1','TEST@TEST.COM','TESTFIRSTNAME','TESTLASTNAME','TESTBIO','TESTPICTUREPATH')");
        pstmt1.executeUpdate();

        PreparedStatement pstmt2 = connection.prepareStatement(
                "INSERT INTO exercises VALUES('-1','-1','testExName1','-13','111','201','401','testExDetails1','141','04/04/01', '1')");
        pstmt2.executeUpdate();
        PreparedStatement pstmt3 = connection.prepareStatement(
                "INSERT INTO exercises VALUES('-2','-1','testExName2','-13','112','202','402','testExDetails2','142','04/04/02', '2')");
        pstmt3.executeUpdate();

        PreparedStatement pstmt5 = connection.prepareStatement(
                "INSERT INTO usergroups VALUES('0','testGroupName','testGroupDetails','1')");
        pstmt5.executeUpdate();

        PreparedStatement pstmt4 = connection.prepareStatement(
                "INSERT INTO goals VALUES('1','-1','0','TESTGOALNAME','distance','60','10/04/20','Running','0','04/04/02', '100', '60')");
        pstmt4.executeUpdate();

        PreparedStatement pstmt8 = connection.prepareStatement(
                "INSERT INTO goals VALUES('2','-1','0','TESTGOALNAME2','weightLoss','5','10/04/20','Not selected','0','04/04/02', '100', '5')");
        pstmt8.executeUpdate();
//
//        PreparedStatement pstmt6 = connection.prepareStatement(
//                "INSERT INTO groupMembers VALUES('1','0','-1')");
//        pstmt6.executeUpdate();

        PreparedStatement pstmt9 = connection.prepareStatement(
                "INSERT INTO healthoverview VALUES('-1','-1','200','85','50','metric','0')");
        pstmt9.executeUpdate();
//
//        PreparedStatement pstmt11 = connection.prepareStatement(
//                "INSERT INTO groupMemberGoalWeight VALUES('-1','-1','2','100')");
//        pstmt11.executeUpdate();

        UserGroups testGroup = DatabaseAccessor.getUserGroupByGroupID(0);
        testGroup.populateUserGroup();
        testGroup.updateGoals();

        int resultDistanceGoalCompleted = 0;
        try {
            PreparedStatement pstmt7 = connection.prepareStatement(
                    "SELECT * FROM goals WHERE goalid = 1");
            ResultSet results = pstmt7.executeQuery();

            if (results.next()) {
                resultDistanceGoalCompleted = results.getInt("completed");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertEquals(1, resultDistanceGoalCompleted);

        int resultWeightGoalCompleted = 0;
        try {
            PreparedStatement pstmt10 = connection.prepareStatement(
                    "SELECT * FROM goals WHERE goalid = 2");
            ResultSet results = pstmt10.executeQuery();

            if (results.next()) {
                resultWeightGoalCompleted = results.getInt("completed");
            }
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, ex);
        }

        assertEquals(1, resultDistanceGoalCompleted);
        assertEquals(1, resultWeightGoalCompleted);

        connection.close();
    }
   

}
