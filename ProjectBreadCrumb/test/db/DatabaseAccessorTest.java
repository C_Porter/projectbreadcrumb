/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import models.Account;
import models.Consumable;
import models.Exercise;
import models.Goal;
import models.HealthOverview;
import models.Notification;
import models.User;
import models.UserGroups;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author agf13qmu
 */
public class DatabaseAccessorTest {

    public DatabaseAccessorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws ServletException {
        Connection connection = DatabaseAccessor.getConnection();

        try {
            PreparedStatement pstmt = connection.prepareStatement(
                    "DELETE FROM account WHERE email = 'TEST@TEST.COM' OR email ='TEST@TEST.COM2'");
            pstmt.executeUpdate();

            PreparedStatement pstmt1 = connection.prepareStatement(
                    "DELETE FROM account WHERE email = 'AccountPersistEmail'");
            pstmt1.executeUpdate();

            PreparedStatement pstmt2 = connection.prepareStatement(
                    "DELETE FROM siteuser WHERE email = 'AccountPersistEmail'");
            pstmt2.executeUpdate();

            PreparedStatement pstmt3 = connection.prepareStatement(
                    "DELETE FROM usergroups WHERE groupName = 'testGroupName'");
            pstmt3.executeUpdate();
            
            PreparedStatement pstmt4 = connection.prepareStatement(
                    "DELETE FROM groupMembers WHERE userId = '-1'");
            pstmt4.executeUpdate();
            
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getConnection method, of class DatabaseAccessor. Counts the
     * predefined exercise list and confirms the correct list size. This
     * confirms database connection is present.
     */
    @Test
    public void testGetConnection() throws Exception {
        System.out.println("getConnection");
        Connection connection = DatabaseAccessor.getConnection();
        PreparedStatement pstmt = connection.prepareStatement(
                "Select count(exname) as listcount from exerciselist");
        ResultSet results = pstmt.executeQuery();

        int resultOf15 = 0;
        if (results.next()) {
            resultOf15 = results.getInt("listcount");
        }

        int expResult = 15;
        int result = resultOf15;
        assertEquals(expResult, result);
    }

    /**
     * Test of getAccountByEmail method, of class DatabaseAccessor.
     *
     * @throws java.lang.Exception Attempts to retrieve account test data from
     * the database
     */
    @Test
    public void testGetAccountByEmail() throws Exception {

        Connection connection = DatabaseAccessor.getConnection();
        PreparedStatement pstmt = connection.prepareStatement(
                "INSERT INTO account VALUES('TEST@TEST.COM', 'TESTPW', 1)");
        pstmt.executeUpdate();
        PreparedStatement pstmt1 = connection.prepareStatement(
                "INSERT INTO siteuser VALUES('-1','TEST@TEST.COM','TESTFIRSTNAME','TESTLASTNAME','TESTBIO','TESTPICTUREPATH')");
        pstmt1.executeUpdate();
        connection.close();

        String testEmail = "TEST@TEST.COM";
        Account result = DatabaseAccessor.getAccountByEmail(testEmail);
        assertEquals("TEST@TEST.COM", result.getEmail());
        assertEquals(true, result.getAdmin());
        assertEquals("TESTPW", result.getPassword());
    }

    /**
     * Test of isAccountEmailUsed method, of class DatabaseAccessor. Attempts to
     * find two email's in the database. The first being present and returning
     * true, the second not being present and returning false.
     */
    @Test
    public void testIsAccountEmailUsed() throws Exception {
        System.out.println("isAccountEmailUsed");
        String email = "TEST@TEST.COM";
        //insert data into database for true test
        Connection connection = DatabaseAccessor.getConnection();
        PreparedStatement pstmt = connection.prepareStatement(
                "INSERT INTO account VALUES('TEST@TEST.COM', 'TESTPW', 1)");
        pstmt.executeUpdate();
        PreparedStatement pstmt1 = connection.prepareStatement(
                "INSERT INTO siteuser VALUES('-1','TEST@TEST.COM','TESTFIRSTNAME','TESTLASTNAME','TESTBIO','TESTPICTUREPATH')");
        pstmt1.executeUpdate();
        boolean expResult = true;
        boolean result = DatabaseAccessor.isAccountEmailUsed(email);
        assertEquals(expResult, result);

        //delete data and test for false value
        PreparedStatement pstmt2 = connection.prepareStatement(
                "DELETE FROM siteuser WHERE email = 'TEST@TEST.COM'");
        pstmt2.executeUpdate();
        connection.close();
        expResult = false;
        result = DatabaseAccessor.isAccountEmailUsed(email);
        assertEquals(expResult, result);
    }

    /**
     * Test of persistAccount method, of class DatabaseAccessor. Persist an
     * account, then pull the data from the database with the persisted email
     * address. Pulled account object should equal the persisted object.
     *
     */
    @Test
    public void testPersistAccount() throws Exception {
        System.out.println("persistAccount");
        Account ExpResultAccount = new Account("AccountPersistEmail", "AccountPersistPassword", 0, "AccountPersistFirstname", "AccountPersistLastname");
        DatabaseAccessor.persistAccount(ExpResultAccount);
        Account result = new Account();

        Connection connection = DatabaseAccessor.getConnection();
        try {
            PreparedStatement pstmt = connection.prepareStatement(
                    "SELECT * FROM account WHERE email = ?");
            pstmt.setString(1, "AccountPersistEmail");
            ResultSet results = pstmt.executeQuery();

            PreparedStatement pstmt1 = connection.prepareStatement(
                    "SELECT * FROM siteuser WHERE email = ?");
            pstmt1.setString(1, "AccountPersistEmail");
            ResultSet results1 = pstmt1.executeQuery();

            if (results.next() && results1.next()) {
                result = new Account(
                        results.getString("email"),
                        results.getString("password"),
                        results.getInt("isadmin"),
                        results1.getString("firstName"),
                        results1.getString("surname"));
            }
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertEquals(result.getEmail(), ExpResultAccount.getEmail());
        assertEquals(result.getPassword(), ExpResultAccount.getPassword());
        assertEquals(result.getAdmin(), ExpResultAccount.getAdmin());
        assertEquals(result.getUser().getFirstName(), ExpResultAccount.getUser().getFirstName());
        assertEquals(result.getUser().getLastName(), ExpResultAccount.getUser().getLastName());
    }

    /**
     * Test of getUserByEmail method, of class DatabaseAccessor. Pulls test user
     * from the database and compares expected results with pulled results.
     * Should be equal.
     */
    @Test
    public void testGetUserByEmail() throws Exception {

        Connection connection = DatabaseAccessor.getConnection();
        PreparedStatement pstmt = connection.prepareStatement(
                "INSERT INTO account VALUES('TEST@TEST.COM', 'TESTPW', 1)");
        pstmt.executeUpdate();
        PreparedStatement pstmt1 = connection.prepareStatement(
                "INSERT INTO siteuser VALUES('-1','TEST@TEST.COM','TESTFIRSTNAME','TESTLASTNAME','TESTBIO','TESTPICTUREPATH')");
        pstmt1.executeUpdate();
        connection.close();

        String inputEmail = "TEST@TEST.COM";
        User expResult = new User(-1, "TEST@TEST.COM", "TESTFIRSTNAME", "TESTLASTNAME", "TESTBIO");
        User result = DatabaseAccessor.getUserByEmail(inputEmail);
        assertEquals(expResult.getID(), result.getID());
        assertEquals(expResult.getEmail(), result.getEmail());
        assertEquals(expResult.getFirstName(), result.getFirstName());
        assertEquals(expResult.getLastName(), result.getLastName());
        assertEquals(expResult.getBio(), result.getBio());
    }

    /**
     * Test of persistUser method, of class DatabaseAccessor.
     *
     * @throws java.lang.Exception Persist a user, then pull the data from the
     * database with the persisted email address. Pulled user object should
     * equal the persisted object.
     */
    @Test
    public void testPersistUser() throws Exception {
        System.out.println("persistUser");
        Connection connection = DatabaseAccessor.getConnection();
        //
        PreparedStatement pstmt = connection.prepareStatement(
                "INSERT INTO account VALUES('TEST@TEST.COM', 'TESTPW', 1)");
        pstmt.executeUpdate();

        User expResultUser = new User(-2, "TEST@TEST.COM", "userPersistFirstname", "userPersistLastname", "userPersistBio");
        User resultUser = new User();
        DatabaseAccessor.persistUser(expResultUser);

        try {
            PreparedStatement pstmt2 = connection.prepareStatement(
                    "SELECT * FROM siteuser WHERE email = ?");
            pstmt2.setString(1, "TEST@TEST.COM");
            ResultSet results = pstmt2.executeQuery();

            if (results.next()) {
                resultUser = new User(
                        results.getInt("userid"),
                        results.getString("email"),
                        results.getString("firstName"),
                        results.getString("surname"),
                        results.getString("bio"));
            }
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, ex);
        }

        assertEquals(expResultUser.getEmail(), resultUser.getEmail());
        assertEquals(expResultUser.getFirstName(), resultUser.getFirstName());
        assertEquals(expResultUser.getLastName(), resultUser.getLastName());
        assertEquals(expResultUser.getBio(), resultUser.getBio());
    }

    /**
     * Test of getHOByUserID method, of class DatabaseAccessor. *
     */
    @Test
    public void testGetHOByUserID() throws Exception {
        System.out.println("getHOByUserID");
        Connection connection = DatabaseAccessor.getConnection();
        //populate test data
        PreparedStatement pstmt = connection.prepareStatement(
                "INSERT INTO account VALUES('TEST@TEST.COM', 'TESTPW', 1)");
        pstmt.executeUpdate();
        PreparedStatement pstmt1 = connection.prepareStatement(
                "INSERT INTO siteuser VALUES('-1','TEST@TEST.COM','TESTFIRSTNAME','TESTLASTNAME','TESTBIO','TESTPICTUREPATH')");
        pstmt1.executeUpdate();
        PreparedStatement pstmt2 = connection.prepareStatement(
                "INSERT INTO healthoverview VALUES('-1','-1','200','100','50','imperial','1')");
        pstmt2.executeUpdate();
        connection.close();
        //expected HO result
        HealthOverview expResult = new HealthOverview(-1, -1, 200, 100, 50, "imperial", 1);
        //pulled result to test
        HealthOverview result = DatabaseAccessor.getHOByUserID(-1);

        assertEquals(expResult.getHOid(), result.getHOid());
        assertEquals(expResult.getUserID(), result.getUserID());
        assertEquals(expResult.getCurrentWeight(), result.getCurrentWeight(), 0);
        assertEquals(expResult.getHeight(), result.getHeight(), 0);
        assertEquals(expResult.getBmi(), result.getBmi(), 0);
        assertEquals(expResult.getConversion(), result.getConversion());
        assertEquals(expResult.getWeightChecked(), result.getWeightChecked());
    }

    /**
     * Test of persistHO method, of class DatabaseAccessor. Persist test health
     * overview for a user and compare persisted values to correct values.
     */
    @Test
    public void testPersistHO() throws Exception {
        System.out.println("persistHO");
        Connection connection = DatabaseAccessor.getConnection();
        //populate test data
        PreparedStatement pstmt = connection.prepareStatement(
                "INSERT INTO account VALUES('TEST@TEST.COM', 'TESTPW', 1)");
        pstmt.executeUpdate();
        PreparedStatement pstmt1 = connection.prepareStatement(
                "INSERT INTO siteuser VALUES('-1','TEST@TEST.COM','TESTFIRSTNAME','TESTLASTNAME','TESTBIO','TESTPICTUREPATH')");
        pstmt1.executeUpdate();
        //pull persisted user from the database to retrieve the userId
        User testUser = new User();
        try {
            PreparedStatement pstmt2 = connection.prepareStatement(
                    "SELECT * FROM siteuser WHERE email = ?");
            pstmt2.setString(1, "TEST@TEST.COM");
            ResultSet results = pstmt2.executeQuery();

            if (results.next()) {
                testUser = new User(
                        results.getInt("userid"),
                        results.getString("email"),
                        results.getString("firstName"),
                        results.getString("surname"),
                        results.getString("bio"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, ex);
        }
        //Create the health overview object to persist
        HealthOverview expHO = new HealthOverview(-1, -1, 200, 100, 50, "imperial", 1);
        //assign the health overview to the user
        testUser.setHO(expHO);
        //test persist    
        DatabaseAccessor.persistHO(testUser);

        HealthOverview resultHO = new HealthOverview();
        try {
            PreparedStatement pstmt3 = connection.prepareStatement(
                    "SELECT * FROM healthoverview WHERE userid = ?");
            pstmt3.setInt(1, testUser.getID());
            ResultSet results = pstmt3.executeQuery();

            if (results.next()) {
                resultHO = new HealthOverview(
                        results.getInt("healthOverviewId"),
                        results.getInt("userId"),
                        results.getFloat("height"),
                        results.getFloat("weight"),
                        results.getFloat("BMI"),
                        results.getString("conversion"),
                        results.getInt("weightChecked"));
            }
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, ex);
        }

        assertEquals(testUser.getID(), resultHO.getUserID());
        assertEquals(testUser.getHO().getHeight(), resultHO.getHeight(), 0.0);
        assertEquals(testUser.getHO().getCurrentWeight(), resultHO.getCurrentWeight(), 0.0);
        assertEquals(testUser.getHO().getBmi(), resultHO.getBmi(), 0.0);
        assertEquals(testUser.getHO().getConversion(), resultHO.getConversion());
        assertEquals(testUser.getHO().getWeightChecked(), resultHO.getWeightChecked());
    }

    /**
     * Test of persistExercise method, of class DatabaseAccessor. Tests
     * persisting of an exercise to the database.
     */
    @Test
    public void testPersistExercise() throws Exception {
        System.out.println("persistExercise");
        Connection connection = DatabaseAccessor.getConnection();
        //populate test data
        PreparedStatement pstmt = connection.prepareStatement(
                "INSERT INTO account VALUES('TEST@TEST.COM', 'TESTPW', 1)");
        pstmt.executeUpdate();
        PreparedStatement pstmt1 = connection.prepareStatement(
                "INSERT INTO siteuser VALUES('-1','TEST@TEST.COM','TESTFIRSTNAME','TESTLASTNAME','TESTBIO','TESTPICTUREPATH')");
        pstmt1.executeUpdate();
        Exercise expEx = new Exercise("testExercise", "Running", "testDetails", 100.5f, 200.5f, 1);
        DatabaseAccessor.persistExercise(-1, expEx);
        //pull persisted data out to be checked
        Exercise resultEx = new Exercise();
        try {
            PreparedStatement pstmt3 = connection.prepareStatement(
                    "SELECT * FROM exercises WHERE userid = ?");
            pstmt3.setInt(1, -1);
            ResultSet results = pstmt3.executeQuery();

            if (results.next()) {
                resultEx = new Exercise(
                        results.getString("exName"),
                        results.getString("exType"),
                        results.getString("exDetails"),
                        results.getFloat("distance"),
                        results.getFloat("completionTime"),
                        results.getInt("checked"));
            }
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, ex);
        }        
        assertEquals(expEx.getName(), expEx.getName());
        assertEquals(expEx.getType(), expEx.getType());
        assertEquals(expEx.getDetails(), expEx.getDetails());
        assertEquals(expEx.getDistance(), expEx.getDistance(), 0);
        assertEquals(expEx.getTime(), expEx.getTime(), 0);
        assertEquals(expEx.getChecked(), expEx.getChecked());   
    }


    /**
     * Test of getExerciseList method, of class DatabaseAccessor.
     * Tests pulling multiple exercises from the database and storing them
     * in an ArrayList.
     */
    @Test
    public void testGetExerciseList() throws Exception {
        System.out.println("getExerciseList");
        Connection connection = DatabaseAccessor.getConnection();
        //populate test data
        PreparedStatement pstmt = connection.prepareStatement(
                "INSERT INTO account VALUES('TEST@TEST.COM', 'TESTPW', 1)");
        pstmt.executeUpdate();
        PreparedStatement pstmt1 = connection.prepareStatement(
                "INSERT INTO siteuser VALUES('-1','TEST@TEST.COM','TESTFIRSTNAME','TESTLASTNAME','TESTBIO','TESTPICTUREPATH')");
        pstmt1.executeUpdate();
        
        PreparedStatement pstmt2 = connection.prepareStatement(
                "INSERT INTO exercises VALUES('-1','-1','testExName1','-3','111','201','401','testExDetails1','141','04/04/01', '1')");
        pstmt2.executeUpdate();
        PreparedStatement pstmt3 = connection.prepareStatement(
                "INSERT INTO exercises VALUES('-2','-1','testExName2','-4','112','202','402','testExDetails2','142','04/04/02', '2')");
        pstmt3.executeUpdate();
        connection.close();
        int userId = -1;
                       
        ArrayList<Exercise> result = DatabaseAccessor.getExerciseList(userId);        
        
        assertEquals(-1, result.get(0).getExId());
        assertEquals("testExName1", result.get(0).getName());
        assertEquals("Weight Lifting vigorous", result.get(0).getType());
        assertEquals(111, result.get(0).getDistance(), 0);
        assertEquals(201, result.get(0).getTime(), 0);
        assertEquals("testExDetails1", result.get(0).getDetails());
        assertEquals(1, result.get(0).getChecked());
        
        assertEquals(-2, result.get(1).getExId());
        assertEquals("testExName2", result.get(1).getName());
        assertEquals("Weight Lifting light", result.get(1).getType());
        assertEquals(112, result.get(1).getDistance(), 1);
        assertEquals(202, result.get(1).getTime(), 1);
        assertEquals("testExDetails2", result.get(1).getDetails());
        assertEquals(2, result.get(1).getChecked());
    }
   

    /**
     * Test of getUserGoalsList method, of class DatabaseAccessor.
     * Persists 2 goals to the database and then check if they are correctly
     * put into an ArrayList by the method.
     */
    @Test
    public void testGetUserGoalsList() throws Exception {
        System.out.println("getUserGoalsList");
        Connection connection = DatabaseAccessor.getConnection();
        //populate test data
        PreparedStatement pstmt = connection.prepareStatement(
                "INSERT INTO account VALUES('TEST@TEST.COM', 'TESTPW', 1)");
        pstmt.executeUpdate();
        PreparedStatement pstmt1 = connection.prepareStatement(
                "INSERT INTO siteuser VALUES('-1','TEST@TEST.COM','TESTFIRSTNAME','TESTLASTNAME','TESTBIO','TESTPICTUREPATH')");
        pstmt1.executeUpdate();
        PreparedStatement pstmt4 = connection.prepareStatement(
                "INSERT INTO usergroups VALUES('0','testGroupName','testGroupDetails','1')");
        pstmt4.executeUpdate();
        PreparedStatement pstmt2 = connection.prepareStatement(
                "INSERT INTO goals VALUES('-1','-1','0','testGoal1','Duration','201','01/01/01','Running','1','04/04/01', '1', '11')");
        pstmt2.executeUpdate();
        PreparedStatement pstmt3 = connection.prepareStatement(
                "INSERT INTO goals VALUES('-2','-1','0','testGoal2','Distance','202','02/02/02','Running','2','04/04/02', '2', '22')");
        pstmt3.executeUpdate();
        connection.close();
        int userID = -1;
        
        ArrayList<Goal> result = DatabaseAccessor.getUserGoalsList(userID);
        
        assertEquals(-1, result.get(0).getGoalId());
        assertEquals("testGoal1", result.get(0).getName());
        assertEquals("Duration", result.get(0).getGoalType());
        assertEquals(201, result.get(0).getTargetVal(), 0);
        assertEquals(-13, result.get(0).getExType());
        assertEquals(1, result.get(0).getCompleted());
        assertEquals(1, result.get(0).getCurrentWeight(),0);
        assertEquals(11, result.get(0).getStartTargetVal(),0);
        
        assertEquals(-2, result.get(1).getGoalId());
        assertEquals("testGoal2", result.get(1).getName());
        assertEquals("Distance", result.get(1).getGoalType());
        assertEquals(202, result.get(1).getTargetVal(), 0);
        assertEquals(-13, result.get(1).getExType());
        assertEquals(2, result.get(1).getCompleted());
        assertEquals(2, result.get(1).getCurrentWeight(),0);
        assertEquals(22, result.get(1).getStartTargetVal(),0);
    }


    /**
     * Test of getGroupUserIds method, of class DatabaseAccessor.
     */
    @Test
    public void testGetGroupUserIds() throws Exception {
        System.out.println("getGroupUserIds");
        Connection connection = DatabaseAccessor.getConnection();
        //populate test data
        PreparedStatement pstmt = connection.prepareStatement(
                "INSERT INTO account VALUES('TEST@TEST.COM', 'TESTPW', 1)");
        pstmt.executeUpdate();
        PreparedStatement pstmt1 = connection.prepareStatement(
                "INSERT INTO siteuser VALUES('-1','TEST@TEST.COM','TESTFIRSTNAME','TESTLASTNAME','TESTBIO','TESTPICTUREPATH')");
        pstmt1.executeUpdate();
        PreparedStatement pstmt4 = connection.prepareStatement(
                "INSERT INTO usergroups VALUES('1','testGroupName','testGroupDetails','1')");
        pstmt4.executeUpdate();
        PreparedStatement pstmt5 = connection.prepareStatement(
                "INSERT INTO groupMembers VALUES('1','1','-1')");
        pstmt5.executeUpdate();
        connection.close();
                
        int groupId = 1;
        ArrayList<User> result = DatabaseAccessor.getGroupUserIds(groupId);
        assertEquals(-1, result.get(0).getID());
    }

    /**
     * Test of getUserList method, of class DatabaseAccessor.
     */
    @Test
    public void testGetUserList() throws Exception {
        System.out.println("getUserList");
        Connection connection = DatabaseAccessor.getConnection();
        PreparedStatement pstmt = connection.prepareStatement(
                "INSERT INTO account VALUES('TEST@TEST.COM', 'TESTPW', 1)");
        pstmt.executeUpdate();
        PreparedStatement pstmt1 = connection.prepareStatement(
                "INSERT INTO siteuser VALUES('-1','TEST@TEST.COM','TESTFIRSTNAME','TESTLASTNAME','TESTBIO','TESTPICTUREPATH')");
        pstmt1.executeUpdate();
        PreparedStatement pstmt2 = connection.prepareStatement(
                "INSERT INTO account VALUES('TEST@TEST.COM2', 'TESTPW', 1)");
        pstmt2.executeUpdate();
        PreparedStatement pstmt3 = connection.prepareStatement(
                "INSERT INTO siteuser VALUES('-2','TEST@TEST.COM2','TESTFIRSTNAME','TESTLASTNAME','TESTBIO','TESTPICTUREPATH')");
        pstmt3.executeUpdate();
        connection.close();
        
        ArrayList<User> result = DatabaseAccessor.getUserList();
        assertEquals(2, result.size());
        assertEquals(-1, result.get(0).getID());
        assertEquals(-2, result.get(1).getID());
    }  
    
}
