package db;

import java.sql.*;
import java.util.Date;
import java.util.Calendar;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import models.*;

public class DatabaseAccessor {

    public DatabaseAccessor() {
    }

    //method used to connect to the database
    public static Connection getConnection() throws ServletException {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new ServletException(String.format(
                    "Error: Cannot find database driver"), e);
        }

        try {
            Connection connection = DriverManager.getConnection(
                    "jdbc:postgresql:postgres",
                    "student",
                    "");
            return connection;
        } catch (SQLException e) {
            throw new ServletException(String.format(
                    "Error: Database connection failed"), e);
        }

    }

    //***ACCOUNT DATABASE METHODS**//
    public static Account getAccountByEmail(String inputEmail) throws ServletException { //add ID when created
        Connection connection = DatabaseAccessor.getConnection();

        try {
            PreparedStatement pstmt = connection.prepareStatement(
                    "SELECT * FROM account WHERE email = ?");
            pstmt.setString(1, inputEmail);
            ResultSet results = pstmt.executeQuery();

            PreparedStatement pstmt1 = connection.prepareStatement(
                    "SELECT * FROM siteuser WHERE email = ?");
            pstmt1.setString(1, inputEmail);
            ResultSet results1 = pstmt1.executeQuery();

            if (results.next() && results1.next()) {
                Account acc = new Account(
                        results.getString("email"),
                        results.getString("password"),
                        results.getInt("isadmin"),
                        results1.getString("firstName"),
                        results1.getString("surname"));

                return acc;
            }
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static boolean isAccountEmailUsed(String email) throws ServletException, SQLException {

        Connection connection1 = DatabaseAccessor.getConnection();
        PreparedStatement pstmt1 = connection1.prepareStatement(
                "Select count(email) as emailTotal from siteuser WHERE email = '" + email + "'"); //change for injection

        ResultSet results1 = pstmt1.executeQuery();

        while (results1.next()) {
            if (results1.getInt("emailTotal") == 0) {
                return false;
            }
        }
        return true;

    }

    public static void persistAccount(Account account) throws ServletException, SQLException {
        Connection connection = DatabaseAccessor.getConnection();
        PreparedStatement pstmt = connection.prepareStatement(
                "INSERT INTO Account(email, password)"
                + "VALUES(?, ?)");
        pstmt.setString(1, account.getEmail());
        pstmt.setString(2, account.getPassword());

        pstmt.executeUpdate();

        pstmt = connection.prepareStatement(
                "INSERT INTO siteuser(email, firstname, surname)"
                + "VALUES(?, ?, ?)");
        pstmt.setString(1, account.getEmail());
        pstmt.setString(2, account.getUser().getFirstName());
        pstmt.setString(3, account.getUser().getLastName());

        pstmt.executeUpdate();
        connection.close();
    }
    //***END ACCOUNT DATABASE METHODS**//

    /**
     * *USER DATABASE METHODS**
     */
    public static User getUserByEmail(String inputEmail) throws ServletException { //add ID when created
        Connection connection = DatabaseAccessor.getConnection();

        try {
            PreparedStatement pstmt = connection.prepareStatement(
                    "SELECT * FROM siteuser WHERE email = ?");
            pstmt.setString(1, inputEmail);
            ResultSet results = pstmt.executeQuery();

            if (results.next()) {
                User user = new User(
                        results.getInt("userid"),
                        results.getString("email"),
                        results.getString("firstName"),
                        results.getString("surname"),
                        results.getString("bio"));
                user.populateUser();
                return user;
            }
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void persistUser(User user) throws ServletException, SQLException {
        Connection connection = DatabaseAccessor.getConnection();
        PreparedStatement pstmt = connection.prepareStatement(
                "INSERT INTO siteuser(email, firstname, surname, bio)"
                + "VALUES(?, ?, ?, ?)");
        pstmt.setString(1, user.getEmail());
        pstmt.setString(2, user.getFirstName());
        pstmt.setString(3, user.getLastName());
        pstmt.setString(4, user.getBio());

        pstmt.executeUpdate();
        connection.close();
    }

    /**
     * *END USER DATABASE METHODS**
     */
    //HEALTH OVERVIEW DATABASE METHODS
    public static HealthOverview getHOByUserID(int userID) throws ServletException {
        Connection connection = DatabaseAccessor.getConnection();
        HealthOverview HO = new HealthOverview();
        try {
            PreparedStatement pstmt = connection.prepareStatement(
                    "SELECT * FROM healthOverview WHERE userid = ?");
            pstmt.setInt(1, userID);
            ResultSet results = pstmt.executeQuery();

            if (results.next()) {
                HO = new HealthOverview(
                        results.getInt("healthoverviewid"),
                        results.getInt("userid"),
                        results.getFloat("height"),
                        results.getFloat("weight"),
                        results.getFloat("bmi"),
                        results.getString("conversion"),
                        results.getInt("weightchecked"));
                return HO;
            }

            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, ex);
        }
        return HO;
    }

    public static void persistHO(User user) throws ServletException, SQLException {

        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(
                    "UPDATE healthoverview SET weight=?, bmi=?, conversion = ?, weightchecked = ?"
                    + "WHERE userid=?");

            //pstmt.setFloat(1, user.getHO().getHeight());
            pstmt.setFloat(1, user.getHO().getCurrentWeight());
            pstmt.setFloat(2, user.getHO().getBmi());
            pstmt.setString(3, user.getHO().getConversion());
            pstmt.setInt(4, user.getHO().getWeightChecked());
            pstmt.setFloat(5, user.getID());
            pstmt.executeUpdate();
        }
        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(
                    "INSERT INTO healthoverview(userid, height, weight, bmi, conversion, weightchecked)"
                    + "SELECT ?,?,?,?,?,? WHERE NOT EXISTS ( SELECT * FROM healthoverview WHERE userid=?)");

            pstmt.setInt(1, user.getID());
            pstmt.setFloat(2, user.getHO().getHeight());
            pstmt.setFloat(3, user.getHO().getCurrentWeight());
            pstmt.setFloat(4, user.getHO().getBmi());
            pstmt.setString(5, user.getHO().getConversion());
            pstmt.setInt(6, user.getHO().getWeightChecked());
            pstmt.setInt(7, user.getID());
            pstmt.executeUpdate();
        }
        persistUpdatedWeight(user.getID(), user.getHO().getCurrentWeight());
    }

    /**
     * *END HEALTH OVERVIEW DATABASE METHODS**
     */
    public static void persistExercise(int userId, Exercise ex) throws ServletException, SQLException {
        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(
                    "INSERT INTO exercises(userid, exname, extype, distance, completiontime, exdetails, dateAdded, checked)"
                    + "VALUES(?, ?,?,?,?,?,?,?)");
            pstmt.setInt(1, userId);
            pstmt.setString(2, ex.getName());
            pstmt.setString(3, ex.getType());
            pstmt.setFloat(4, ex.getDistance());
            pstmt.setFloat(5, ex.getTime());
            pstmt.setString(6, ex.getDetails());
            pstmt.setDate(7, ex.getDateAdded());
            pstmt.setInt(8, ex.getChecked());
            pstmt.executeUpdate();
        }
    }

    public static void updateGoalTarget(int userId, Goal goal) throws ServletException, SQLException {
        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(
                    "UPDATE goals SET targetvalue=?"
                    + "WHERE userid=? AND goalid=?");

            pstmt.setFloat(1, goal.getTargetVal());
            pstmt.setFloat(2, userId);
            pstmt.setFloat(3, goal.getGoalId());
            pstmt.executeUpdate();
        }

    }

    //HEALTH OVERVIEW DATABASE METHODS
    public static ArrayList<Exercise> getExerciseList(int userID) throws ServletException {
        Connection connection = DatabaseAccessor.getConnection();
        ArrayList<Exercise> exercisesList = new ArrayList<>();
        try {
            PreparedStatement pstmt = connection.prepareStatement(
                    "SELECT * FROM exercises WHERE userid = ?");
            pstmt.setInt(1, userID);
            ResultSet results = pstmt.executeQuery();

            while (results.next()) {

                PreparedStatement pstmt2 = connection.prepareStatement(
                        "SELECT exname FROM exerciseList WHERE exerciseid = ?");
                pstmt2.setFloat(1, results.getFloat("extype"));
                ResultSet results2 = pstmt2.executeQuery();
                results2.next();

                Exercise ex = new Exercise(
                        results.getString("exname"),
                        results2.getString("exname"),
                        results.getString("exdetails"),
                        results.getFloat("distance"),
                        results.getFloat("completiontime"),
                        results.getInt("checked"));

                ex.setExId(results.getInt("exerciseid"));
                exercisesList.add(ex);
            }
            connection.close();
            return exercisesList;

        } catch (SQLException er) {
            Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, er);
        }
        return exercisesList;
    }

    public static void persistUserGoal(int userId, Goal goal) throws ServletException, SQLException {

        try (Connection connection = DatabaseAccessor.getConnection()) {

            PreparedStatement pstmt2 = connection.prepareStatement(
                    "SELECT exname FROM exerciseList WHERE exerciseid = ?");
            pstmt2.setFloat(1, goal.getExType());
            ResultSet results2 = pstmt2.executeQuery();
            results2.next();

            PreparedStatement pstmt = connection.prepareStatement(
                    "INSERT INTO goals(userid, goalname, goaltype, targetvalue, targetdate, targettype, completed, dateCreated, currentWeight, startTargetValue)"
                    + "VALUES(?, ?,?,?,?,?,?,?,?,?)");
            pstmt.setInt(1, userId);
            pstmt.setString(2, goal.getName());
            pstmt.setString(3, goal.getGoalType());
            pstmt.setFloat(4, goal.getTargetVal());
            pstmt.setDate(5, goal.getDate());
            pstmt.setString(6, results2.getString("exname"));
            pstmt.setInt(7, goal.getCompleted());
            pstmt.setTimestamp(8, goal.getDateCreated());
            pstmt.setFloat(9, goal.getCurrentWeight());
            pstmt.setFloat(10, goal.getStartTargetVal());
            pstmt.executeUpdate();
        }
    }

    public static void setGoalCompleted(int userId, Goal goal) throws ServletException, SQLException {
        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(
                    "UPDATE goals SET completed=?"
                    + "WHERE userid=? AND goalid=?");

            pstmt.setInt(1, 1);
            pstmt.setFloat(2, userId);
            pstmt.setFloat(3, goal.getGoalId());
            pstmt.executeUpdate();
        }

    }

    public static void setExerciseChecked(int userId, Exercise ex) throws ServletException, SQLException {
        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(
                    "UPDATE exercises SET checked=?"
                    + "WHERE userid=? AND exerciseid=?");

            pstmt.setInt(1, 1);
            pstmt.setFloat(2, userId);
            pstmt.setFloat(3, ex.getExId());
            pstmt.executeUpdate();
        }

    }

    /**
     * Method to return the string of an exercise ID
     *
     * @param exId exercise ID to convert to it's relevant string
     * @return string representation of the exercise
     * @throws ServletException
     * @throws SQLException
     */
    public static String getExNameFromId(int exId) throws ServletException, SQLException {
        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt2 = connection.prepareStatement(
                    "SELECT exname FROM exerciseList WHERE exerciseid = ?");
            pstmt2.setFloat(1, exId);
            ResultSet results2 = pstmt2.executeQuery();
            results2.next();
            return results2.getString("exname");
        }
    }

    //HEALTH OVERVIEW DATABASE METHODS
    public static ArrayList<Goal> getUserGoalsList(int userID) throws ServletException {
        Connection connection = DatabaseAccessor.getConnection();
        ArrayList<Goal> goalsList = new ArrayList<>();
        try {

            PreparedStatement pstmt = connection.prepareStatement(
                    "SELECT * FROM goals WHERE userid = ?");
            pstmt.setInt(1, userID);
            ResultSet results = pstmt.executeQuery();
            while (results.next()) {

                PreparedStatement pstmt2 = connection.prepareStatement(
                        "SELECT exerciseid FROM exerciseList WHERE exname = ?");
                pstmt2.setString(1, results.getString("targetType"));

                ResultSet results2 = pstmt2.executeQuery();
                results2.next();

                Goal goal = new Goal(
                        results.getString("goalName"),
                        results.getString("goalType"),
                        results.getFloat("targetvalue"),
                        results.getDate("targetDate"),
                        results2.getInt("exerciseid"),
                        results.getFloat("currentWeight"),
                        results.getTimestamp("dateCreated"),
                        results.getInt("completed"),
                        results.getFloat("startTargetValue"));
                goal.setGoalId(results.getInt("goalId"));
                goalsList.add(goal);
            }
            connection.close();
            return goalsList;

        } catch (SQLException er) {
            Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, er);
        }
        return goalsList;
    }

    //needs rethinkning, date passing?
    public static ArrayList<Consumable> getUserMealList(int userID) throws ServletException {
        Connection connection = DatabaseAccessor.getConnection();
        ArrayList<Consumable> foodList = new ArrayList<>();
        try {

            PreparedStatement pstmt = connection.prepareStatement(
                    "SELECT * FROM meals WHERE userid = ?");
            pstmt.setInt(1, userID);
            ResultSet results = pstmt.executeQuery();
            while (results.next()) {

                PreparedStatement pstmt2 = connection.prepareStatement(
                        "SELECT * FROM consumable WHERE mealid = ?");
                pstmt2.setInt(1, Integer.parseInt(results.getString("mealid")));

                ResultSet results2 = pstmt2.executeQuery();
                while (results2.next()) {

                    Consumable consumable = new Consumable(results2.getString("name"), Float.parseFloat(results2.getString("calories")), Float.parseFloat(results2.getString("protein")),
                            Float.parseFloat(results2.getString("fat")), Float.parseFloat(results2.getString("carbs")));
                    foodList.add(consumable);
                }
            }
            connection.close();
            return foodList;

        } catch (SQLException er) {
            Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, er);
        }
        return foodList;
    }

    public static void persistConsumable(int mealId, Consumable consumable) throws ServletException, SQLException {
        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(
                    "INSERT INTO consumable(mealid, name, calories, protein, fat, carbs)"
                    + "VALUES(?, ?,?,?,?,?)");
            pstmt.setInt(1, mealId);
            pstmt.setString(2, consumable.getName());
            pstmt.setFloat(3, consumable.getCalories());
            pstmt.setFloat(4, consumable.getProtein());
            pstmt.setFloat(5, consumable.getSatFat());
            pstmt.setFloat(6, consumable.getCarbs());
            pstmt.executeUpdate();
        }
    }

    public static void persistMeal(ArrayList<Consumable> consumables, int userId, Timestamp dateConsumed) throws ServletException, SQLException {
        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(
                    "INSERT INTO meals(userid, dateConsumed)"
                    + "VALUES(?, ?)");
            pstmt.setInt(1, userId);
            pstmt.setTimestamp(2, dateConsumed);
            pstmt.executeUpdate();

        }
        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(
                    "SELECT mealid FROM meals WHERE userId=? AND dateConsumed=?");
            pstmt.setInt(1, userId);
            pstmt.setTimestamp(2, dateConsumed);
            ResultSet results = pstmt.executeQuery();
            int mealId = 0;
            while (results.next()) {

                mealId = results.getInt("mealId");

            }
            for (Consumable c : consumables) {
                persistConsumable(mealId, c);
            }

        }
    }

    public static void persistUpdatedWeight(int userId, float weight) throws ServletException, SQLException {

        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(
                    "SELECT COUNT(weight) AS numOfWeights FROM weightHist WHERE userId=?");
            pstmt.setInt(1, userId);
            ResultSet results = pstmt.executeQuery();
            //GETS THE CURRENT TIME
            java.util.Date utilDate = new java.util.Date();
            //SETS CURRENT TIME AS A TIMESTAMP------------------------------------------------
            java.sql.Timestamp sq = new java.sql.Timestamp(utilDate.getTime());

            if (results.next()) {
                if (results.getInt("numOfWeights") >= 7) //need to run update last
                {
                    //gets primary keys of users last 7 weights
                    ArrayList<Integer> weightHistoryId = new ArrayList<>();
                    PreparedStatement pstmt2 = connection.prepareStatement(
                            "SELECT * FROM weightHist WHERE userId = ?");
                    pstmt2.setInt(1, userId);
                    ResultSet results2 = pstmt2.executeQuery();
                    while (results2.next()) {
                        weightHistoryId.add(results2.getInt("histid"));
                    }
                    //get smallest primary key value
                    int minPkey = Collections.min(weightHistoryId);

                    pstmt2 = connection.prepareStatement(
                            "DELETE FROM weightHist WHERE histId = ?");
                    pstmt2.setInt(1, minPkey);
                    pstmt2.executeUpdate();

                    pstmt2 = connection.prepareStatement(
                            "INSERT INTO weightHist(userid, weight, timeAdded)"
                            + "VALUES(?, ?, ?)");
                    pstmt2.setInt(1, userId);
                    pstmt2.setFloat(2, weight);
                    pstmt2.setTimestamp(3, sq);
                    pstmt2.executeUpdate();

                } else {

                    PreparedStatement pstmt2 = connection.prepareStatement(
                            "INSERT INTO weightHist(userid, weight, timeAdded)"
                            + "VALUES(?, ?, ?)");
                    pstmt2.setInt(1, userId);
                    pstmt2.setFloat(2, weight);
                    pstmt2.setTimestamp(3, sq);
                    pstmt2.executeUpdate();
                }
            }
        }
    }

    public static float[] getUserWeightHist(int userId) throws ServletException, SQLException {
        float[] weightHistory = new float[7];
        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt2 = connection.prepareStatement(
                    "SELECT * FROM weightHist WHERE userId = ?");
            pstmt2.setFloat(1, userId);
            ResultSet results = pstmt2.executeQuery();
            int i = 0;
            while (results.next()) {
                weightHistory[i] = Float.parseFloat(results.getString("weight"));
                i++;
            }

        }
        return weightHistory;
    }

    public static void editUserAccount(User user, String newPassword, String newFirstName, String newLastName,
            String newBio, String newPicPath, float newHeight) throws ServletException, SQLException {

        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(
                    "UPDATE siteUser SET firstname=?, surname=?, bio=?, picturepath=?"
                    + "WHERE userid=?");

            pstmt.setString(1, newFirstName);
            pstmt.setString(2, newLastName);
            pstmt.setString(3, newBio);
            pstmt.setString(4, newPicPath);
            pstmt.setInt(5, user.getID());

            pstmt.executeUpdate();
        }

        if (!newPassword.equals("")) {
            try (Connection connection = DatabaseAccessor.getConnection()) {
                PreparedStatement pstmt = connection.prepareStatement(
                        "UPDATE account SET password=?"
                        + "WHERE email=?");

                pstmt.setString(1, newPassword);
                pstmt.setString(2, user.getEmail());
                pstmt.executeUpdate();
            }
        }

        if (newHeight != 0) {
            try (Connection connection = DatabaseAccessor.getConnection()) {
                PreparedStatement pstmt = connection.prepareStatement(
                        "UPDATE healthoverview SET height=?"
                        + "WHERE userId=?");

                pstmt.setFloat(1, newHeight);
                pstmt.setInt(2, user.getID());
                pstmt.executeUpdate();
            }

        }

    }

    public static ArrayList<User> getGroupUserIds(int groupId) throws ServletException {
        Connection connection = DatabaseAccessor.getConnection();
        ArrayList<User> groupMembers = new ArrayList<>();
        try {

            PreparedStatement pstmt = connection.prepareStatement(
                    "SELECT * FROM groupMembers WHERE groupid = ?");
            pstmt.setInt(1, groupId);
            ResultSet results = pstmt.executeQuery();
            while (results.next()) {

                PreparedStatement pstmt2 = connection.prepareStatement(
                        "SELECT * FROM siteuser WHERE userid = ?");
                pstmt2.setInt(1, results.getInt("userid"));

                ResultSet results2 = pstmt2.executeQuery();
                while (results2.next()) {

                    User user = new User(results2.getInt("userid"), results2.getString("email"), results2.getString("firstname"), results2.getString("surname"));
                    groupMembers.add(user);
                }
            }
            connection.close();
            return groupMembers;

        } catch (SQLException er) {
            Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, er);
        }

        return groupMembers;

    }

    public static ArrayList<User> getUserList() throws ServletException {
        Connection connection = DatabaseAccessor.getConnection();
        ArrayList<User> usersList = new ArrayList<>();
        try {

            PreparedStatement pstmt = connection.prepareStatement(
                    "SELECT * FROM siteuser");
            //pstmt.setInt(1, userID);
            ResultSet results = pstmt.executeQuery();
            while (results.next()) {

                User user = new User(results.getInt("userId"), results.getString("email"), results.getString("firstName"), results.getString("surname"));

                usersList.add(user);
            }
            connection.close();
            return usersList;

        } catch (SQLException er) {
            Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, er);
        }

        return usersList;

    }

    public static ArrayList<UserGroups> searchGroup(String query) throws ServletException, SQLException {
        ArrayList<UserGroups> userGroups = new ArrayList<>();
        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(
                    "SELECT * FROM userGroups WHERE groupName ILIKE (?)");
            query = "%" + query + "%";
            pstmt.setString(1, query);
            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                UserGroups userGroup = new UserGroups(results.getInt("groupid"), results.getString("groupname"), results.getString("groupdetails"), results.getInt("groupadminuserid"));
                userGroups.add(userGroup);
            }
        }
        return userGroups;
    }

    public static void addUserToGroup(int userId, int groupId) throws ServletException, SQLException {

        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(
                    "INSERT INTO groupMembers(groupid, userid)"
                    + "VALUES(?, ?)");
            pstmt.setInt(1, groupId);
            pstmt.setInt(2, userId);
            pstmt.executeUpdate();
        }
    }

    public static void removeUserFromGroup(int userId, int groupId) throws ServletException, SQLException {

        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(
                    "DELETE FROM groupMembers WHERE groupid = ? AND userid = ?");
            pstmt.setInt(1, groupId);
            pstmt.setInt(2, userId);
            pstmt.executeUpdate();
        }
    }

    public static UserGroups getUserGroupByGroupID(int groupId) throws ServletException {
        Connection connection = DatabaseAccessor.getConnection();
        UserGroups userGroup = new UserGroups();
        try {
            PreparedStatement pstmt = connection.prepareStatement(
                    "SELECT * FROM usergroups WHERE groupid = ?");
            pstmt.setInt(1, groupId);
            ResultSet results = pstmt.executeQuery();

            if (results.next()) {
                userGroup = new UserGroups(results.getInt("groupid"), results.getString("groupname"), results.getString("groupdetails"), results.getInt("groupadminuserid"));

                return userGroup;
            }

            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, ex);
        }
        return userGroup;
    }

    public static void deleteUser(int userId) throws ServletException {
        Connection connection = DatabaseAccessor.getConnection();

        try {

            //Get email associated with user from database.
            PreparedStatement pstmt = connection.prepareStatement(
                    "SELECT * FROM siteuser WHERE userid = ?");
            pstmt.setInt(1, userId);
            ResultSet results = pstmt.executeQuery();
            results.next();
            String email = results.getString("email");

            //Deletes account under email.
            PreparedStatement pstmt2 = connection.prepareStatement(
                    "DELETE FROM account WHERE email = ?");
            pstmt2.setString(1, email);
            pstmt2.executeUpdate();

        } catch (SQLException er) {
            Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, er);
        }
    }

    public static void updateGroup(int groupId, String newGroupName, String newGroupDetails) throws ServletException, SQLException {
        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(
                    "UPDATE usergroups SET groupname=?, groupDetails =?"
                    + "WHERE groupid=?");

            pstmt.setString(1, newGroupName);
            pstmt.setString(2, newGroupDetails);
            pstmt.setInt(3, groupId);
            pstmt.executeUpdate();
        }

    }

    public static void persistGroup(int userId, String groupName, String groupDetails) throws ServletException, SQLException {
        Connection connection = DatabaseAccessor.getConnection();
        PreparedStatement pstmt = connection.prepareStatement(
                "INSERT INTO usergroups(groupname, groupdetails, groupadminuserid)"
                + "VALUES(?, ?, ?)");
        pstmt.setString(1, groupName);
        pstmt.setString(2, groupDetails);
        pstmt.setInt(3, userId);

        pstmt.executeUpdate();
        connection.close();
    }

    public static ArrayList<UserGroups> getYourGroups(int userId) throws ServletException {
        Connection connection = DatabaseAccessor.getConnection();
        ArrayList<UserGroups> yourGroups = new ArrayList<>();
        try {

            PreparedStatement pstmt = connection.prepareStatement(
                    "SELECT * FROM groupMembers WHERE userid = ?");
            pstmt.setInt(1, userId);

            ResultSet results = pstmt.executeQuery();
            while (results.next()) {

                PreparedStatement pstmt2 = connection.prepareStatement(
                        "SELECT * FROM usergroups WHERE groupid = ?");
                pstmt2.setInt(1, results.getInt("groupid"));

                ResultSet results2 = pstmt2.executeQuery();
                while (results2.next()) {

                    UserGroups group = new UserGroups(results2.getInt("groupid"), results2.getString("groupname"), results2.getString("groupdetails"), results2.getInt("groupadminuserid"));
                    yourGroups.add(group);
                }
            }
            connection.close();
            return yourGroups;

        } catch (SQLException er) {
            Logger.getLogger(Account.class
                    .getName()).log(Level.SEVERE, null, er);
        }

        return yourGroups;
    }

    public static UserGroups getGroupByNameAndAdminId(String groupName, int adminId) throws ServletException, SQLException {
        UserGroups userGroup = new UserGroups();
        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(
                    "SELECT * FROM usergroups WHERE groupname = ? AND groupadminuserid =?");
            pstmt.setString(1, groupName);
            pstmt.setInt(2, adminId);
            ResultSet results = pstmt.executeQuery();

            if (results.next()) {
                userGroup = new UserGroups(results.getInt("groupid"), results.getString("groupname"), results.getString("groupdetails"), results.getInt("groupadminuserid"));
            }
            return userGroup;
        }
    }

    public static ArrayList<Consumable> getMealbyDate(int userID, Date date) throws ServletException {
        Connection connection = DatabaseAccessor.getConnection();
        ArrayList<Consumable> foodList = new ArrayList<>();

        Calendar cal1 = Calendar.getInstance();       // get calendar instance
        cal1.setTime(date);                           // set cal to date
        cal1.set(Calendar.HOUR_OF_DAY, 0);            // set hour to midnight
        cal1.set(Calendar.MINUTE, 0);                 // set minute in hour
        cal1.set(Calendar.SECOND, 0);                 // set second in minute
        cal1.set(Calendar.MILLISECOND, 0);            // set millis in second
        Date date1 = cal1.getTime();             // actually computes the new Date

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date1);
        cal2.add(Calendar.DATE, 1);
        Date date2 = cal2.getTime();

        java.sql.Timestamp begin = new java.sql.Timestamp(date1.getTime());
        java.sql.Timestamp end = new java.sql.Timestamp(date2.getTime());

        try {

            PreparedStatement pstmt = connection.prepareStatement(
                    "SELECT * FROM meals WHERE userid = ? AND dateconsumed >= ? AND dateconsumed < ?");

            pstmt.setInt(1, userID);
            pstmt.setTimestamp(2, begin);
            pstmt.setTimestamp(3, end);

            ResultSet results = pstmt.executeQuery();
            while (results.next()) {

                PreparedStatement pstmt2 = connection.prepareStatement(
                        "SELECT * FROM consumable WHERE mealid = ?");
                pstmt2.setInt(1, Integer.parseInt(results.getString("mealid")));

                ResultSet results2 = pstmt2.executeQuery();
                while (results2.next()) {

                    Consumable consumable = new Consumable(results2.getString("name"), Float.parseFloat(results2.getString("calories")), Float.parseFloat(results2.getString("protein")),
                            Float.parseFloat(results2.getString("fat")), Float.parseFloat(results2.getString("carbs")), results2.getInt("mealid"));
                    foodList.add(consumable);
                }
            }
            connection.close();
            
            Collections.sort(foodList);
            
            return foodList;

        } catch (SQLException er) {
            Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, er);
        }
        
        Collections.sort(foodList);
        return foodList;
    }

    public static void deleteGroup(int groupId) throws ServletException, SQLException {

        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(
                    "DELETE FROM usergroups WHERE groupid = ?");
            pstmt.setInt(1, groupId);
            pstmt.executeUpdate();
        }
    }

    public static void updateGroupGoalTarget(int groupId, Goal goal) throws ServletException, SQLException {
        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(
                    "UPDATE goals SET targetvalue=?"
                    + "WHERE groupId=? AND goalid=?");

            pstmt.setFloat(1, goal.getTargetVal());
            pstmt.setFloat(2, groupId);
            pstmt.setFloat(3, goal.getGoalId());
            pstmt.executeUpdate();
        }

    }

    public static Goal getGroupGoalByDetails(UserGroups group, Goal goal) throws ServletException, SQLException {
        Goal tempGoal = new Goal();
        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(
                    "SELECT * FROM goals WHERE goalname = ? AND groupid =? AND datecreated = ?");
            pstmt.setString(1, goal.getName());
            pstmt.setInt(2, group.getGroupID());
            pstmt.setTimestamp(3, goal.getDateCreated());
            ResultSet results = pstmt.executeQuery();

            if (results.next()) {
                tempGoal = new Goal(
                        results.getString("goalName"),
                        results.getInt("goalId"),
                        results.getTimestamp("dateCreated"));
            }
            return tempGoal;
        }
    }

    public static void setGroupGoalCompleted(int groupId, Goal goal) throws ServletException, SQLException {
        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(
                    "UPDATE goals SET completed=?"
                    + "WHERE groupId=? AND goalid=?");

            pstmt.setInt(1, 1);
            pstmt.setFloat(2, groupId);
            pstmt.setFloat(3, goal.getGoalId());
            pstmt.executeUpdate();
        }

    }

    public static void persistGroupGoal(int groupid, Goal goal) throws ServletException, SQLException {

        try (Connection connection = DatabaseAccessor.getConnection()) {

            PreparedStatement pstmt2 = connection.prepareStatement(
                    "SELECT exname FROM exerciseList WHERE exerciseid = ?");
            pstmt2.setFloat(1, goal.getExType());
            ResultSet results2 = pstmt2.executeQuery();
            results2.next();

            PreparedStatement pstmt = connection.prepareStatement(
                    "INSERT INTO goals(groupid, goalname, goaltype, targetvalue, targetdate, targettype, completed, dateCreated, currentWeight, startTargetValue)"
                    + "VALUES(?, ?,?,?,?,?,?,?,?,?)");
            pstmt.setInt(1, groupid);
            pstmt.setString(2, goal.getName());
            pstmt.setString(3, goal.getGoalType());
            pstmt.setFloat(4, goal.getTargetVal());
            pstmt.setDate(5, goal.getDate());
            pstmt.setString(6, results2.getString("exname"));
            pstmt.setInt(7, goal.getCompleted());
            pstmt.setTimestamp(8, goal.getDateCreated());
            pstmt.setFloat(9, goal.getCurrentWeight());
            pstmt.setFloat(10, goal.getStartTargetVal());
            pstmt.executeUpdate();
        }
    }

    public static ArrayList<Goal> getGroupGoals(int groupId) throws ServletException {
        Connection connection = DatabaseAccessor.getConnection();
        ArrayList<Goal> goalsList = new ArrayList<>();
        try {

            PreparedStatement pstmt = connection.prepareStatement(
                    "SELECT * FROM goals WHERE groupId = ?");
            pstmt.setInt(1, groupId);
            ResultSet results = pstmt.executeQuery();
            while (results.next()) {

                PreparedStatement pstmt2 = connection.prepareStatement(
                        "SELECT exerciseid FROM exerciseList WHERE exname = ?");
                pstmt2.setString(1, results.getString("targetType"));

                ResultSet results2 = pstmt2.executeQuery();
                results2.next();

                Goal goal = new Goal(
                        results.getString("goalName"),
                        results.getString("goalType"),
                        results.getFloat("targetvalue"),
                        results.getDate("targetDate"),
                        results2.getInt("exerciseid"),
                        results.getFloat("currentWeight"),
                        results.getTimestamp("dateCreated"),
                        results.getInt("completed"),
                        results.getFloat("startTargetValue"));
                goal.setGoalId(results.getInt("goalId"));
                goalsList.add(goal);
            }
            connection.close();
            return goalsList;

        } catch (SQLException er) {
            Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, er);
        }
        return goalsList;
    }

    public static void persistGroupMemberGoalWeight(User user, Goal goal) throws ServletException, SQLException {
        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(
                    "INSERT INTO groupMemberGoalWeight (userid, goalId, creationWeight)"
                    + "VALUES(?,?,?)");
            pstmt.setInt(1, user.getID());
            pstmt.setInt(2, goal.getGoalId());
            pstmt.setFloat(3, user.getHO().getCurrentWeight());
            pstmt.executeUpdate();
        }
    }

    public static float getMembersStartingWeightByGoalIdAndUserId(int userId, int goalId) throws ServletException, SQLException {

        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(
                    "SELECT * FROM groupMemberGoalWeight WHERE userid = ? AND goalId =?");
            pstmt.setInt(1, userId);
            pstmt.setInt(2, goalId);
            ResultSet results = pstmt.executeQuery();

            if (results.next()) {
                return results.getFloat("creationWeight");
            }
            return 0;
        }
    }

    public static ArrayList<Notification> getNotifications(int userId) throws ServletException, SQLException {
       ArrayList<Notification> notifications = new ArrayList<Notification>();
        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(
                    "SELECT * FROM notifications WHERE recieverId = ?");
            pstmt.setInt(1, userId);

            ResultSet results = pstmt.executeQuery();

            if (results.next()) {
                Notification not = new Notification(results.getInt("recieverId"), results.getInt("senderid"),results.getInt("groupid"),results.getString("content"), results.getInt("notificationId"));
                notifications.add(not);
            }
            return notifications;
        }
    }

    public static ArrayList<User> searchUser(String query) throws ServletException, SQLException {
        ArrayList<User> users = new ArrayList<>();
        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(
                    "SELECT * FROM siteUser WHERE firstname ILIKE (?) OR surname ILIKE (?)");
            query = "%" + query + "%";
            pstmt.setString(1, query);
            pstmt.setString(2, query);
            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                User user = new User(results.getInt("userid"), results.getString("email"), results.getString("firstname"), results.getString("surname"), results.getString("bio"));
                users.add(user);
            }
        }
        return users;
    }

    public static void sendInviteToGroup(Notification not) throws ServletException, SQLException {

        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(
                    "INSERT INTO notifications (recieverId,senderId,groupId, content) VALUES(?,?,?,?)");
            pstmt.setInt(1, not.getRecieverId());
            pstmt.setInt(2, not.getSenderId());
            pstmt.setInt(3, not.getGroupId());
            pstmt.setString(4, not.getContent());
            pstmt.executeUpdate();

        }
    }
     public static UserGroups getGroupByGroupId(int groupId) throws ServletException, SQLException {
        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt2 = connection.prepareStatement(
                    "SELECT * FROM usergroups WHERE groupid = ?");
            pstmt2.setInt(1, groupId);
            ResultSet results2 = pstmt2.executeQuery();
            results2.next();
            return new UserGroups(results2.getInt("groupid"), results2.getString("groupname"), results2.getString("groupdetails"), results2.getInt("groupAdminUserId"));
        }
    }

     public static void deleteNotification(int notificationId) throws ServletException, SQLException {

        try (Connection connection = DatabaseAccessor.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(
                    "DELETE FROM notifications WHERE notificationId = ?");
            pstmt.setInt(1, notificationId);
            pstmt.executeUpdate();
        }
    }
    
}
