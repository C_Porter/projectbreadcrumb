/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import db.DatabaseAccessor;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.*;

@WebServlet(urlPatterns = {"/GroupAdminController"})
public class GroupAdminController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = (String) request.getSession().getAttribute("email");
        User user = DatabaseAccessor.getUserByEmail(email);
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        UserGroups userGroup = (UserGroups) request.getSession().getAttribute("selectedGroup");
        if(request.getParameter("newGroupName")!= null && request.getParameter("newGroupDetails") !=null){
            if (!request.getParameter("newGroupName").equals("")) {
                String newGroupName = request.getParameter("newGroupName");
                try {
                    userGroup = DatabaseAccessor.getGroupByNameAndAdminId(userGroup.getGroupName(), userGroup.getAdminId());
                    DatabaseAccessor.updateGroup(userGroup.getGroupID(),newGroupName, userGroup.getGroupDetails());
                    userGroup = DatabaseAccessor.getGroupByNameAndAdminId(newGroupName, userGroup.getAdminId());
                } catch (SQLException ex) {
                    Logger.getLogger(GroupAdminController.class.getName()).log(Level.SEVERE, null, ex);
                }
            } 
            if (!request.getParameter("newGroupDetails").equals("")) {
                String newGroupDetails = request.getParameter("newGroupDetails");
                try {
                    userGroup = DatabaseAccessor.getGroupByNameAndAdminId(userGroup.getGroupName(), userGroup.getAdminId());
                    DatabaseAccessor.updateGroup(userGroup.getGroupID(),userGroup.getGroupName(), newGroupDetails);
                } catch (SQLException ex) {
                    Logger.getLogger(GroupAdminController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        else if (request.getParameter("userID") != null) {
            int userIdToRemove = Integer.parseInt(request.getParameter("userID"));
            try {
                DatabaseAccessor.removeUserFromGroup(userIdToRemove, userGroup.getGroupID());
            } catch (SQLException ex) {
                Logger.getLogger(GroupAdminController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        try {
            //uupdates session

            userGroup = DatabaseAccessor.getGroupByNameAndAdminId(userGroup.getGroupName(), userGroup.getAdminId());
        } catch (SQLException ex) {
            Logger.getLogger(GroupAdminController.class.getName()).log(Level.SEVERE, null, ex);
        }
          request.getSession().setAttribute("selectedGroup", userGroup);
        
        request.getRequestDispatcher("GroupPage.jsp").forward(request, response);
        out.close();
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
