/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import db.DatabaseAccessor;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.*;

@WebServlet(urlPatterns = {"/AddMealController"})
public class AddMealController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String email = (String) request.getSession().getAttribute("email");
        User user = DatabaseAccessor.getUserByEmail(email);

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        ArrayList<Consumable> tempMeal;
        if (request.getSession().getAttribute("tempMeal") == null) {
            tempMeal = new ArrayList<Consumable>();
            // mealList = (ArrayList<Consumable>) request.getAttribute("tempMeal");
        } else {
            tempMeal = (ArrayList<Consumable>) request.getSession().getAttribute("tempMeal");
        }
        
         int hours = Integer.parseInt(request.getParameter("hours"));
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        /* long ms=0;
        try {
            ms= dateFormat.parse("3600").getTime();
        } catch (ParseException ex) {
            Logger.getLogger(AddMealController.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        
     
        java.util.Date goalTargetDateTemp;
        java.sql.Date dateConsumed;
        //get current date
      

        try {
            goalTargetDateTemp = dateFormat.parse(request.getParameter("dateConsumed"));
            dateConsumed = new java.sql.Date(goalTargetDateTemp.getTime());
            

        } catch (ParseException e) {
            dateConsumed = new java.sql.Date(01, 01, 1800);
            //e.printStackTrace();
        }
        java.sql.Timestamp ts = new java.sql.Timestamp(dateConsumed.getTime()+(hours*3600000));
        
       
       
       
        try {
            DatabaseAccessor.persistMeal(tempMeal,user.getID(),ts); // user.updateGoals();
        } catch (SQLException ex) {
            Logger.getLogger(AddMealController.class.getName()).log(Level.SEVERE, null, ex);
        }
           
        request.getSession().setAttribute("tempMeal", new ArrayList<Consumable>());
        
       
         request.setAttribute("exMessage", "Consumable Added!");
        request.getRequestDispatcher("Diet.jsp").forward(request, response);

        out.close();
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
