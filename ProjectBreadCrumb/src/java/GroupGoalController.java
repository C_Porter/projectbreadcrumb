/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import db.DatabaseAccessor;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.*;

@WebServlet(urlPatterns = {"/GroupGoalController"})
public class GroupGoalController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = (String) request.getSession().getAttribute("email");
        User user = DatabaseAccessor.getUserByEmail(email);
        UserGroups userGroup = (UserGroups) request.getSession().getAttribute("selectedGroup");
        userGroup.populateUserGroup();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String goalName = request.getParameter("goalName");
        String goalType = request.getParameter("goalType");
        // int exType = Integer.parseInt(request.getParameter("exType"))==0?0:Integer.parseInt(request.getParameter("exType"));
        int exType = Integer.parseInt(request.getParameter("exType"));
        float goalTargetVal = Float.parseFloat(request.getParameter("goalTargetVal"));

        java.util.Date goalTargetDateTemp;
        java.sql.Date goalTargetDate;
        //get current date
        java.util.Date utilDate = new java.util.Date();
        //SETS CURRENT TIME AS A TIMESTAMP------------------------------------------------
        java.sql.Timestamp dateCreated = new java.sql.Timestamp(utilDate.getTime());

        try {
            goalTargetDateTemp = dateFormat.parse(request.getParameter("goalTargetDate"));
            goalTargetDate = new java.sql.Date(goalTargetDateTemp.getTime());

        } catch (ParseException e) {
            goalTargetDate = new java.sql.Date(01, 01, 1800);
            //e.printStackTrace();
        }

        user.populateUser();
        Goal goal = new Goal(goalName, goalType, goalTargetVal, goalTargetDate, exType, 0, dateCreated, 0, goalTargetVal);

        try {
            DatabaseAccessor.persistGroupGoal(userGroup.getGroupID(), goal);

            if(goalType.equals("weightLoss") || goalType.equals("weightGain")){
                Goal tempGoal = DatabaseAccessor.getGroupGoalByDetails(userGroup, goal);

                for(User u : userGroup.getUserList()){
                    u.populateUser();
                    DatabaseAccessor.persistGroupMemberGoalWeight(u, tempGoal);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(GoalsController.class.getName()).log(Level.SEVERE, null, ex);
        }
        // request.setAttribute("accountCreatedMessage", "Updated");
        request.getRequestDispatcher("Profile.jsp").forward(request, response);

        out.close();
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
