/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import db.DatabaseAccessor;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.*;

@WebServlet(urlPatterns = {"/InviteUserToGroupController"})
public class InviteUserToGroupController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = (String) request.getSession().getAttribute("email");
        User user = DatabaseAccessor.getUserByEmail(email);
    UserGroups group = (UserGroups) request.getSession().getAttribute("selectedGroup");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        int recieverId = Integer.parseInt(request.getParameter("selectedResult"));
        
         ArrayList<User> recievers = (ArrayList<User>) request.getSession().getAttribute("userResults");
      
        try {
           String content = ""+ user.getFirstName()+" "+user.getLastName()+" has invited you to join '"+group.getGroupName()+"'";
           Notification not = new Notification(user.getID(), recievers.get(recieverId).getID(),group.getGroupID(),content);
          DatabaseAccessor.sendInviteToGroup(not);
         
        } catch (SQLException ex) {
            Logger.getLogger(UpdateHealthOverviewController.class.getName()).log(Level.SEVERE, null, ex);
        }
        // request.setAttribute("accountCreatedMessage", "Updated");
        request.getRequestDispatcher("ResultsPage.jsp").forward(request, response);

        out.close();
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
