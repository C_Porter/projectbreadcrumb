/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import db.DatabaseAccessor;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.*;

@WebServlet(urlPatterns = {"/GroupController"})
public class GroupController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String email = (String) request.getSession().getAttribute("email");
            User user = DatabaseAccessor.getUserByEmail(email);

            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();           

            if (request.getParameter("groupJoinedId") != null) {
                int groupJoinedId = Integer.parseInt(request.getParameter("groupJoinedId"));
                UserGroups group = DatabaseAccessor.getUserGroupByGroupID(groupJoinedId);
                if (!group.isAMember(user.getID())) {
                    DatabaseAccessor.addUserToGroup(user.getID(), groupJoinedId);
                }
            }
            else if(request.getParameter("groupToLeaveId") != null){
                int groupToLeaveId = Integer.parseInt(request.getParameter("groupToLeaveId"));
                UserGroups group = DatabaseAccessor.getUserGroupByGroupID(groupToLeaveId);
                if (group.isAMember(user.getID())) {
                    DatabaseAccessor.removeUserFromGroup(user.getID(), groupToLeaveId);
                }
            }
            else if(request.getParameter("groupToDeleteId") != null){
                int groupToDeleteId = Integer.parseInt(request.getParameter("groupToDeleteId"));
                /*UserGroups group = DatabaseAccessor.getUserGroupByGroupID(groupToDeleteId);
                if (group.isAMember(user.getID())) {
                    DatabaseAccessor.removeUserFromGroup(user.getID(), groupToLeaveId);
                }*/
                DatabaseAccessor.deleteGroup(groupToDeleteId);
                request.getRequestDispatcher("/YourGroupsController").forward(request, response);
            }

            request.getRequestDispatcher("GroupPage.jsp").forward(request, response);

            out.close();
        } catch (SQLException ex) {
            Logger.getLogger(GroupController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
