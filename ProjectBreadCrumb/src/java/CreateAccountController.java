/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import db.DatabaseAccessor;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Properties;
//import javax.mail.Message;
//import javax.mail.MessagingException;
//import javax.mail.PasswordAuthentication;
//import javax.mail.Session;
//import javax.mail.Transport;
//import javax.mail.internet.InternetAddress;
//import javax.mail.internet.MimeMessage;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Account;
import models.User;

@WebServlet(urlPatterns = {"/CreateAccountController"})
public class CreateAccountController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String email = request.getParameter("inputEmail");
        String password = request.getParameter("inputPassword");
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        float height = Float.parseFloat(request.getParameter("height"));
        float weight = Float.parseFloat(request.getParameter("weight"));
        String conversion = request.getParameter("conversion");

        int adminLogin = 0;
        try {
            if (DatabaseAccessor.isAccountEmailUsed(email) == false) {
                //call error if fields are empty
                if (email.trim().equals("")
                        || password.trim().equals("")) {
                    request.setAttribute("accountCreatedMessage", "All fields require input");
                    request.getRequestDispatcher("index.jsp").forward(request, response);
                } //makes a new user and redirects to login page for user to login
                else {

                    Account newAccount = new Account(email,
                            password, firstName, lastName, height, weight, conversion);
                    //test
                    try {
                        DatabaseAccessor.persistAccount(newAccount);
                       
                        User tempUser = DatabaseAccessor.getUserByEmail(email);
                        newAccount.getUser().setId(tempUser.getID());
                        newAccount.getUser().getHO().calcBMI();
                        DatabaseAccessor.persistHO(newAccount.getUser());

                        /*Properties  properties = new Properties();
                         properties.put("mail.smtp.host", "smtp.gmail.com");
                         properties.put("mail.smtp.socketFactory.port", "465");
                         properties.put("mail.smtp.socketFactory.class",
                         "javax.net.ssl.SSLSocketFactory");
                         properties.put("mail.smtp.auth", "true");
                         properties.put("mail.smtp.port", "465");
                        
                         // Session mailSession = Session.getDefaultInstance(properties);
                         Session mailSession = Session.getInstance(properties,
                         new javax.mail.Authenticator() {
                         protected PasswordAuthentication getPasswordAuthentication() {
                         return new PasswordAuthentication("healthtrackertoast@gmail.com", "toast123");
                         }
                         });
                         try {
                        
                         Message message = new MimeMessage(mailSession);
                         message.setFrom(new InternetAddress("from@no-spam.com"));
                         message.setRecipients(Message.RecipientType.TO,
                         InternetAddress.parse("testing@hotmail.co.uk"));
                         //Need to get email to send to
                         message.setSubject("Testing Subject");
                         message.setText("Dear HealthTracker user,"
                         + "\n\n You are now registered! ");
                        
                         Transport.send(message);
                        
                         System.out.println("Done");
                        
                         } catch (MessagingException e) {
                         throw new RuntimeException(e);
                         }*/
                    } catch (SQLException ex) {
                        Logger.getLogger(CreateAccountController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    //request.setAttribute("accountCreatedMessage", "Your account has been created, please login");
                    request.getRequestDispatcher("/LogInController").forward(request, response);
                    

                }

            } else {
                request.setAttribute("accountCreatedMessage", "Email is already in use");
                request.getRequestDispatcher("index.jsp").forward(request, response);

            }
        } catch (SQLException ex) {
            Logger.getLogger(CreateAccountController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
