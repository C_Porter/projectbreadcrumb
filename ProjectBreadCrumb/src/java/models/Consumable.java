package models;
import java.util.Comparator;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author puy12jqu
 */
public class Consumable implements Comparable<Consumable> {
    
    private String name;
    private float calories, protein, satFat, carbs;
    private int mealID;

    public Consumable(String name, float calories, float protein, float satFat, float carbs) {
        this.name = name;
        this.calories = calories;
        this.protein = protein;
        this.satFat = satFat;
        this.carbs = carbs;
    }
    
    public Consumable(String name, float calories, float protein, float satFat, float carbs, int mealID) {
        this.name = name;
        this.calories = calories;
        this.protein = protein;
        this.satFat = satFat;
        this.carbs = carbs;
        this.mealID = mealID;
    }

    public String getName() {
        return name;
    }


    public float getCalories() {
        return calories;
    }

    public float getProtein() {
        return protein;
    }

    public float getSatFat() {
        return satFat;
    }

    public float getCarbs() {
        return carbs;
    }
    
    public int getMealID() {
        return mealID;
    }
    
    @Override
    public int compareTo(Consumable c) {
        return mealID < c.mealID ? -1 : mealID == c.mealID ? 0 : 1;
    }
    
}
