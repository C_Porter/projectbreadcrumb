package models;

import db.DatabaseAccessor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;

public class HealthOverview {

    private float  bmi, height, currentWeight;
    private String conversion; //Metric or imperial 
    //database variables
    private int HOid, userID;
    private float[] weightArray = {0, 0, 0, 0, 0, 0, 0};
    int weightInput, bmiInput, weightChecked ;


    public HealthOverview(float height,
            float currentWeight, String conversion) {
        //this.targetWeight = targetWeight;
        this.height = height;
        this.currentWeight = currentWeight;
        this.conversion = conversion;
        this.weightInput = 0;
        this.bmiInput = 0;
        calcBMI();

        //targetweight if imperial needs to be converted to just lbs
        //targetweight if imperial needs to converted to inches     
    }

    /**
     * Constructor for database details
     *
     * @param HOid
     * @param userID
     * @param height
     * @param currentWeight
     * @param bmi
     */
    public HealthOverview(int HOid, int userID,
            float height, float weight, float bmi, String conversion, int weightChecked) {
        this.HOid = HOid;
        this.userID = userID;
        this.height = height;
        this.currentWeight = weight;
        this.bmi = bmi;
        this.conversion = conversion;
        this.weightInput = 0;
        this.bmiInput = 0;
        this.weightChecked = weightChecked;

    }

    public HealthOverview() {}

    public void setWeightChecked(int setValue){
        this.weightChecked = setValue;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }


    public int getWeightChecked() {
        return this.weightChecked;
    }
  

    public float[] getWeightArray() {

        return weightArray;
    }

    public float getBmi() {
        calcBMI();
        double newBMI = Math.round(bmi * 100.0) / 100.0;
        return (float) newBMI;

    }

    public String getConversion() {
        return conversion;
    }

    public int getHOid() {
        return HOid;
    }

    public float getHeight() {
        return height;
    }

    public float getCurrentWeight() {
        return currentWeight;
    }


    public void setCurrentWeight(float currentWeight) {

        if (weightInput > 6) {
            weightArray[0] = weightArray[1];
            weightArray[1] = weightArray[2];
            weightArray[2] = weightArray[3];
            weightArray[3] = weightArray[4];
            weightArray[4] = weightArray[5];
            weightArray[5] = weightArray[6];
            weightArray[6] = currentWeight;
        } else {
            weightArray[weightInput] = currentWeight;
            weightInput++;
        }
        this.currentWeight = currentWeight;
    }

   
    /*
     Used for calory count in exercise
     */
    public double poundsToKgs() {
        if (conversion.equals("imperial")) {
            return currentWeight / 2.2;
        }
        return currentWeight;
    }

    public void calcBMI() {
        if (conversion.equals("imperial")) {
            bmi = ((float)currentWeight / (height * height)) * 703;

        } else if (conversion.equals("metric")) {
            bmi = ((float)poundsToKgs() / (height * height))*10000;
        }
       
    }

}
