package models;

import db.DatabaseAccessor;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;

public class UserGroups {

    private int groupId, adminId;
    private String groupName, details;
    private ArrayList<User> userList;
    private ArrayList<Goal> groupGoals;


    public UserGroups(String groupName) {
        this.groupName = groupName;
        userList = new ArrayList<>();
        groupGoals = new ArrayList<>();
    }

    public UserGroups() {
    }

    public UserGroups(int groupId, String groupName, String groupDetails, int adminId) {
        this.groupId = groupId;
        this.groupName = groupName;
        this.details = groupDetails;
        this.adminId = adminId;
        this.groupGoals = new ArrayList<>();

        try {
            userList = DatabaseAccessor.getGroupUserIds(groupId);
        } catch (ServletException ex) {
            Logger.getLogger(UserGroups.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getGroupName() {
        return groupName;
    }

    public int getGroupID() {
        return groupId;
    }

    public String getGroupDetails() {
        return details;
    }

    public ArrayList<User> getUserList() {
        return userList;
    }

    public ArrayList<Goal> getGroupGoals() {
        return groupGoals;
    }

    public int getAdminId() {
        return this.adminId;
    }

    public boolean isAMember(int userId) {

        for (User user : this.userList) {
            if (user.getID() == userId) {
                return true;
            }
        }
        return false;
    }

    public void populateUserGroup() {
        try {
            this.userList = DatabaseAccessor.getGroupUserIds(groupId);
            this.groupGoals = DatabaseAccessor.getGroupGoals(groupId);
        } catch (ServletException ex) {
            Logger.getLogger(UserGroups.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateGoals() throws SQLException, ServletException {

        for (Goal g : this.groupGoals) {

            int amountCompleted = 0;

            for (User user : this.userList) {
                user.populateUser();
                float distanceTotal = 0;
                float durationTotal = 0;
                float weightLost = 0;
                float weightGain = 0;

                for (Exercise ex : user.getExercises()) {
                    if (ex.getDateAdded().after(g.getDateCreated()) || ex.getDateAdded().equals(g.getDateCreated())) {
                        if ((DatabaseAccessor.getExNameFromId(g.getExType()).equals(ex.getType()))) {
                            switch (g.getGoalType()) {
                                case "distance":
                                    distanceTotal += ex.getDistance();
                                    break;
                                case "duration":
                                    durationTotal += ex.getTime();
                                    break;
                            }

                        }

                    }
                }
                if (g.getGoalType().equals("weightLoss")) {
                    weightLost = DatabaseAccessor.getMembersStartingWeightByGoalIdAndUserId(user.getID(), g.getGoalId()) - user.getHO().getCurrentWeight();
                }
                if (g.getGoalType().equals("weightGain")) {
                    weightGain = user.getHO().getCurrentWeight() - DatabaseAccessor.getMembersStartingWeightByGoalIdAndUserId(user.getID(), g.getGoalId());
                }
                switch (g.getGoalType()) {
                    case "distance":
                        if (distanceTotal >= g.getStartTargetVal()) {
                            for (User notifyUser : this.userList) {
                                String userName = user.getFirstName() + " " + user.getLastName();
                                Notification not = new Notification(user.getID(), notifyUser.getID(), this.getGroupID(), userName + " has completed the goal " + g.getName());
                                DatabaseAccessor.sendInviteToGroup(not);
                            }
                            amountCompleted++;
                        }
                        break;
                    case "duration":
                        if (durationTotal >= g.getStartTargetVal()) {
                            for (User notifyUser : this.userList) {
                                String userName = user.getFirstName() + " " + user.getLastName();
                                Notification not = new Notification(user.getID(), notifyUser.getID(), this.getGroupID(), userName + " has completed the goal " + g.getName());
                                DatabaseAccessor.sendInviteToGroup(not);
                            }
                            amountCompleted++;
                        }
                        break;
                    case "weightLoss":
                        if (weightLost >= g.getStartTargetVal()) {
                            for (User notifyUser : this.userList) {
                                String userName = user.getFirstName() + " " + user.getLastName();
                                Notification not = new Notification(user.getID(), notifyUser.getID(), this.getGroupID(), userName + " has completed the goal " + g.getName());
                                DatabaseAccessor.sendInviteToGroup(not);
                            }
                            amountCompleted++;
                        }
                        break;
                    case "weightGain":
                        if (weightGain >= g.getStartTargetVal()) {
                            for (User notifyUser : this.userList) {
                                String userName = user.getFirstName() + " " + user.getLastName();
                                Notification not = new Notification(user.getID(), notifyUser.getID(), this.getGroupID(), userName + " has completed the goal " + g.getName());
                                DatabaseAccessor.sendInviteToGroup(not);
                            }
                            amountCompleted++;
                        }
                        break;
                }

            }
            if (amountCompleted == this.userList.size()) {
                g.setCompleted();
                DatabaseAccessor.setGroupGoalCompleted(this.groupId, g);
            }
            double y = userList.size();
            double x = amountCompleted;
            g.setPercentageCompleted((x / y) * 100);
        }
    }
}
