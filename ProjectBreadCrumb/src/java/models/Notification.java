package models;

public class Notification {
    
    private String content;
    private int senderId, recieverId, groupId, notificationId;
    
   public  Notification(int senderId, int recieverId, int groupId, String content, int notificationId){
       this.senderId=senderId;
       this.recieverId=recieverId;
       this.groupId=groupId;
       this.content=content;
       this.notificationId = notificationId;
   }

   public  Notification(int senderId, int recieverId, int groupId, String content){
       this.senderId=senderId;
       this.recieverId=recieverId;
       this.groupId=groupId;
       this.content=content;
   }
   
    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }   
   
    public String getContent() {
        return content;
    }

    public int getSenderId() {
        return senderId;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setSenderId(int senderId) {
        this.senderId = senderId;
    }

    public void setRecieverId(int recieverId) {
        this.recieverId = recieverId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getRecieverId() {
        return recieverId;
    }

    public int getGroupId() {
        return groupId;
    }
    
}
