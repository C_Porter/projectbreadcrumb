package models;

import java.util.Date;

public class Exercise {
    
    private String name, type, details;
    private float distance, time, calories, metValue;
    int exId,checked;//pkey
    private HealthOverview ho;
    private java.sql.Date dateAdded;
    
    public Exercise(){}
    
    public Exercise(String name, String type, String details, float distance, float time, HealthOverview h, int checked) {
        this.name = name;
        this.type = type;
        this.details = details;
        this.distance = distance;
        this.time = time;
        this.ho = h;
        java.util.Date utilDate = new java.util.Date();
        this.dateAdded  = new java.sql.Date(utilDate.getTime());
        this.checked=checked;
        
    }

    public Exercise(String name, String type, String details, float distance, float time, int checked) {
        this.name = name;
        this.type = type;
        this.details = details;
        this.distance = distance;
        this.time = time;
        java.util.Date utilDate = new java.util.Date();
        this.dateAdded  = new java.sql.Date(utilDate.getTime());
        this.checked=checked;
        
    }
    
    //Getters
    public String getName() {
        return name;
    }

    public java.sql.Date getDateAdded() {
        return dateAdded;
    }

    
    public String getType() {
        return type;
    }

    public String getDetails() {
        return details;
    }

    public float getDistance() {
        return distance;
    }

    public float getTime() {
        return time;
    }

    public float getCalories() {
        return calories;
    }

    public int getChecked() {
        return this.checked;
    }
  

   public double caloryCalc(){
       return ho.poundsToKgs()*this.metValue*(time/60);
   }
    
   public int getExId() {
        return exId;
    }

    public void setExId(int exId) {
         this.exId=exId;
    }
    
    public String toString(){
        StringBuilder str = new StringBuilder();
        str.append("Exercise Name: ").append(this.name).append("\n");
        str.append("Exercise Type: ").append(this.type).append("\n");
        str.append("Distance: ").append(this.distance).append("\n");
        str.append("Duration: ").append(this.time).append("\n");//duration
        str.append("Details: ").append(this.details).append("\n");
        return str.toString();
    }
}
