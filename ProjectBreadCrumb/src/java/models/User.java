package models;

import db.DatabaseAccessor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import db.DatabaseAccessor;
import java.util.Date;

public class User {

    private int id;
    private String firstName, lastName, bio, email;
    private ArrayList<Goal> goals;
    private ArrayList<Exercise> exercises;
    private HealthOverview healthOverview;

    private ArrayList<Consumable> tempMeal;
 
     public User(String firstName, String lastName, float height, float weight, String conversion) {
     this.firstName = firstName;
     this.lastName = lastName;
 
     goals = new ArrayList<>();
     exercises = new ArrayList<>();
     healthOverview = new HealthOverview(height, weight, conversion);

     tempMeal = new ArrayList<>();
     }

     public User(String firstName, String lastName) {
     this.firstName = firstName;
     this.lastName = lastName;
  
     goals = new ArrayList<>();
     exercises = new ArrayList<>();
     healthOverview = new HealthOverview();

     tempMeal = new ArrayList<>();
     }
 

    public User() {
    }

    /**
     * Constructor that also takes database primary key as id
     *
     * @param id database pKey
     * @param firstName
     * @param lastName
     * @param email
     */
    public User(int id, String email, String firstName, String lastName) throws ServletException {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        goals = new ArrayList<>();
        exercises = new ArrayList<>();
        healthOverview = new HealthOverview();// DatabaseAccessor.getHOByUserID(id);
        tempMeal = new ArrayList<>();

    }

    public User(int id, String email, String firstName, String lastName, String bio) throws ServletException {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        goals = new ArrayList<>();
        exercises = new ArrayList<>();
        healthOverview = new HealthOverview();
        tempMeal = new ArrayList<>();
        this.bio = bio;

    }

    //Getters

    public int getID() {
        return id;
    }
        
    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public HealthOverview getHO() {
        return this.healthOverview;
    }

    public String getBio() {
        return bio;
    }

    public ArrayList<Goal> getGoals() {

        return goals;
    }

    public ArrayList<Exercise> getExercises() {
        populateUser();
        return exercises;
    }

    public void addGoal(Goal goal) {
        this.goals.add(goal);
    }

    public void setId(int newId) {
        this.id = newId;
    }

    public void setHO(HealthOverview healthOverview) {
        this.healthOverview = healthOverview;
    }    
    
    
    //Exercises
    public void addExercise(Exercise exercise) {
        exercises.add(exercise);
    }

       public void populateUser() {

        try {
            this.exercises = DatabaseAccessor.getExerciseList(id);
            this.healthOverview = DatabaseAccessor.getHOByUserID(id);
            this.goals = DatabaseAccessor.getUserGoalsList(id);
            //meals to be added
        } catch (ServletException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateGoals() throws SQLException, ServletException {

        populateUser();
        //meals to be added

        for (Goal g : this.goals) {

            if (g.getGoalType().equals("distance") || g.getGoalType().equals("duration")) {

                for (Exercise ex : this.exercises) {

                    //CANT GET INTO THIS IF STATEMENT
                    if ((ex.getDateAdded().after(g.getDateCreated()) || ex.getDateAdded().equals(g.getDateCreated())) && ex.checked == 0 && ex.getDateAdded().before(g.getDate())) {

                        if ((DatabaseAccessor.getExNameFromId(g.getExType()).equals(ex.getType())) && g.getGoalType().equals("distance")) {
                            g.setTargetVal(-ex.getDistance());

                        } else if ((DatabaseAccessor.getExNameFromId(g.getExType()).equals(ex.getType())) && g.getGoalType().equals("duration")) {
                            g.setTargetVal(-ex.getTime());

                        }
                        DatabaseAccessor.updateGoalTarget(this.id, g);
                    }
                    if (g.getTargetVal() <= 0) {
                        DatabaseAccessor.setGoalCompleted(this.id, g);
                        //    g.setPercentageAchieved(100);
                    }

                }
                //same loop for meals

            }//plus or minus targetVal reading from their current weight (current weight when goal is set is stored in the goal object
            java.util.Date utilDate = new java.util.Date();
            java.sql.Date dateNow = new java.sql.Date(utilDate.getTime());
            if (this.healthOverview.getWeightChecked() == 0 && g.getCompleted() == 0 && dateNow.before(g.getDate())) {
                if (g.getGoalType().equals("weightLoss")) {
                    float weightLost = g.getCurrentWeight() - this.healthOverview.getCurrentWeight();
                    g.assignTargetVal(g.getStartTargetVal() - weightLost);
                    DatabaseAccessor.updateGoalTarget(this.id, g);
                    if (g.getCompleted() == 0 && this.healthOverview.getCurrentWeight() <= g.getCurrentWeight() - g.getStartTargetVal()) {
                        g.setCompleted();
                        DatabaseAccessor.setGoalCompleted(this.id, g);
                    }

                }
                if (g.getGoalType().equals("weightGain")) {
                    float weightGained = g.getCurrentWeight() - this.healthOverview.getCurrentWeight();
                    g.assignTargetVal(g.getStartTargetVal() + weightGained);
                    DatabaseAccessor.updateGoalTarget(this.id, g);
                    if (g.getCompleted() == 0 && this.healthOverview.getCurrentWeight() >= g.getCurrentWeight() + g.getStartTargetVal()) {
                        g.setCompleted();
                        DatabaseAccessor.setGoalCompleted(this.id, g);
                    }
                }
                if (g.getGoalType().equals("weightGain") || g.getGoalType().equals("weightLoss")) {
                    this.healthOverview.setWeightChecked(1);
                    DatabaseAccessor.persistHO(this);
                }
            }

            if (g.getGoalType().equals("calories")) {

                if (g.getCompleted() == 0 && this.healthOverview.getCurrentWeight() == g.getTargetVal()) {
                    g.setCompleted();

                    DatabaseAccessor.setGoalCompleted(this.id, g);

                }

            }

        }
        for (Exercise ex : this.exercises) {
            DatabaseAccessor.setExerciseChecked(this.id, ex);
            ex.checked = 1;
        }
    }

    public float percentageGoalCalc(Goal g) {
        populateUser();
        return g.calculatePercentageAchieved();
    }

}

