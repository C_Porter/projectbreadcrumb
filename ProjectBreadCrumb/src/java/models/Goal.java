package models;

import db.DatabaseAccessor;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;

public class Goal {

    //Target type is needed to know if they want to exercise for a certain
    //distance or time
    private String name, goalType;
    private float targetVal, startTargetVal, currentWeight;
    private  java.sql.Date targetDate;
    private java.sql.Timestamp dateCreated;
    private double percentageCompleted;

    private int completed, exType, goalId;

    public final static long MILLISECONDS_IN_DAY = 24 * 60 * 60 * 1000;

     public Goal(){}
    
    public Goal(String name, String goalType, float targetVal, java.sql.Date date,
            int exType, float currentWeight, java.sql.Timestamp dateCreated, int completed, float startTargetVal) {
        this.name = name;
        this.exType = exType;
        //Will minus from target val, when targetVal <=0, goal reached
        this.targetVal = targetVal;
        this.startTargetVal = startTargetVal;

        this.targetDate = date;
        this.dateCreated = dateCreated;
        this.goalType = goalType;
        this.currentWeight = currentWeight;
        this.completed = completed;

        this.percentageCompleted=0;
    }
    
    //for group goal id checking
    public Goal(String name, int goalId, java.sql.Timestamp dateCreated){
        this.name = name;
        this.goalId = goalId;
        this.dateCreated = dateCreated;        
    }

   
    
    public float calculatePercentageAchieved(){
        if(targetVal < startTargetVal && completed ==0){
       return 100 -((targetVal/startTargetVal) * 100);
        }
        else if(completed ==1){
            return 100;
            
        }
        return 0;
         
     }
    
    public void setPercentageCompleted(double val){
        percentageCompleted=val;
    }

    
    public int differenceInDays() {
        java.util.Date utilDate = new java.util.Date();
        java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());
        return (int) ((targetDate.getTime() - currentDate.getTime()) / MILLISECONDS_IN_DAY);
    }

    public int getExType() {
        return exType;
    }

    public String getName() {
        return name;
    }

    public java.sql.Timestamp getDateCreated() {
        return dateCreated;
    }

    public float getCurrentWeight() {
        return this.currentWeight;
    }

    public String getGoalType() {
        return goalType;
    }

    public float getStartTargetVal() {
        return startTargetVal;
    }

    public float getTargetVal() {
        return targetVal;
    }

    public void setTargetVal(float val) {
        this.targetVal += val;
    }
    
    public void assignTargetVal(float val){
        this.targetVal = val;
    }

    public java.sql.Date getDate() {
        return targetDate;
    }

    public int getCompleted() {
        return completed;
    }

    public void setCompleted() {
        completed = 1;

    }

    public int getGoalId() {
        return goalId;
    }

    public void setGoalId(int goalId) {
        this.goalId = goalId;
    }

    public boolean isGoalValMet() {
        // if tagret val less than or equal 0, target is met
        return targetVal <= 0;
    }

    public boolean isGoalDateMet() {
        //If target met before date set or on set date
        if (this.dateCreated.before(targetDate)
                || this.dateCreated.equals(targetDate) && isGoalValMet()) {
            return true;
        } else {
            return false;
        }
    }

    public double getPercentageCompleted() {
        return percentageCompleted;
    }
    
    

    /*
     Display on profile page under goals
     */

    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("Goal Name: ").append(this.name).append("\n");
        str.append("Goal Type: ").append(this.goalType).append("\n");
        try {
            str.append("Exercise Type: ").append(DatabaseAccessor.getExNameFromId(exType)).append("\n");
        } catch (ServletException ex) {
            Logger.getLogger(Goal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Goal.class.getName()).log(Level.SEVERE, null, ex);
        }
        str.append("Target Value: ").append(this.targetVal).append("\n");
        str.append("Target Date: ").append(this.targetDate).append("\n");//duration
        return str.toString();
    }
}
