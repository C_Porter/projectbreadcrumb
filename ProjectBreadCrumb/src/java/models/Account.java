package models;
import java.sql.Connection;
import db.DatabaseAccessor;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;

public class Account {

    private String email, password;
    private boolean isAdmin;
    private User user;

    public Account(){}
    
    public Account(
            String email, String password, String firstName, String lastName, float height, float weight, String conversion) {
        this.email = email;
        this.password = password;
        this.user = new User(firstName, lastName,height, weight, conversion);
       
    }
    
     public Account(
            String email, String password, int isadmin, String firstName, String lastName) {
        this.email = email;
        this.password = password;
        this.isAdmin = isadmin == 1;
        this.user = new User(firstName, lastName);
        
    }

    public User getUser() {
        return user;
    }
    
    public String getEmail() {
        return email;
    }
    
    public boolean getAdmin() {
       return isAdmin;
   }

    
     public String getPassword() {
        return this.password;
    }

}
