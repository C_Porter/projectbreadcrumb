/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import db.DatabaseAccessor;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.*;

@WebServlet(urlPatterns = {"/CreateGroupController"})
public class CreateGroupController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = (String) request.getSession().getAttribute("email");
        User user = DatabaseAccessor.getUserByEmail(email);
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String groupName = request.getParameter("groupName");
        String groupDetails = request.getParameter("groupDetails");

        ArrayList<UserGroups> yourGroupList = DatabaseAccessor.getYourGroups(user.getID());

        boolean alreadyExists = false;
        for (UserGroups ug : yourGroupList) {
            if (ug.getGroupName().equals(groupName)) {
                alreadyExists = true;
            }
        }

        if (!alreadyExists) {
            try {
                DatabaseAccessor.persistGroup(user.getID(), groupName, groupDetails);
                UserGroups addMemberTo = DatabaseAccessor.getGroupByNameAndAdminId(groupName, user.getID());
                DatabaseAccessor.addUserToGroup(user.getID(), addMemberTo.getGroupID());
            } catch (SQLException ex) {
                Logger.getLogger(CreateGroupController.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.getRequestDispatcher("/YourGroupsController").forward(request, response);
        }else{
        request.setAttribute("errorGroupCreationMessage", "You have already created a group with this name");
        }
        request.getRequestDispatcher("YourGroups.jsp").forward(request, response);
        out.close();
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
