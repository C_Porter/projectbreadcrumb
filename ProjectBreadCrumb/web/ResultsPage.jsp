<%-- 
    Document   : ResultsPage
    Created on : 20-Apr-2015, 11:22:25
    Author     : qrk13cdu
--%>

<%@page import="models.User"%>
<%@page import="models.UserGroups"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" media="all" href="Main.css">
        <link rel="stylesheet" type="text/css" media="all" href="GoalPage.css">
        <title>Search Results</title>
    </head>
    <body>

        <div id="bannerWrapper">
            <div id="bannerContent">        
                <div style="float:left">
                    <a href="Profile.jsp"><h1>Health Tracker Project</h1></a>
                </div>
                <div id="logoutPos">
                    <form action="LogOutController" method="POST">
                        <input type ="submit" value="Log Out" id="LogoutButton" />
                    </form>
                </div>
                <div id="searchBoxPos">
                    <form action="SearchController" method="POST">                
                        <input id="searchBox" name="query" placeholder="Search"/>
                    </form>
                </div>
            </div>
        </div>
        
        <div id="contentWrapper">
            
            <%
                String searchFor = (String) request.getSession().getAttribute("searchFor");
                if(searchFor.equals("groups")) {
                    
                    out.println("<div id=\"activeGoalListHeader\">");
                        out.println("<div id=\"sectionTitle\" style=\"padding:10px;\">Group Results</div>");
                    out.println("</div>");
                    
                    out.println("<div id=\"activeGoalList\">"); 
            
                    ArrayList<UserGroups> groups = (ArrayList<UserGroups>) request.getSession().getAttribute("groupResults");

                    if (groups.size() == 0) {
                        out.println("<div class=\"noGoalContent\">No results found</div>");
                    }

                    int j = 0;
                    for (int i = groups.size() - 1; i >= 0; i--) {
                        out.println("<form action=\"ProcessSearchResult"
                            + "\" method=\"get\">");
                        if (j % 2 == 0) {
                            out.println("<div class=\"listWrapper1\">");
                            out.println("<div class=\"listDiv1\"><p>" + groups.get(i).getGroupID() + "</p></div>");
                            out.println("<button name=\"selectedResult\" type=\"submit\" class=\"button\" value=\"" + i + "\">" + groups.get(i).getGroupName() + "</button>");
                            out.println("</div>");
                        } else {
                            out.println("<div class=\"listWrapper2\">");
                            out.println("<div class=\"listDiv2\"><p>" + groups.get(i).getGroupID() + "</p></div>");
                            out.println("<button name=\"selectedResult\" type=\"submit\" class=\"button\" value=\"" + i + "\">" + groups.get(i).getGroupName() + "</button>");
                            out.println("</div>");
                        }
                        j++;
                        out.println("</form>");
                    }
                    out.println("</div>");
                    
                } else {
                    
                    out.println("<div id=\"activeGoalListHeader\">");
                        out.println("<div id=\"sectionTitle\" style=\"padding:10px;\">User Results</div>");
                    out.println("</div>");

                    out.println("<div id=\"activeGoalList\">");
                    ArrayList<User> users = new ArrayList<User>();

                    if (request.getSession().getAttribute("userResults") != null) {
                        users = (ArrayList<User>) request.getSession().getAttribute("userResults");
                        if (users.size() == 0) {
                            out.println("<div class=\"noGoalContent\">No Users found</div>");
                        }
                    }

                    int j = 0;
                    for (int i = 0; i < users.size(); i++) {
                        out.println("<form action=\"InviteUserToGroupController"
                            + "\" method=\"get\">");
                        if (j % 2 == 0) {
                            out.println("<div class=\"listWrapper1\">");
                            out.println("<div class=\"listDiv1\"><p>" + (i+1) + "</p></div>");
                            out.println("<div class=\"listDiv1\"><p>" + users.get(i).getFirstName() +" " + users.get(i).getLastName() + "</p></div>");
                            out.println("<button name=\"selectedResult\" type=\"submit\" class=\"button\" value=\"" + i + "\">Invite</button>");
                            out.println("</div>");
                        } else {
                            out.println("<div class=\"listWrapper2\">");
                            out.println("<div class=\"listDiv2\"><p>" + (i+1) + "</p></div>");
                            out.println("<div class=\"listDiv2\"><p>" + users.get(i).getFirstName() +" " + users.get(i).getLastName() + "</p></div>");
                            out.println("<button name=\"selectedResult\" type=\"submit\" class=\"button\" value=\"" + i + "\">Invite</button>");
                            out.println("</div>");
                        }
                        j++;
                        out.println("</form>");
                    }
                    out.println("</div>");
                }
                    %>
        </div>
    </body>
</html>
