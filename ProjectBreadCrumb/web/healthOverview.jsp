<%@page import="models.Consumable"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.ArrayList"%>
<%@page import="db.DatabaseAccessor"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="models.User" %>
<%@page import="java.util.Random;"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>            

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" media="all" href="Main.css">
        <link rel="stylesheet" type="text/css" media="all" href="ProfilePage.css">
        <link rel="stylesheet" type="text/css" media="all" href="healthOverviewPage.css">
        <title>Health Overview</title> 
        <script src="//www.chartjs.org/assets/Chart.min.js">
        </script>
    </head>
    <!--Java block for security. Checking if there is a session and deleting
    any cached version of the page-->
    <%

        String email = (String) request.getSession().getAttribute("email");

        User user = DatabaseAccessor.getUserByEmail(email);
        //user.populateUser();

    %>
    <body>
        <div style="min-width: 1350px; margin: 0 auto;">
            <div id="bannerWrapper">
                <div id="bannerContent">        
                    <div style="float:left">
                        <a href="Profile.jsp"><h1>Health Tracker Project</h1> </a>
                    </div>
                    <div id="logoutPos">
                        <form action="LogOutController" method="POST">
                            <input type ="submit" value="Log Out" id="LogoutButton" />
                        </form>
                    </div>
                    <div id="searchBoxPos">
                        <form action="SearchController" method="POST">                
                            <input id="searchBox" name="query" placeholder="Search"/>
                        </form>
                    </div>

                </div>
            </div>

            <div id="profileWrapper">

                <div id="userDetails">
                    <div id="healthTitle">Health Overview</div>



                    <!--Outputs account updated message-->
                    <%  String accountEdited = (String) request.getAttribute("accountEditedMessage");
                        if (accountEdited != null) {
                            out.println(accountEdited);
                        }
                    %>




                    <div id="edit">
                        <form action="UpdateHealthOverviewController" method="POST">
                            <input type="number" id="email" minLength="2" placeholder="Current Weight" name="weight" class="inputBox"  onkeypress = "return isNumber(event)"/> 
                           

                           
                            <input type ="submit" value="Update" class="linkButton" />

                        </form>    
                    </div>
                           <script>
                       
                                function isNumber(evt) {
                                    evt = (evt) ? evt : window.event;
                                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                                    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                                        return false;
                                    }
                                    return true;
                                }
                    </script>



                </div>

                <marquee behavior="scroll" direction="left" LOOP=1><p class="banner"> You have  
                        <%if (user.getGoals().size() > 0) {
                                out.println(user.getGoals().get(user.getGoals().size() - 1).differenceInDays());
                            } else {
                                out.println(user.getGoals().size());
                            }

                        %> 

                        days left to complete your most recent goal.</p></marquee>
                <div id="WBT">
                    <div id="Goals">
                        <div id="subSectionTitle">Weight </div>
                        <p class="normal">  <%=user.getHO().getCurrentWeight()%> 
                            <%
                                if (user.getHO().getConversion().equals("imperial")) {
                                    out.println("lbs");
                                } else if (user.getHO().getConversion().equals("metric")) {
                                    out.println("kgs");
                                }
                            %> </p> 

                    </div>
                    <div id="Diet">
                        <div id="subSectionTitle">BMI Range</div>
                        <%
                            if (user.getHO().getBmi() < 18.5) {
                                out.println("<p class = red>UNDERWEIGHT</p>");
                            } else if (user.getHO().getBmi() < 24.99) {
                                out.println("<p class = green>HEALTHY WEIGHT</p>");
                            } else if (user.getHO().getBmi() < 29.99) {
                                out.println("<p class = orange>OVER WEIGHT</p>");
                            } else if (user.getHO().getBmi() > 30) {
                                out.println("<p class = red>OBESE</p>");
                            }
                        %> 
                    </div>
                    <div id="Exercise">
                        <div id="subSectionTitle">BMI</div>
                        <p class="normal">  <%=user.getHO().getBmi()%> </p>
                    </div> 
                </div>

                <marquee behavior="scroll" direction="right" LOOP=1><p class="banner">Please keep your weight up to date.</p></marquee>
                <div id="Graph">

                    <div id="legend"> <p class="BMI">BMI</p><p class="Weight">Weight</p></div>
                    <div id='d2' style="padding-left: 25px;">
                        <canvas id='updating-chart' width ="650" height = "250"></canvas>      
                    </div>

                    <p style="text-align: center;">Weight update</p> 
                </div>
                <%
                    float[] weightdata = DatabaseAccessor.getUserWeightHist(user.getID());
                    float[] Bmi = new float[7];

                    for (int i = 0; i < weightdata.length - 1; i++) {
                        if (user.getHO().getConversion().equals("imperial")) {
                            Bmi[i] = (weightdata[i] / (user.getHO().getHeight() * user.getHO().getHeight())) * 703;

                        } else {
                            Bmi[i] = (weightdata[i] / (user.getHO().getHeight() * user.getHO().getHeight())) *10000;

                        }
                    }


                %>


                <script>

                    // var myCanvas = document.getElementById('updating-chart');
                    var weight = new Array();
                    var Bmi = new Array();


                    weight = ["<%=weightdata[0]%>", "<%=weightdata[1]%>", "<%=weightdata[2]%>", "<%=weightdata[3]%>", "<%=weightdata[4]%>", "<%=weightdata[5]%>", "<%=weightdata[6]%>"];
                    Bmi = ["<%=Bmi[0]%>", "<%=Bmi[1]%>", "<%=Bmi[2]%>", "<%=Bmi[3]%>", "<%=Bmi[4]%>", "<%=Bmi[5]%>", "<%=Bmi[6]%>"];


                    var canvas = document.getElementById('updating-chart'),
                            ctx = canvas.getContext('2d'),
                            startingData = {
                                labels: [1, 2, 3, 4, 5, 6, 7],
                                datasets: [
                                    {
                                        label: "BMI",
                                        fillColor: "rgba(179,79,36,0.2)",
                                        strokeColor: "rgba(179,79,36,,1)",
                                        pointColor: "rgba(179,79,36,1)",
                                        pointStrokeColor: "#fff",
                                        data: Bmi
                                    },
                                    {
                                        label: "Weight",
                                        fillColor: "rgba(76,186,194,0.2)",
                                        strokeColor: "rgba(76,186,194,1)",
                                        pointColor: "rgba(76,186,194,1)",
                                        pointStrokeColor: "#fff",
                                        data: weight
                                    }
                                ]
                            },
                    latestLabel = startingData.labels[6];
                    var myLiveChart = new Chart(ctx).Line(startingData);

                </script>
            </div>    
        </div>
    </body>
</html>
