<%-- 
    Document   : GoalInformation
    Created on : 21-Apr-2015, 21:33:21
    Author     : akc13dyu
--%>
<%@page import="java.util.ArrayList"%>
<%@page import="models.Exercise"%>
<%@page import="models.User"%>
<%@page import="models.Goal"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="db.DatabaseAccessor"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    
    <%
        String email = (String) request.getSession().getAttribute("email");
        User user = DatabaseAccessor.getUserByEmail(email);
        String goalExercise = (String) request.getSession().getAttribute("goalExercise");
    %>
        
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" media="all" href="Main.css">
        <link rel="stylesheet" type="text/css" media="all" href="GoalPage.css">
        <link rel="stylesheet" type="text/css" media="all" href="Exercisepage.css">
    </head>
    <body>
        
        <div id="exerciseListHeader">
                <div id="sectionTitle" style="padding:10px;">Contributing Exercises</div>
                <div id="listTitleWrapper">
                    <!--Name-->
                    <div class="listTitle"><p>Name</p></div>
                    <!--Type-->
                     <div class="listTitle"><p>Type</p></div>
                    <!--Distance-->
                     <div class="listTitle"><p>Distance</p></div>
                    <!--Duration-->
                     <div class="listTitle"><p>Duration</p></div>
                    <!--Notes-->
                     <div class="listTitle"><p>Notes</p></div>
                    
                </div>
            </div>
            <div id="exerciseList">  
                <%
                        ArrayList<Exercise>exList = DatabaseAccessor.getExerciseList(user.getID());      
                        
                        if(exList.size() == 0) {
                            out.println("<div class=\"noGoalContent\">No Exercises for this goal added yet</div>");
                        }
                        int counter = 0;
                        int j = 0;
                        for(int i = exList.size()-1; i >= 0; i--) {
                            
                            if(exList.get(i).getType().equals(goalExercise)) {
                                if(j % 2 == 0) {
                                    out.println("<div class=\"listWrapper1\">");
                                        out.println("<div class=\"listDiv1\"><p>"+exList.get(i).getName()+"</p></div>");
                                        out.println("<div class=\"listDiv1\"><p>"+exList.get(i).getType()+"</p></div>");
                                        out.println("<div class=\"listDiv1\"><p>"+exList.get(i).getDistance()+"</p></div>");
                                        out.println("<div class=\"listDiv1\"><p>"+exList.get(i).getTime()+"</p></div>");
                                        out.println("<div class=\"listDiv1\"><p>"+exList.get(i).getDetails()+"</p></div>");
                                    out.println("</div>");
                                }else {
                                    out.println("<div class=\"listWrapper2\">");
                                        out.println("<div class=\"listDiv2\"><p>"+exList.get(i).getName()+"</p></div>");
                                        out.println("<div class=\"listDiv2\"><p>"+exList.get(i).getType()+"</p></div>");
                                        out.println("<div class=\"listDiv2\"><p>"+exList.get(i).getDistance()+"</p></div>");
                                        out.println("<div class=\"listDiv2\"><p>"+exList.get(i).getTime()+"</p></div>");
                                        out.println("<div class=\"listDiv2\"><p>"+exList.get(i).getDetails()+"</p></div>");
                                    out.println("</div>");
                                }
                                j++;
                                counter++;
                            }
                            if(counter == 0) {
                            out.println("<div class=\"noGoalContent\">No Exercises for this goal added yet</div>");
                        }
                        }
                        
                %>
            </div>  
    </body>
</html>
