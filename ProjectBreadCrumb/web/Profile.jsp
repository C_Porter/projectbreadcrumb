
<%@page import="java.text.DecimalFormat"%>
<%@page import="models.Notification"%>
<%@page import="models.Consumable"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.ArrayList"%>
<%@page import="db.DatabaseAccessor"%>
<%@page import="models.Exercise"%>
<%@page import="models.Goal"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="models.User" %>
<%@page import="models.Account" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>            

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" media="all" href="Main.css">
        <link rel="stylesheet" type="text/css" media="all" href="ProfilePage.css">
        <title>Profile Page</title>  
        <!-- JavaScript for text area counter-->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="//www.chartjs.org/assets/Chart.min.js">
        </script>
   





    </head>

    <style>
        #editDiv{

            width: 100%;
            height: 100%;
            font-family: Geo Sans Light; 
            background-color: #FFFFFF;
            position: absolute;
            left: 0px;
            top: 0px;
            z-index: 1;
            border-radius: 6px;
        }



    </style>

    <!--Java block for security. Checking if there is a session and deleting
    any cached version of the page-->
    <%
        //If no session exists, go to index page
        if (request.getSession().getAttribute("email") == null) {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }

        //clearing the cached page, preventing users from hitting backspace
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
    %>
    <!--Java block to retrieve current session-->
    <%
        String email = (String) request.getSession().getAttribute("email");
        User user = DatabaseAccessor.getUserByEmail(email);
        Account acc = DatabaseAccessor.getAccountByEmail(user.getEmail());

    %>


    <%    //        String pictureSource = "c:\\apache-tomcat-5.5.29\\webapps\\data\\" + user.getID() + ".jpg";
        //        File f = new File(pictureSource);
        //
        //        if (f.exists()) {
        //            pictureSource = "c:\\apache-tomcat-5.5.29\\webapps\\data\\" + user.getID() + ".jpg";
        //        } else {
        //            pictureSource = "c:\\apache-tomcat-5.5.29\\webapps\\data\\default.jpg";
        //        }

    %>

    <body>


        <div style="min-width: 1500px; ">
            <div id="bannerWrapper">
                <div id="bannerContent">        
                    <div style="float:left">
                        <h1>Health Tracker Project</h1>
                    </div>
                    <div id="logoutPos">
                        <form action="LogOutController" method="POST">
                            <input type ="submit" value="Log Out" id="LogoutButton" />
                        </form>
                    </div>
                    <div id="searchBoxPos">
                        <form action="SearchController" method="POST">                
                            <input id="searchBox" name="query" placeholder="Search"/>
                        </form>
                    </div>
                </div>
            </div>

            <div id="profileWrapper">
                <!--User Details -->
                <div id="userDetails">         
                    <!--Outputs account updated message-->
                    <%  String accountEdited = (String) request.getAttribute("accountEditedMessage");
                        if (accountEdited != null) {
                            out.println(accountEdited);
                        }
                    %>
                    <div id="profilePicture"></div>
                    <div id="nameDetails">
                        <p id="userName"><%=user.getFirstName()%> <%=user.getLastName()%></p>
                        <p id="bio"><%=user.getBio()%></p>
                    </div>
                    <div id="edit">




                        <div class="button">    <button id ="editAccountBut" class="manageButton">Edit Account</button></div>

                        <form action="YourGroupsController">
                            <div class="button">   <input type="submit" class="manageButton" value="Groups"></div>
                        </form> 

                        <%
                            if (acc.getAdmin() == true) {
                                out.println("<form action=\"Admin.jsp\">");
                                out.println("<input type=\"submit\" class=\"manageButton\" value=\"Admin\">");
                                out.println("</form>");
                            }
                        %>
                    </div>
                    <div id="bg">
                        <div id="search-container">
                            <div id="search-bg"></div>
                            <div id="search">
                                <!--    **************Edit account form ****************** -->

                                <form action="EditAccountController" method="POST"> 
                                    <div class="inputRegisterInfo">
                                        <input class="inputbox" type ="text" id="firstName" name ="firstName" placeholder="First Name"/>


                                        <input class="inputbox" type ="text" id="lastName" name ="lastName" placeholder="Surname"/>

                                        <input class="inputbox" type ="password" id="password" name ="inputPassword" placeholder="Password"/>


                                        <input class="inputbox" type ="text" id="lastName" name ="bio" placeholder="Bio"/>

                                        <input class="inputbox" type ="text" id="lastName" name ="height" placeholder="Height"/>

                                    <input class="inputbox" type ="text" id="lastName" name ="picPath" placeholder="Picture Path"/>

                                        <input class="updateButton" type ="submit" value="Update"  />   

                                    </div>
                                </form>               


                                <h5>edit your account </h5>

                            </div>
                        </div>
                    </div>
                </div>         


                <marquee behavior="scroll" direction="left" LOOP=1><p class="banner">Please enter your height in edit account.</p></marquee> 
                <!--Health Overview-->

                <div id="HeathOverview">



                    <div id="sectionTitle"><a href="healthOverview.jsp" style="text-decoration: none; color: #018dc4;">Health Overview</a></div>      


                    <%
                        float[] weightdata = DatabaseAccessor.getUserWeightHist(user.getID());
                    %>


                    <canvas id='updating-chart' width ="650" height = "200"></canvas>  
                    Weight

                </div>  
                <!--Goals Exercises Diet-->
                <div id="GED">
                    <div id="Goals">

                        <% ArrayList<Goal> goalsList = DatabaseAccessor.getUserGoalsList(user.getID());%>
                        <div id="subSectionTitle"><%
                            if (goalsList.size() != 0) {
                                out.println(goalsList.get(goalsList.size() - 1).getName());
                            }%> Goal</div>

                        <%

                            if (goalsList.size() == 0) {
                                out.println("<div class=\"noContent\">No Goals</div>");
                            } else {
                                double percentage = user.percentageGoalCalc(goalsList.get(goalsList.size() - 1));
                                DecimalFormat df = new DecimalFormat("#.0");
                                out.println("<div class=\"percentage\">" + df.format(percentage) + "%" + "</div>" + "<div class=\"noContent\">complete</div>");
                            }

                            int counter = 0;
//                        for(int i = goalsList.size()-1; i >= 0; i--) {
//                            
//                            if(counter < 3) {
//                            out.println("<div class=\"listWrapper\">");                         
//                                    out.println("<div class=\"listName\"><p>"+goalsList.get(i).getName()+"</p></div>");
//                                    out.println("<div class=\"listType\"><p>"+goalsList.get(i).getGoalType()+"</p></div>");
//                            out.println("</div>");
//                            }                        
//                            counter ++;
//                            
                            //     }

                        %>

                        <div id="manage">
                            <form action="Goals.jsp">

                                <input type="submit" class="manageButton" value="Manage Goals">
                            </form> 
                        </div>
                    </div>
                    <div id="Diet">
                        <div id="subSectionTitle">Diet</div>


                        <%           Date dNow = new Date();
                            ArrayList<Consumable> meals = DatabaseAccessor.getMealbyDate(user.getID(), dNow);

                            if (meals.size() == 0) {
                                out.println("<div class=\"noContent\">No Entries</div>");
                            } else {

                                int fats = 1;
                                int protein = 1;
                                int carbs = 1;

                                for (int i = 0; i < meals.size(); i++) {
                                    fats += meals.get(i).getSatFat();
                                    protein += meals.get(i).getProtein();
                                    carbs += meals.get(i).getCarbs();%>

                        <script>
            var carbs = <%=carbs%>;
            var prot = <%=protein%>;
            var fat = <%=fats%>;
                        </script>

                        <div id='d2' style="padding-left:10%; padding-top:15px;">
                            <canvas id="pieChartLoc" height="200" width="200"></canvas>
                        </div>

                        <%   }

                            }

                        %>

                        <script>



                            var pieChartData = [
                                {
                                    value: carbs,
                                    color: "lightblue",
                                    label: 'Carbs',
                                    labelColor: 'black',
                                    labelFontSize: '16'

                                },
                                {
                                    value: prot,
                                    color: "green",
                                    label: 'Protein',
                                    labelColor: 'black',
                                    labelFontSize: '16'
                                },
                                {
                                    value: fat,
                                    color: "red",
                                    label: 'Fats',
                                    labelColor: 'black',
                                    labelFontSize: '16'
                                }

                            ];
                            var myLine = new Chart(document.getElementById("pieChartLoc").getContext("2d")).Pie(pieChartData);
                        </script>
                        <div id="manage">
                            <form action="Diet.jsp">
                                <input type="submit" class="manageButton" value="Manage diet">
                            </form> 
                        </div>
                    </div>

                    <div id="Exercise">
                        <div id="subSectionTitle">
                            Last 3 Exercises
                        </div>
                        <%                            ArrayList<Exercise> exList = DatabaseAccessor.getExerciseList(user.getID());

                            if (exList.size() == 0) {
                                out.println("<div class=\"noContent\">No Exercises</div>");
                            }

                            counter = 0;
                            for (int i = exList.size() - 1; i >= 0; i--) {

                                if (counter < 3) {
                                    out.println("<div class=\"listWrapper\">");
                                    out.println("<div class=\"listName\"><p>" + exList.get(i).getName() + "</p></div>");
                                    out.println("<div class=\"listType\"><p>" + exList.get(i).getType() + "</p></div>");
                                    out.println("</div>");
                                }
                                counter++;

                            }
                        %>

                        <div id="manage">
                            <form action="Exercise.jsp">
                                <input type="submit" class="manageButton" value="Manage Exercises">
                            </form>                      
                        </div>                        
                    </div>
                </div>
            </div>

            <div class="icon">
           <div class="type">Notification Center<br>
                    <%
                        ArrayList<Notification> notfications = DatabaseAccessor.getNotifications(user.getID());
                        for (Notification nf : notfications) {
                            out.println("<form action=\"GroupPage.jsp" //button?
                                    + "\" method=\"get\">");
                            out.println(nf.getContent());
                                     out.println("<br>");
                            out.println("<button class=\"myButtontick\"  name=\"selectedResult\"  type=\"submit\" value=\"" + nf.getGroupId() + "\">✓</button>");
                            request.getSession().setAttribute("selectedGroup", DatabaseAccessor.getGroupByGroupId(nf.getGroupId()));
                            out.println("</form>");
                            out.println("<form action=\"NotificationController" //button?
                                    + "\" method=\"get\">");
                            
                            out.println("<button class=\"myButton\" name=\"notificationId\" type=\"submit\" value=\"" + nf.getNotificationId() + "\">X</button>");                            
                            out.println("</form>");
                            out.println("<br>");
                            
                        }

                        for (Goal g : goalsList) {
                              if ( g.differenceInDays() == 0 && g.getCompleted() == 0) {
                                out.println(g.getName() + " has less than a day to complete!");
                                out.println("<br>");
                            }
                           else if (g.differenceInDays() == 2 && g.getCompleted()==0) {
                                out.println(g.getName() + " only has 2 days left to complete!");
                                out.println("<br>");
                            }

                        }
                    
                    %>



                </div>
            </div>


        </div>

    </body>
    
    <script>
        $(function() {

            $("#radio").buttonset();

        });


    </script>

    <script type="text/javascript">

        (function($) {
            $(".type").hover(function(e) {
                e.stopPropagation();
                $('.type')
                        .stop()
                        .animate(
                                {"margin-top": (
                                            $(this).parent().outerHeight() -
                                            $(this).outerHeight()
                                            )},
                        250
                                );
                return false;
            }, function(e) {
                e.stopPropagation();
                console.log("hover out");
                $('.type').stop().animate(
                        {"margin-top": "50px"},
                250
                        );
                return false;
            });
        }(jQuery));



        $("#editAccountBut").click(function() {
            $("#bg").toggle();
        });     
            </script>


    <script>

        // var myCanvas = document.getElementById('updating-chart');
        var weight = new Array();
        weight = ["<%=weightdata[0]%>", "<%=weightdata[1]%>", "<%=weightdata[2]%>", "<%=weightdata[3]%>", "<%=weightdata[4]%>", "<%=weightdata[5]%>", "<%=weightdata[6]%>"];
        var canvas = document.getElementById('updating-chart'),
                ctx = canvas.getContext('2d'),
                startingData = {
                    labels: [1, 2, 3, 4, 5, 6, 7],
                    datasets: [
                        {
                            label: "Weight",
                            fillColor: "rgba(76,186,194,0.2)",
                            strokeColor: "rgba(76,186,194,1)",
                            pointColor: "rgba(76,186,194,1)",
                            pointStrokeColor: "#fff",
                            data: weight
                        }
                    ]
                };

        latestLabel = startingData.labels[6];
        var myLiveChart = new Chart(ctx).Line(startingData);

        // Reduce the animation steps for demo clarity.






        /*         setInterval(function() {
         // Add two random numbers for each dataset
         myLiveChart.addData([Math.random() * 100, Math.random() * 100], ++latestLabel);
         // Remove the first point so we dont just add values forever
         myLiveChart.removeData();
         }, 5000);
         */
    </script>
</html>

