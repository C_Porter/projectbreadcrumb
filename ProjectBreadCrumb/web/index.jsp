<%@page import="db.DatabaseAccessor"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@ page import="java.io.*,java.util.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>


<!DOCTYPE html>


<html>

    <head>
        <title>Health Tracker</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" media="all" href="LoginStyleSheet.css">
        <link rel="stylesheet" type="text/css" media="all" href="Main.css">

        <!-- JavaScript for text area counter-->


        <!-- MAIN COPY-->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" ></script>

    </head>
    <body>
        <!--        <div style="background-color: #cc00cc" width="100%" height="100%">-->
        <div id="bannerWrapper">
            <div id="bannerContent">  
                <div id="bannerTitle">
                    <h1>Health Tracker Project</h1>
                </div>
                <div id="bannerLinks">
                    <div class="linkWrap"><a class="link" href="#about">About</a></div>
                    <div class="linkWrap"><a class="link" href="#team">Team</a></div>
                    <div class="linkWrap"><a class="link" href="#contact">Contact</a></div>
                    <div class="linkWrap"><a class="link" href="#terms">Terms</a></div>
                </div>
            </div>
        </div>
        <div id="contentWrapper">

            <%--Login Area--%>    
            <div id="loginBox" align="center">   
                <p style="font-size: 25px; color: #FFFFFF;">Have an Account?</p>
                <form action="LogInController" method="POST">
                    <div class="inputEmailBox">                   
                        <input class="textField" input type="email" required  id="email" name ="inputEmail" placeholder="Email Address" required/>
                    </div>
                    <div class="inputPasswordBox">
                        <input class="textField" type="password" required id="password" name="inputPassword" placeholder="Password" minlength=8 required/>
                    </div>
                    <div class="inputbox">                   
                        <input class="linkButton" type ="submit" value="Log in"  />
                    </div>
                </form>
                <a href="#" id="register" style="color: #FFFFFF; font: Arial">Register</a>
                <div class="errorMessage">
                    <%                        String error = (String) request.getAttribute("errorMessage");
                        if (error != null) {
                            out.println(error);
                        }
                    %>


                </div>
                <div class="accountCreatedMessage">
                    <%
                        String accountCreated = (String) request.getAttribute("accountCreatedMessage");
                        if (accountCreated != null) {
                            out.println(accountCreated);
                        }
                    %>
                </div>
            </div>

            <%--Register Area--%>
            <div id="createAccount" align="center">   
                <p style="font-size: 25px; color: #FFFFFF;">Want to Register?</p>
                <form action="CreateAccountController" method="POST">                
                    <div class="inputRegisterInfo">
                        <input class="textField" type ="text" required id="firstName" name ="firstName" placeholder="First Name" required/>
                    </div>
                    <div class="inputRegisterInfo">
                        <input class="textField" type ="text" required id="lastName" name ="lastName" placeholder="Surname" required/>
                    </div>
                    <div class="inputRegisterInfo">
                        <input class="textField" input type="email" required id="emailaddress" name ="inputEmail" placeholder="Email" required/>
                    </div>
                    <div class="inputRegisterInfo">

                        <input class="textField" type ="password" required id="lastName" name ="inputPassword" placeholder="Password" minlength=8/>
                    </div>
                    <div class="inputRegisterInfo">
                        <div class="dropDownBox">
                            <select type="text" step="1" id="GoalType"  name="conversion" required>
                                <option type="text" value ="imperial" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspImperial - lbs, inch</option>
                                <option type="text" value="metric" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspMetric - kg, metres</option>
                            </select>
                        </div>
                    </div>
                    <div class="inputRegisterInfo">
                        <input class="textField" type ="number" step="0.01" required id="lastName" name ="height" placeholder="Height" required/>
                    </div>
                    <div class="inputRegisterInfo">
                        <input class="textField" type ="number" required id="lastName" name ="weight" placeholder="Weight" onkeypress = "return isNumber(event)" required/>
                    </div>


                    <div class="inputPasswordBox">
                        <input class="linkButton" type ="submit" value="Create Account" class="linkButton" />   
                    </div>                 
                </form> 
                <a href="#" id="login" style="color: #FFFFFF; font: Arial">login</a>

            </div> 

        </div>

        <div id="loginInfo">
            <div class="infoContent">
                <div id="about">
                    <div id="sectionTitle"><p style="color: #018dc4;">About</p></div>
                    <div id="siteDescription"><p>Lots of words about the site and what we do.</p>
                    </div>
                </div>
                <div id="team">
                    <div id="sectionTitle"><p style="color: #018dc4;">Meet The Team</p></div>
                    <div class="meetTheTeam">
                        <div class="imagel">

                        </div>
                        <div class="teamDescriptionl">
                            <div class="name">
                                <p>Callum Coombes</p>
                            </div>
                            <div class="memberDescription">
                                <p>Description goes here</p>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="meetTheTeam">
                        <div class="imager">

                        </div>
                        <div class="teamDescriptionr">
                            <div class="name">
                                <p>Charlotte Porter</p>
                            </div>
                            <div class="memberDescription">
                                <p>Description goes here</p>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="meetTheTeam">
                        <div class="imagel">

                        </div>
                        <div class="teamDescriptionl">
                            <div class="name">
                                <p>Joe Lilley</p>
                            </div>
                            <div class="memberDescription">
                                <p>Description goes here</p>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="meetTheTeam">
                        <div class="imager">

                        </div>
                        <div class="teamDescriptionr">
                            <div class="name">
                                <p>James Rogers</p>
                            </div>
                            <div class="memberDescription">
                                <p>Description goes here</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="contact">
                    <div id="sectionTitle"><p style="color: #018dc4;">Contact Us</p></div>

                </div>
                <div id="terms">
                    <div id="sectionTitle"><p style="color: #018dc4;">Terms and Conditions</p></div>

                </div>
            </div>
            <div id="footerWrapper">
                <div id="footerContent">
                    <p>
                        Calorie Counter &nbsp&nbsp
                        Blog &nbsp&nbsp
                        Terms &nbsp&nbsp
                        Privacy &nbsp&nbsp
                        Contact Us &nbsp&nbsp
                        API &nbsp&nbsp
                        Jobs &nbsp&nbsp
                        Feedback &nbsp&nbsp
                        Community Guidelines 

                        <br/>
                        <br/>
                        Copyright 2005-2015 Project Breadcrumb, Inc.
                    </p>
                </div>
            </div>

        </div>
                    <script>

            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            }
        </script>


        <script type="text/javascript">

            $(function() {
                var images = ['1.jpg', '2.jpg', '3.jpg', '4.jpg', '5.jpg', '7.jpg'];
                var time = new Date().getSeconds();

                if (time <= 10) {
                    $('body').css({'background-image': 'url(images/' + images[0] + ')'});
                } else if (time <= 20) {
                    $('body').css({'background-image': 'url(images/' + images[1] + ')'});
                } else if (time <= 30) {
                    $('body').css({'background-image': 'url(images/' + images[2] + ')'});
                } else if (time <= 40) {
                    $('body').css({'background-image': 'url(images/' + images[3] + ')'});
                } else if (time <= 50) {
                    $('body').css({'background-image': 'url(images/' + images[4] + ')'});
                } else
                    $('body').css({'background-image': 'url(images/' + images[5] + ')'});
            });

            $("#register").click(function() {
                $("#loginBox").fadeOut();
                $("#createAccount").fadeIn();
            });

            $("#login").click(function() {
                $("#createAccount").fadeOut();
                $("#loginBox").fadeIn();
            });

            $(document).ready(function() {
                $('a[href^="#"]').on('click', function(e) {
                    e.preventDefault();

                    var target = this.hash;
                    var $target = $(target);

                    $('html, body').stop().animate({
                        'scrollTop': $target.offset().top
                    }, 900, 'swing', function() {
                        window.location.hash = target;
                    });
                });
            });
        </script>
    </body>
</html>