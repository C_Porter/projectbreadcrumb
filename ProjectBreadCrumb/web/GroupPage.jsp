<%-- 
    Document   : GroupPage
    Created on : 20-Apr-2015, 10:26:47
    Author     : qrk13cdu
--%>

<%@page import="models.Goal"%>
<%@page import="models.Exercise"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.ArrayList"%>
<%@page import="models.UserGroups"%>
<%@page import="models.User"%>
<%@page import="db.DatabaseAccessor"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <link rel="stylesheet" type="text/css" media="all" href="Main.css">
    <link rel="stylesheet" type="text/css" media="all" href="GroupPage.css">
    <link rel="stylesheet" href="/resources/demos/style.css"> 
    <title>Profile Page</title>  
    <!-- JavaScript for text area counter-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="//www.chartjs.org/assets/Chart.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
        $(function() {
            $("#datepicker").datepicker({dateFormat: 'dd/mm/yy'});
        });
    </script>

    <%
        if (request.getSession().getAttribute("email") == null) {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        //clearing the cached page, preventing users from hitting backspace
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
    %>

    <%
        String email = (String) request.getSession().getAttribute("email");
        User user = DatabaseAccessor.getUserByEmail(email);

        UserGroups userGroup = (UserGroups) request.getSession().getAttribute("selectedGroup");
        userGroup.populateUserGroup();
        userGroup.updateGoals();
    %>


    <body>
        <div id="bannerWrapper">
            <div id="bannerContent">        
                <div style="float:left">
                    <a href="Profile.jsp"><h1>Health Tracker Project</h1> </a>
                </div>

                <div id="logoutPos">
                    <form action="LogOutController" method="POST">
                        <input type ="submit" value="Log Out" id="LogoutButton" />
                    </form>
                </div>
                <div id="searchBoxPos">
                    <form action="SearchController" method="POST">                
                        <input id="searchBox" name="query" placeholder="Search"/>
                    </form>
                </div>

            </div>
        </div>

        <div id="contentWrapper">
            <div id="groupDetails"> 
                <div id="profilePicture"></div>
                <div id="nameDetails">
                    <p id="groupName"><%=userGroup.getGroupName()%></p>
                    <p id="bio"><%=userGroup.getGroupDetails()%></p>
                </div>
                <div id="edit">
                    <%
                        if (user.getID() != userGroup.getAdminId()) {
                            if (!userGroup.isAMember(user.getID())) {

                                out.println("<form action=\"GroupController\" method=\"get\">");
                                out.println("<button name=\"groupJoinedId\" type=\"submit\" class=\"manageButton\" value=\"" + userGroup.getGroupID() + "\">Join Group</button>");
                                out.println("</form>");

                            } else {

                                out.println("<form action=\"GroupController\" method=\"get\">");
                                out.println("<button name=\"groupToLeaveId\" type=\"submit\" class=\"manageButton\" value=\"" + userGroup.getGroupID() + "\">Leave Group</button>");
                                out.println("</form>");

                            }
                        } else {//delete group button
                            out.println("<form action=\"GroupController\" method=\"get\">");
                            out.println("<button name=\"groupToDeleteId\" type=\"submit\" class=\"manageButton\" value=\"" + userGroup.getGroupID() + "\">Delete Group</button>");
                            out.println("</form>");
                        }
                        if (userGroup.getAdminId() == user.getID()) {
                            out.println("<button id =\"editAccountBut\" class=\"manageButton\">Group Settings</button>");
                        }
                    %>  
                </div>
                <div id="bg">
                    <div id="search-container">
                        <div id="search">
                            <form action="GroupAdminController" method="POST">                
                                <div class="inputRegisterInfo">
                                    <input class="inputBox1" type ="text" id="firstName" name ="newGroupName" placeholder="Group Name" />
                                </div>
                                <div class="inputRegisterInfo">
                                    <input class="inputBox1" type ="text" id="lastName" name ="newGroupDetails" placeholder="Group Details" />
                                </div>
                                <div class="inputRegisterInfo">
                                    <input type ="submit" value="Update" class="manageButton1" />   
                                </div>
                            </form>
                            <form action="UserSearchController" method="POST">                
                                <div class="inputRegisterInfo">
                                    <input class="inputBox1" name="query" placeholder="Search"/>
                                </div>
                                <div class="inputRegisterInfo">
                                    <input class="manageButton1" type ="submit" value="Search" class="linkButton" />   
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div id="activeGoalListHeader">
                <div id="sectionTitle" style="padding:10px;">Group Members</div>
                <div id="listTitleWrapper">
                    <!--First Name-->
                    <div class="listTitle"><p>First Name</p></div>
                    <!--Last Name-->
                    <div class="listTitle"><p>Last Name</p></div>                      
                    <!--Delete User--> 
                    <%
                        if (user.getID() == userGroup.getAdminId()) {
                            out.println(" <div class=\"listTitle\"><p>Delete</p></div>");
                        }
                    %>
                </div>
            </div>
            <div id="activeGoalList"> 
                <%
                    ArrayList<User> users = userGroup.getUserList();

                    int j = 0;
                    for (int i = users.size() - 1; i >= 0; i--) {

                        if (j % 2 == 0) {
                            out.println("<div class=\"listWrapper1\">");
                            out.println("<div class=\"listDiv1\"><p>" + users.get(i).getFirstName() + "</p></div>");
                            out.println("<div class=\"listDiv1\"><p>" + users.get(i).getLastName() + "</p></div>");
                            out.println("<div class=\"listDiv1\">");
                            if (user.getID() == userGroup.getAdminId() && user.getID() != users.get(i).getID()) {
                                out.println("<form action=\"GroupAdminController\" method=\"POST\">"
                                        + "<input style=\"display:none\" type =\"text\" class=\"inputbox\" name=\"userID\" placeholder=\"User ID\""
                                        + "value=\"" + users.get(i).getID() + "\"/>"
                                        + "<div class=\"buttonWrapper\">"
                                        + "<input type =\"submit\" value=\"Remove\" class=\"removeButton\"/>"
                                        + "</div>"
                                        + "</form>");
                            }
                            out.println("</div></div>");
                        } else {
                            out.println("<div class=\"listWrapper2\">");
                            out.println("<div class=\"listDiv2\"><p>" + users.get(i).getFirstName() + "</p></div>");
                            out.println("<div class=\"listDiv2\"><p>" + users.get(i).getLastName() + "</p></div>");
                            out.println("<div class=\"listDiv2\">");
                            if (user.getID() == userGroup.getAdminId() && user.getID() != users.get(i).getID()) {
                                out.println("<form action=\"GroupAdminController\" method=\"POST\">"
                                        + "<input style=\"display:none\" type =\"text\" class=\"inputbox\" name=\"userID\" placeholder=\"User ID\""
                                        + "value=\"" + users.get(i).getID() + "\"/>"
                                        + "<div class=\"buttonWrapper\">"
                                        + "<input type =\"submit\" value=\"Remove\" class=\"removeButton\"/>"
                                        + "</div>"
                                        + "</form>");
                            }
                            out.println("</div></div>");
                        }
                        j++;
                    }


                %>
            </div>


            <%                if (user.getID() == userGroup.getAdminId()) {
                    out.println("<div id=\"updateGoal\">");
                    out.println("<div id=\"sectionTitle\">Add Goal</div>");

                    out.println("<form action=\"GroupGoalController\" method=\"POST\">");

                    out.println("<div id=\"section1\">");
                    //<!--Goal Name-->
                    out.println("<div class=\"boxContainer\">");
                    out.println("<input type=\"text\" class=\"inputBox\" name =\"goalName\" placeholder=\"Goal Name\" maxlength=\"15\" required/>");
                    out.println("</div>");

                    //<!--Goal Type-->
                    out.println("<div class=\"boxContainer\">");
                    out.println("<div class=\"dropDownBox\">");
                    out.println("<select type=\"number\" step=\"1\" id=\"GoalType\"  name=\"goalType\" required>");
                    out.println("<option type=\"number\" value =\"\" >Select Goal Type</option>");
                    out.println("<option type=\"number\" value=\"distance\" >Distance</option>");
                    out.println("<option type=\"number\" value=\"calories\" >Calories</option>");
                    out.println("<option type=\"number\" value=\"duration\" >Duration</option>");
                    out.println("<option type=\"number\" value=\"weightLoss\"  >Weight Loss</option>");
                    out.println("<option type=\"number\" value=\"weightGain\"  >Weight Gain</option>");
                    out.println("</select>");
                    out.println("</div>");
                    out.println("</div>");
                    out.println("</div>");

                    out.println("<div id=\"section3\">");
                    //<!--Goal Date-->
                    out.println("<div class=\"boxContainer\">");
                    out.println("<input type =\"text\" class=\"inputBox\" id=\"datepicker\" name =\"goalTargetDate\" placeholder=\"*TargetDate?\" required/>");
                    out.println("</div>");
                    //<!--Goal Submit Button-->
                    out.println("<div class=\"boxContainer\">");
                    out.println("<input type =\"submit\" value=\"Add Goal\" id=\"addGoalButton\" />");
                    out.println("</div>");
                    out.println("</div>");

                    out.println("<div id=\"section2\">");
                    //<!--Goal distance and Duration-->
                    out.println("<div class=\"distanceAndDurationContainer\">");
                    out.println("<div class=\"dropDownBox\">");
                    //<!--if goal type si distance or duration, list of exercises shown to select from  -->
                    Connection connection = DatabaseAccessor.getConnection();
                    Statement statement = connection.createStatement();
                    ResultSet resultSet = statement.executeQuery("Select * from exerciseList");

                    out.println("<select name=\"exType\">");
                    out.println("<option value =\"0\">Select Exercise Type</option>");
                    while (resultSet.next()) {
                        out.println("<option value =\"" + resultSet.getString("exerciseId") + "\">" + resultSet.getString("exName") + "</option>");
                    }
                    out.println("</select>");

                    out.println("</div>");

                    out.println("</div>");

                    out.println("<div class=\"boxContainer\">");
                    out.println("<input type =\"number\" class=\"inputBox\" name=\"goalTargetVal\" placeholder=\"Target Value\" maxlength=\"15\" onkeypress=\"return isNumber(event)\" required/>");
                    out.println("</div>");
                    out.println("</div>");
                    out.println("</form>");
                    out.println("</div>");
                }
            %>

            <div id="activeGoalListHeader">
                <div id="sectionTitle" style="padding:10px;">Active Goals</div>
                <div id="listTitleWrapper">
                    <!--Name-->
                    <div class="listTitle1"><p>Name</p></div>
                    <!--Type-->
                    <div class="listTitle1"><p>Type</p></div>
                    <!--Distance-->
                    <div class="listTitle1"><p>Exercise</p></div>
                    <!--Duration-->
                    <div class="listTitle1"><p>Target</p></div> 
                    <div class="listTitle1"><p>Left</p></div>
                    <!--Notes-->
                    <div class="listTitle1"><p>Date</p></div>  
                    <div class="listTitle1"><p>Achieved</p></div>   
                </div> 
            </div>
            <script>
    
                function isNumber(evt) {
                    evt = (evt) ? evt : window.event;
                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                        return false;
                    }
                    return true;
                }
            </script>

            <div id="activeGoalList">                              
                <% ArrayList<Goal> goalsList = userGroup.getGroupGoals();

                    if (goalsList.size() == 0) {
                        out.println("<div class=\"noGoalContent\">No Goals added yet</div>");
                    }

                    j = 0;

                    for (int i = goalsList.size() - 1; i >= 0; i--) {

                        if (goalsList.get(i).getCompleted() == 0) {
                            if (j % 2 == 0) {

                                out.println("<div class=\"listWrapper1\">");
                                out.println("<div class=\"listDiv3\"><p>" + goalsList.get(i).getName() + "</p></div>");
                                out.println("<div class=\"listDiv3\"><p>" + goalsList.get(i).getGoalType() + "</p></div>");
                                out.println("<div class=\"listDiv3\"><p>" + DatabaseAccessor.getExNameFromId(goalsList.get(i).getExType()) + "</p></div>");
                                out.println("<div class=\"listDiv3\"><p>" + goalsList.get(i).getStartTargetVal() + "</p></div>");
                                out.println("<div class=\"listDiv3\"><p>" + goalsList.get(i).getTargetVal() + "</p></div>");
                                out.println("<div class=\"listDiv3\"><p>" + goalsList.get(i).getDate() + "</p></div>");
                                if (goalsList.get(i).isGoalDateMet() == false) {

                                    out.println("<div class=\"listDiv3\"><p>Expired</p></div>");

                                } else {
                                    out.println("<div class=\"listDiv3\"><p>" + goalsList.get(i).getPercentageCompleted() + "%</p></div>");
                                }%>

                <%   out.println("</div>");
                } else {
                    out.println("<div class=\"listWrapper2\">");
                    out.println("<div class=\"listDiv4\"><p>" + goalsList.get(i).getName() + "</p></div>");
                    out.println("<div class=\"listDiv4\"><p>" + goalsList.get(i).getGoalType() + "</p></div>");
                    out.println("<div class=\"listDiv4\"><p>" + DatabaseAccessor.getExNameFromId(goalsList.get(i).getExType()) + "</p></div>");
                    out.println("<div class=\"listDiv4\"><p>" + goalsList.get(i).getStartTargetVal() + "</p></div>");
                    out.println("<div class=\"listDiv4\"><p>" + goalsList.get(i).getTargetVal() + "</p></div>");
                    out.println("<div class=\"listDiv4\"><p>" + goalsList.get(i).getDate() + "</p></div>");
                    if (goalsList.get(i).isGoalDateMet() == false) {

                        out.println("<div class=\"listDiv4\"><p>Expired</p></div>");

                    } else {
                        out.println("<div class=\"listDiv4\"><p>" + goalsList.get(i).getPercentageCompleted() + "%</p></div>");
                                    } %>


                <% out.println("</div>");
                            }
                            j++;
                        }
                    }
                %>
            </div>


            <div id="completedGoalListHeader">
                <div id="sectionTitle" style="padding:10px;">Completed Goals</div>
                <div id="listTitleWrapper">
                    <!--Name-->
                    <div class="listCompTitle"><p>Name</p></div>
                    <!--Type-->
                    <div class="listCompTitle"><p>Type</p></div>
                    <!--Distance-->
                    <div class="listCompTitle"><p>Exercise</p></div>
                    <!--Duration-->
                    <div class="listCompTitle"><p>Target</p></div>
                    <!--Notes-->
                    <div class="listCompTitle"><p>Date</p></div>   
                </div>
            </div>

            <div id="completedGoalList">                
                <%
                    boolean completedGoal = false;
                    j = 0;
                    for (int i = goalsList.size() - 1; i >= 0; i--) {
                        if (goalsList.get(i).getCompleted() == 1) {
                            if (j % 2 == 0) {
                                out.println("<div class=\"listWrapper1\">");
                                out.println("<div class=\"listCompDiv1\"><p>" + goalsList.get(i).getName() + "</p></div>");
                                out.println("<div class=\"listCompDiv1\"><p>" + goalsList.get(i).getGoalType() + "</p></div>");
                                out.println("<div class=\"listCompDiv1\"><p>" + DatabaseAccessor.getExNameFromId(goalsList.get(i).getExType()) + "</p></div>");
                                out.println("<div class=\"listCompDiv1\"><p>" + goalsList.get(i).getStartTargetVal() + "</p></div>");

                                out.println("<div class=\"listCompDiv1\"><p>" + goalsList.get(i).getDate() + "</p></div>");
                                out.println("</div>");
                            } else {
                                out.println("<div class=\"listWrapper2\">");
                                out.println("<div class=\"listCompDiv2\"><p>" + goalsList.get(i).getName() + "</p></div>");
                                out.println("<div class=\"listCompDiv2\"><p>" + goalsList.get(i).getGoalType() + "</p></div>");
                                out.println("<div class=\"listCompDiv2\"><p>" + DatabaseAccessor.getExNameFromId(goalsList.get(i).getExType()) + "</p></div>");
                                out.println("<div class=\"listCompDiv2\"><p>" + goalsList.get(i).getStartTargetVal() + "</p></div>");
                                out.println("<div class=\"listCompDiv2\"><p>" + goalsList.get(i).getDate() + "</p></div>");
                                out.println("</div>");
                            }
                            completedGoal = true;
                            j++;
                        }
                    }
                    if (!completedGoal) {
                        out.println("<div class=\"noGoalContent\">No Goals completed yet</div>");
                    }
                %>
            </div> 

        </div>

                    <script>
onkeypress = "return isNumber(event)"
            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            }
        </script>

    </body>
    <script type="text/javascript">

        $("#editAccountBut").click(function() {
            $("#bg").toggle();
        });
    </script>
</html>
