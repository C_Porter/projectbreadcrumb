<%-- 
    Document   : Exercise
    Created on : 01-Apr-2015, 12:34:43
    Author     : qrk13cdu
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="models.Exercise"%>
<%@page import="models.User"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="db.DatabaseAccessor"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" media="all" href="Main.css">
        <link rel="stylesheet" type="text/css" media="all" href="Exercisepage.css">

           <!-- JavaScript for text area counter-->
        <script language="javascript" type="text/javascript">
            function limitText(limitField, limitCount, limitNum) {
                if (limitField.value.length > limitNum) {
                    limitField.value = limitField.value.substring(0, limitNum);
                } else {
                    limitCount.value = limitNum - limitField.value.length;
                }
            }
        </script>
        <%
        String email = (String) request.getSession().getAttribute("email");
        User user = DatabaseAccessor.getUserByEmail(email);
        %>
        <title>Exercise Page</title>
    </head>
    <body>        
        <div id="bannerWrapper">
            <div id="bannerContent">        
                <div style="float:left">
                    <a href="Profile.jsp"><h1>Health Tracker Project</h1> </a>
                </div>
                <div id="logoutPos">
                    <form action="LogOutController" method="POST">
                        <input type ="submit" value="Log Out" id="LogoutButton" />
                    </form>
                </div>
                <div id="searchBoxPos">
                    <form action="SearchController" method="POST">                
                        <input id="searchBox" name="query" placeholder="Search"/>
                    </form>
                </div>
                
            </div>
        </div>
             
        <div id="contentWrapper">
            
            <div id="updateExercise">
                <div id="sectionTitle">Add Exercise</div>
                <form action="AddExerciseController" method="POST">  
                
                <!--Exercise Name-->
                    <div class="exerciseName">
                        <input type ="text" class="inputbox" name="exName" placeholder="Exercise Name" maxlength="20"/>
                    </div>
                
                    <!--Exercise Notes-->
                    <div class="exerciseNotes">
                        <!-- HTML for the text counter-->
                        <textarea class="inputtextbox" name="exDetails" rows="4" cols="28" onKeyDown="limitText(this.form.description, this.form.countdown, 100);" 
                                  onKeyUp="limitText(this.form.description, this.form.countdown, 100);">                                    
                        </textarea>
                    </div>
                
                    <!--Exercise Type-->
                    <div class="exerciseType">
                        <div class="dropDownBox">
                            <%Connection connection = DatabaseAccessor.getConnection();
                                    Statement statement = connection.createStatement();
                                    ResultSet resultSet = statement.executeQuery("Select * from exerciseList");
                           
                                    out.println("<select name=\"exType\">");
                            
                                    while (resultSet.next()) {
                                        out.println("<option value =\"" + resultSet.getString("exerciseId") + "\">" + resultSet.getString("exName") + "</option>");
                                    }
                                    out.println("</select>");
                            %>
                        </div>
                    </div>
                        
                    <!--Submit Button-->
                    <div class="submitExercise">
                        <input type ="submit" value="Add Exercise" id="addExerciseButton" />   
                    </div>
                    
                    <!--Exercise Distance-->
                    <div class="exerciseDistance">
                        <input type ="number" class="inputbox" name ="exDistance" placeholder="Distance" onkeypress = "return isNumber(event)" required/>
                    </div>
                
                    <!--Exercise Duration-->
                    <div class="exerciseDuration">
                        <input type ="number" class="inputbox" name ="exDuration" placeholder="Duration" onkeypress = "return isNumber(event)" required/>
                    </div> 
                    
                    <div class="exerciseMessage">
                        <%
                        //message to say added or not.. 
                        String message = (String) request.getAttribute("exMessage");
                        if (message != null) {
                            out.println(message);
                        }
                    %>
                    </div>
                </form>   
            </div>
                        
                        
                        
            <!--Exercise List-->
            
            <div id="exerciseListHeader">
                <div id="sectionTitle" style="padding:10px;">Completed Exercises</div>
                <div id="listTitleWrapper">
                    <!--Name-->
                    <div class="listTitle"><p>Name</p></div>
                    <!--Type-->
                     <div class="listTitle"><p>Type</p></div>
                    <!--Distance-->
                     <div class="listTitle"><p>Distance</p></div>
                    <!--Duration-->
                     <div class="listTitle"><p>Duration</p></div>
                    <!--Notes-->
                     <div class="listTitle"><p>Notes</p></div>
                    
                </div>
            </div>
            <div id="exerciseList">  
                <%
                        ArrayList<Exercise>exList = DatabaseAccessor.getExerciseList(user.getID());      
                        
                        if(exList.size() == 0) {
                            out.println("<div class=\"noGoalContent\">No Exercises added yet</div>");
                        }
                        
                        int j = 0;
                        for(int i = exList.size()-1; i >= 0; i--) {
                            
                            if(j % 2 == 0) {
                                out.println("<div class=\"listWrapper1\">");
                                    out.println("<div class=\"listDiv1\"><p>"+exList.get(i).getName()+"</p></div>");
                                    out.println("<div class=\"listDiv1\"><p>"+exList.get(i).getType()+"</p></div>");
                                    out.println("<div class=\"listDiv1\"><p>"+exList.get(i).getDistance()+"</p></div>");
                                    out.println("<div class=\"listDiv1\"><p>"+exList.get(i).getTime()+"</p></div>");
                                    out.println("<div class=\"listDiv1\"><p>"+exList.get(i).getDetails()+"</p></div>");
                                out.println("</div>");
                            }else {
                                out.println("<div class=\"listWrapper2\">");
                                    out.println("<div class=\"listDiv2\"><p>"+exList.get(i).getName()+"</p></div>");
                                    out.println("<div class=\"listDiv2\"><p>"+exList.get(i).getType()+"</p></div>");
                                    out.println("<div class=\"listDiv2\"><p>"+exList.get(i).getDistance()+"</p></div>");
                                    out.println("<div class=\"listDiv2\"><p>"+exList.get(i).getTime()+"</p></div>");
                                    out.println("<div class=\"listDiv2\"><p>"+exList.get(i).getDetails()+"</p></div>");
                                out.println("</div>");
                            }
                            j++;
                        }
                %>
            </div>    
            <div id="exerciseListFooter"></div>
        </div>
                    <script>

            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            }
        </script>
    </body>
</html>
