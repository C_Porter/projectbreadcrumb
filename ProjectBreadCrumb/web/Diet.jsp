<%-- 
    Document   : Exercise
    Created on : 01-Apr-2015, 12:34:43
    Author     : qrk13cdu
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Date"%>
<%@page import="models.*"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="db.DatabaseAccessor"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- JavaScript for date picker-->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" type="text/css" media="all" href="Main.css">
        <link rel="stylesheet" type="text/css" media="all" href="Dietpage.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <link rel="stylesheet" href="/resources/demos/style.css">        

        <%
            String email = (String) request.getSession().getAttribute("email");
            User user = DatabaseAccessor.getUserByEmail(email);

        %>
        <title>Goals Page</title>
    </head>

    <body>        
        <div id="bannerWrapper">
            <div id="bannerContent">        
                <div style="float:left">
                    <a href="Profile.jsp"><h1>Health Tracker Project</h1> </a>
                </div>

                <div id="logoutPos">
                    <form action="LogOutController" method="POST">
                        <input type ="submit" value="Log Out" id="LogoutButton" />
                    </form>
                </div>
                <div id="searchBoxPos">
                    <form action="SearchController" method="POST">                
                        <input id="searchBox" name="query" placeholder="Search"/>
                    </form>
                </div>

            </div>
        </div>

        <div id="contentWrapper">

            <!--Add Goal Form-->
            <div id="updateDiet">          
                <div id="sectionTitle">Add Meal</div>

                <div id="section1">
                    <form action="DietController" method="POST">   
                        <div id="addConsumable">

                            <!--Consumable name-->
                            <div class="boxContainer">
                                <input type ="text" class="inputBox" name ="consumableName" placeholder="Consumable Name" maxlength="20" required/>
                            </div>

                            <!--Fat-->
                            <div class="boxContainer">
                                <input type ="number" class="inputBox" name ="fat" placeholder="Fat g"  onkeypress = "return isNumber(event)" required/>
                            </div>

                            <!--Protein-->
                            <div class="boxContainer">
                                <input type ="number" class="inputBox" name ="protein" placeholder="Protein g"  onkeypress = "return isNumber(event)" required/>
                            </div>

                            <!--Carbs-->
                            <div class="boxContainer">
                                <input type ="number" class="inputBox" name ="carbs" placeholder="Carbs g" onkeypress = "return isNumber(event)" required/>
                            </div>

                            <!--Calories-->
                            <div class="boxContainer">
                                <input type ="number" class="inputBox" name ="calories" placeholder="Calorific cals" onkeypress = "return isNumber(event)" required/>
                            </div>

                            <!--Add Consumable-->
                            <div class="boxContainer"> 
                                <input type ="submit" value="Add Consumable" id="addConsumableButton" />        
                            </div>

                        </div>                      
                    </form> 
                </div>

                <div id="section2">

                    <!--<div id="sectionTitle">Meal</div>-->
                    <div id="meal">
                        <div id="listTitleWrapper">
                            <!--Name-->
                            <div class="listTitle"><p>Name</p></div>
                            <!--Type-->
                            <div class="listTitle"><p>Kcal</p></div>
                            <!--Distance-->
                            <div class="listTitle"><p>Protein</p></div>
                            <!--Duration-->
                            <div class="listTitle"><p>Carbs</p></div>
                            <!--Notes-->
                            <div class="listTitle"><p>Fat</p></div>                 
                        </div> 
                        <%                            ArrayList<Consumable> tempMeal;
                            if (request.getSession().getAttribute("tempMeal") == null) {
                                tempMeal = new ArrayList<Consumable>();
                                // mealList = (ArrayList<Consumable>) request.getAttribute("tempMeal");
                            } else {
                                tempMeal = (ArrayList<Consumable>) request.getSession().getAttribute("tempMeal");
                            }

                            int j = 0;
                            for (int i = tempMeal.size() - 1; i >= 0; i--) {

                                if (j % 2 == 0) {
                                    out.println("<div class=\"listWrapper1\">");
                                    out.println("<div class=\"listDiv1\"><p>" + tempMeal.get(i).getName() + "</p></div>");
                                    out.println("<div class=\"listDiv1\"><p>" + tempMeal.get(i).getCalories() + "</p></div>");
                                    out.println("<div class=\"listDiv1\"><p>" + tempMeal.get(i).getProtein() + "</p></div>");
                                    out.println("<div class=\"listDiv1\"><p>" + tempMeal.get(i).getCarbs() + "</p></div>");
                                    out.println("<div class=\"listDiv1\"><p>" + tempMeal.get(i).getSatFat() + "</p></div>");
                                    out.println("</div>");
                                } else {
                                    out.println("<div class=\"listWrapper2\">");
                                    out.println("<div class=\"listDiv2\"><p>" + tempMeal.get(i).getName() + "</p></div>");
                                    out.println("<div class=\"listDiv2\"><p>" + tempMeal.get(i).getCalories() + "</p></div>");
                                    out.println("<div class=\"listDiv2\"><p>" + tempMeal.get(i).getProtein() + "</p></div>");
                                    out.println("<div class=\"listDiv2\"><p>" + tempMeal.get(i).getCarbs() + "</p></div>");
                                    out.println("<div class=\"listDiv2\"><p>" + tempMeal.get(i).getSatFat() + "</p></div>");
                                    out.println("</div>");
                                }
                                j++;
                            }
                        %>
                    </div>

                    <div class="boxContainer"> 
                        <form action="AddMealController" method="POST">
                            <div id="date">

                                <!--Time-->
                                <div class="dropDownBox">   
                                    <select name="hours">

                                        <option value="1">1:00 am</option>
                                        <option value="2">2:00 am</option>
                                        <option value="3">3:00 am</option>
                                        <option value="4">4:00 am</option>
                                        <option value="5">5:00 am</option>
                                        <option value="6">6:00 am</option>
                                        <option value="7">7:00 am</option>
                                        <option value="8">8:00 am</option>
                                        <option value="9">9:00 am</option>
                                        <option value="10">10:00 am</option>
                                        <option value="11">11:00 am</option>
                                        <option value="12">12:00 pm</option>
                                        <option value="13">1:00 pm</option>
                                        <option value="14">2:00 pm</option>
                                        <option value="15">3:00 pm</option>
                                        <option value="16">4:00 pm</option>
                                        <option value="17">5:00 pm</option>
                                        <option value="18">6:00 pm</option>
                                        <option value="19">7:00 pm</option>
                                        <option value="20">8:00 pm</option>
                                        <option value="21">9:00 pm</option>
                                        <option value="22">10:00 pm</option>
                                        <option value="23">11:00 pm</option>
                                        <option value="0">12:00 am</option>
                                    </select>
                                </div>

                                <!--Date-->
                                <div class="dateSelect">                   
                                    <input type ="text" class="inputBox" id="datepicker" name ="dateConsumed" placeholder="Date Meal Consumed"  pattern="\d{1,2}/\d{1,2}/\d{4}" required/>                                 
                                </div>
                            </div>    

                            <div id="submitMeal">
                                <!--Add Meal-->
                                <input type ="submit" value="Add Meal" id="addMealButton" />
                            </div>

                        </form> 
                    </div>                                    
                </div>
            </div>


            <%
                Date date = (Date) request.getSession().getAttribute("mealDate");
                if (date == null) {
                    date = new Date();
                }
            %>

            <div id="mealListHeader">

                <div id="headWrapper">
                    <form action="MealDateController" method="POST">
                        <div id="section11">
                            <div id="sectionTitle2" style="padding:10px;">Meals</div>
                            <input type ="submit" value="<" id="incrementButtonl" /> 
                        </div>

                        <div id="section33">
                            <div class="rightbutton"> 
                                <input type ="submit" value=">" id="incrementButtonr" />        
                            </div>
                        </div>

                        <div id="section22">
                            <div id="test">
                                <div class="datewrapper">                   
                                    <input type ="text" class="dateinputBox" id="mealdatepicker" name ="mealDate" placeholder="<%out.println(date.toString());%>"/>                                 
                                </div>
                            </div>
                        </div>
                    </form>
                </div> 


                <div id="listTitleWrapper">
                    <!--Name-->
                    <div class="listTitle"><p>Name</p></div>
                    <!--Type-->
                    <div class="listTitle"><p>Calories</p></div>
                    <!--Distance-->
                    <div class="listTitle"><p>Protein</p></div>
                    <!--Duration-->
                    <div class="listTitle"><p>Carbs</p></div>
                    <!--Notes-->

                    <div class="listTitle"><p>Fat</p></div>   
                </div> 
            </div>   

            <div id="mealList">                            
                <%

                   // if (request.getAttribute("date") != null) {
                  //  }
                    ArrayList<Consumable> meal = DatabaseAccessor.getMealbyDate(user.getID(), date);

                    j = 0;
                    int k = 0;
                    int mealno = 1;
                    boolean bodge = true;

                    if (meal.size() > 0) {
                        k = meal.get(0).getMealID();
                    }

                    for (int i = 0; i < meal.size(); i++) {

                        if (j % 2 == 0) {
                            if (meal.get(i).getMealID() == k) {

                                if (bodge) {
                                    out.println("<div class=\"listWrapper1\">");
                                    out.println("<div class=\"listDiv11\"><p>Meal " + mealno + "</p></div>");
                                    out.println("</div>");
                                    out.println("<div class=\"listWrapper2\">");
                                    out.println("<div class=\"listDiv2\"><p>Name</p></div>");
                                    out.println("<div class=\"listDiv2\"><p>Calories</p></div>");
                                    out.println("<div class=\"listDiv2\"><p>Protein</p></div>");
                                    out.println("<div class=\"listDiv2\"><p>Carbs</p></div>");
                                    out.println("<div class=\"listDiv2\"><p>Fat</p></div>");
                                    out.println("</div>");
                                    bodge = false;
                                }
                            } else {
                                k = meal.get(i).getMealID();
                                mealno++;
                                bodge = false;
                                out.println("<div class=\"listWrapper1\">");
                                out.println("<div class=\"listDiv11\"><p>Meal " + mealno + "</p></div>");
                                out.println("</div>");
                                out.println("<div class=\"listWrapper2\">");
                                out.println("<div class=\"listDiv2\"><p>Name</p></div>");
                                out.println("<div class=\"listDiv2\"><p>Calories</p></div>");
                                out.println("<div class=\"listDiv2\"><p>Protein</p></div>");
                                out.println("<div class=\"listDiv2\"><p>Carbs</p></div>");
                                out.println("<div class=\"listDiv2\"><p>Fat</p></div>");
                                out.println("</div>");

                            }

                            out.println("<div class=\"listWrapper1\">");
                            out.println("<div class=\"listDiv1\"><p>" + meal.get(i).getName() + "</p></div>");
                            out.println("<div class=\"listDiv1\"><p>" + meal.get(i).getCalories() + "</p></div>");
                            out.println("<div class=\"listDiv1\"><p>" + meal.get(i).getProtein() + "</p></div>");
                            out.println("<div class=\"listDiv1\"><p>" + meal.get(i).getCarbs() + "</p></div>");
                            out.println("<div class=\"listDiv1\"><p>" + meal.get(i).getSatFat() + "</p></div>");
                            out.println("</div>");
                        } else {
                            if (meal.get(i).getMealID() == k) {

                                if (bodge) {
                                    out.println("<div class=\"listWrapper2\">");
                                    out.println("<div class=\"listDiv22\"><p>Meal " + mealno + "</p></div>");
                                    out.println("</div>");
                                    out.println("<div class=\"listWrapper1\">");
                                    out.println("<div class=\"listDiv1\"><p>Name</p></div>");
                                    out.println("<div class=\"listDiv1\"><p>Calories</p></div>");
                                    out.println("<div class=\"listDiv1\"><p>Protein</p></div>");
                                    out.println("<div class=\"listDiv1\"><p>Carbs</p></div>");
                                    out.println("<div class=\"listDiv1\"><p>Fat</p></div>");
                                    out.println("</div>");
                                    bodge = false;
                                }
                            } else {
                                k = meal.get(i).getMealID();
                                mealno++;
                                bodge = false;
                                out.println("<div class=\"listWrapper2\">");
                                out.println("<div class=\"listDiv22\"><p>Meal " + mealno + "</p></div>");
                                out.println("</div>");
                                out.println("<div class=\"listWrapper1\">");
                                out.println("<div class=\"listDiv1\"><p>Name</p></div>");
                                out.println("<div class=\"listDiv1\"><p>Calories</p></div>");
                                out.println("<div class=\"listDiv1\"><p>Protein</p></div>");
                                out.println("<div class=\"listDiv1\"><p>Carbs</p></div>");
                                out.println("<div class=\"listDiv1\"><p>Fat</p></div>");
                                out.println("</div>");
                            }
                            out.println("<div class=\"listWrapper2\">");
                            out.println("<div class=\"listDiv2\"><p>" + meal.get(i).getName() + "</p></div>");
                            out.println("<div class=\"listDiv2\"><p>" + meal.get(i).getCalories() + "</p></div>");
                            out.println("<div class=\"listDiv2\"><p>" + meal.get(i).getProtein() + "</p></div>");
                            out.println("<div class=\"listDiv2\"><p>" + meal.get(i).getCarbs() + "</p></div>");
                            out.println("<div class=\"listDiv2\"><p>" + meal.get(i).getSatFat() + "</p></div>");
                            out.println("</div>");
                        }
                        j++;

                    }
                %>
            </div> 


        </div>


        <%//                        //message to say added or not.. 
//                        String message = (String) request.getAttribute("exMessage");
//                        if (message != null) {
//                            out.println(message);
//                        }
        %>


        <%            /*      ArrayList<Goal> goalsList = DatabaseAccessor.getUserGoalsList(user.getID());

             for (Goal g : goalsList) {
             out.println("<br>" + g.toString() + "</br>");

             } */

        %> 
        <script>

            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            }
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#GoalType').bind('change', function() {
                    var elements = $('div.distanceAndDurationContainer').children().hide(); // hide all the elements
                    var value = $(this).val();

                    if (value === "distance" || value === "duration") { // if somethings' selected
                        $('div.distanceAndDurationContainer').children().show(); // show the ones we want
                    }
                }).trigger('change');
            });

            $(document).ready(function() {
                $('#targetDateyesno').bind('change', function() {
                    var elements = $('div.container2').children().hide(); // hide all the elements
                    var value = $(this).val();

                    if (value.length) { // if somethings' selected
                        elements.filter('.' + value).show(); // show the ones we want
                    }
                }).trigger('change');
            });

            $("#date").click(function() {
                $("#submitMeal").fadeIn();
            });


            $(function() {
                $("#datepicker").datepicker({dateFormat: 'dd/mm/yy'});
            });

            $(function() {
                $("#mealdatepicker").datepicker({dateFormat: 'dd/mm/yy'});
            });

        </script>

    </body>
</html>
