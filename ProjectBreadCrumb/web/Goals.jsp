<%-- 
    Document   : Exercise
    Created on : 01-Apr-2015, 12:34:43
    Author     : qrk13cdu
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="models.Goal"%>
<%@page import="models.User"%>
<%@page import="models.Exercise"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="db.DatabaseAccessor"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- JavaScript for date picker-->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" type="text/css" media="all" href="Main.css">
        <link rel="stylesheet" type="text/css" media="all" href="GoalPage.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <link rel="stylesheet" href="/resources/demos/style.css">       

        <script>
            $(function() {
                $("#datepicker").datepicker({dateFormat: 'dd/mm/yy'});
            });
        </script>
        <style type="text/css">

        </style>
        <!-- end JavaScript for date picker-->
        <%
            String email = (String) request.getSession().getAttribute("email");
            User user = DatabaseAccessor.getUserByEmail(email);
            user.populateUser();
        %>
        <title>Goals Page</title>
    </head>

    <body>        
        <div id="bannerWrapper">
            <div id="bannerContent">        
                <div style="float:left">
                    <a href="Profile.jsp"><h1>Health Tracker Project</h1> </a>
                </div>

                <div id="logoutPos">
                    <form action="LogOutController" method="POST">
                        <input type ="submit" value="Log Out" id="LogoutButton" />
                    </form>
                </div>
                <div id="searchBoxPos">
                    <form action="SearchController" method="POST">                
                        <input id="searchBox" name="query" placeholder="Search"/>
                    </form>
                </div>

            </div>
        </div>

        <div id="contentWrapper">

            <!--Add Goal Form-->
            <div id="updateGoal">          
                <div id="sectionTitle">Add Goal</div>

                <form action="GoalsController" method="POST">   

                    <div id="section1">
                        <!--Goal Name-->
                        <div class="boxContainer">
                            <input type ="text" class="inputBox" name ="goalName" placeholder="Goal Name" maxlength="20"  required/>
                        </div>

                        <!--Goal Type-->
                        <div class="boxContainer">
                            <div class="dropDownBox">
                                <select type="number" step="1" id="GoalType"  name="goalType">
                                    <option type="number" value ="" >Select Goal Type</option>
                                    <option type="number" value="distance"  onkeypress = "return isNumber(event)" >Distance miles</option>
                                    <option type="number" value="calories"  onkeypress = "return isNumber(event)" >Calories</option>
                                    <option type="number" value="duration"  onkeypress = "return isNumber(event)">Duration minutes</option>
                                    <option type="number" value="weightLoss"  onkeypress = "return isNumber(event)" >Weight Loss <%
                                        if (user.getHO().getConversion().equals("imperial")) {
                                            out.println("lbs");
                                        } else if (user.getHO().getConversion().equals("metric")) {
                                            out.println("kgs");
                                        }
                                        %></option>
                                    <option type="number" value="weightGain"  onkeypress = "return isNumber(event)" >Weight Gain
                                        <%
                                            if (user.getHO().getConversion().equals("imperial")) {
                                                out.println("lbs");
                                            } else if (user.getHO().getConversion().equals("metric")) {
                                                out.println("kgs");
                                            }
                                        %></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <script>
                       
                                function isNumber(evt) {
                                    evt = (evt) ? evt : window.event;
                                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                                    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                                        return false;
                                    }
                                    return true;
                                }
                    </script>

                    <div id="section3">
                        <!--Goal Date-->
                        <div class="boxContainer">                   
                            <input type ="text" class="inputBox" id="datepicker" name ="goalTargetDate" placeholder="*TargetDate?" pattern="\d{1,2}/\d{1,2}/\d{4}" required/>                                 
                        </div>
                        <!--Goal Submit Button-->
                        <div class="boxContainer"> 
                            <input type ="submit" value="Add Goal" id="addGoalButton" />        
                        </div>
                    </div>

                    <div id="section2">
                        <!--Goal distance and Duration-->
                        <div class="distanceAndDurationContainer">                 
                            <div class="dropDownBox">
                                <!--if goal type si distance or duration, list of exercises shown to select from  -->
                                <%Connection connection = DatabaseAccessor.getConnection();
                                    Statement statement = connection.createStatement();
                                    ResultSet resultSet = statement.executeQuery("Select * from exerciseList");

                                    out.println("<select name=\"exType\">");
                                    out.println("<option value =\"0\">Select Exercise Type</option>");
                                    while (resultSet.next()) {
                                        out.println("<option value =\"" + resultSet.getString("exerciseId") + "\">" + resultSet.getString("exName") + "</option>");
                                    }
                                    out.println("</select>");

                                %>

                            </div>

                        </div>

                        <div class="boxContainer"> 
                            <input type ="number" class="inputBox" name="goalTargetVal" placeholder="Target Value" maxlength="15"  onkeypress = "return isNumber(event)" required/>         
                        </div>
                    </div>                        
                </form> 
                <div id="bg">
                    <div id="search-container">
                        <div id="search-bg"></div>
                        <div id="search">
                            <iframe name="informationFrame" scrolling="no" id="goalInformationFrame"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div id="activeGoalListHeader">
                <div id="sectionTitle" style="padding:10px;">Active Goals</div>
                <div id="listTitleWrapper">
                    <!--Name-->
                    <div class="listTitle"><p>Name</p></div>
                    <!--Type-->
                    <div class="listTitle"><p>Type</p></div>
                    <!--Distance-->
                    <div class="listTitle"><p>Exercise</p></div>
                    <!--Duration-->
                    <div class="listTitle"><p>Target</p></div> 
                    <div class="listTitle"><p>Left</p></div>
                    <!--Notes-->
                    <div class="listTitle"><p>Date</p></div>  
                    <div class="listTitle"><p>Achieved</p></div>   

                </div> 
            </div>

            <div id="activeGoalList">                              

                <%                    ArrayList<Goal> goalsList = DatabaseAccessor.getUserGoalsList(user.getID());

                    if (goalsList.size() == 0) {
                        out.println("<div class=\"noGoalContent\">No Goals added yet</div>");
                    }

                    int j = 0;

                    for (int i = goalsList.size() - 1; i >= 0; i--) {

                        if (goalsList.get(i).getCompleted() == 0) {
                            if (j % 2 == 0) {

                                out.println("<div class=\"listWrapper1\">");
                                out.println("<div class=\"listDiv1\">"
                                        + "<form action=\"GoalInformationController\" method=\"POST\" target=\"informationFrame\">"
                                        + "<input type =\"text\" class=\"hiddenField\" name=\"goalEx\" value=\"" + DatabaseAccessor.getExNameFromId(goalsList.get(i).getExType()) + "\"/>"
                                        + "<input type =\"submit\" class=\"button\" value=\"" + goalsList.get(i).getName() + "\"/> "
                                        + "</form>"
                                        + "</div>");
                                out.println("<div class=\"listDiv1\"><p>" + goalsList.get(i).getGoalType() + "</p></div>");
                                out.println("<div class=\"listDiv1\"><p>" + DatabaseAccessor.getExNameFromId(goalsList.get(i).getExType()) + "</p></div>");
                                out.println("<div class=\"listDiv1\"><p>" + goalsList.get(i).getStartTargetVal() + "</p></div>");
                                out.println("<div class=\"listDiv1\"><p>" + goalsList.get(i).getTargetVal() + "</p></div>");
                                if (goalsList.get(i).isGoalDateMet() == false) {
                                    out.println("<div class=\"listDiv1\"><p>Expired</p></div>");
                                    out.println("<div class=\"listDiv1\"><p>Expired</p></div>");

                                } else {
                                    DecimalFormat df = new DecimalFormat("#.0");
                                    out.println("<div class=\"listDiv1\"><p>" + goalsList.get(i).getDate() + "</p></div>");
                                    out.println("<div class=\"listDiv1\"><p>" + df.format(goalsList.get(i).calculatePercentageAchieved()) + "%</p></div>");
                                }
                                out.println("</div>");
                            } else {
                                out.println("<div class=\"listWrapper2\">");
                                out.println("<div class=\"listDiv2\">"
                                        + "<form action=\"GoalInformationController\" method=\"POST\" target=\"informationFrame\">"
                                        + "<input type =\"text\" class=\"hiddenField\" name=\"goalEx\" value=\"" + DatabaseAccessor.getExNameFromId(goalsList.get(i).getExType()) + "\"/>"
                                        + "<input type =\"submit\" class=\"button\" value=\"" + goalsList.get(i).getName() + "\"/> "
                                        + "</form>"
                                        + "</div>");
                                out.println("<div class=\"listDiv2\"><p>" + goalsList.get(i).getGoalType() + "</p></div>");
                                out.println("<div class=\"listDiv2\"><p>" + DatabaseAccessor.getExNameFromId(goalsList.get(i).getExType()) + "</p></div>");
                                out.println("<div class=\"listDiv2\"><p>" + goalsList.get(i).getStartTargetVal() + "</p></div>");
                                out.println("<div class=\"listDiv2\"><p>" + goalsList.get(i).getTargetVal() + "</p></div>");
                                if (goalsList.get(i).isGoalDateMet() == false) {
                                    out.println("<div class=\"listDiv2\"><p>Expired</p></div>");
                                    out.println("<div class=\"listDiv2\"><p>Expired</p></div>");

                                } else {
                                    out.println("<div class=\"listWrapper2\">");
                                    out.println("<div class=\"listDiv2\"><p>" + goalsList.get(i).getName() + "</p></div>");
                                    out.println("<div class=\"listDiv2\"><p>" + goalsList.get(i).getGoalType() + "</p></div>");
                                    out.println("<div class=\"listDiv2\"><p>" + DatabaseAccessor.getExNameFromId(goalsList.get(i).getExType()) + "</p></div>");
                                    out.println("<div class=\"listDiv2\"><p>" + goalsList.get(i).getStartTargetVal() + "</p></div>");
                                    out.println("<div class=\"listDiv2\"><p>" + goalsList.get(i).getTargetVal() + "</p></div>");
                                    if (goalsList.get(i).isGoalDateMet() == false) {
                                        out.println("<div class=\"listDiv2\"><p>Expired</p></div>");
                                        out.println("<div class=\"listDiv2\"><p>Expired</p></div>");

                                    } else {
                                        DecimalFormat df = new DecimalFormat("#.0");
                                        out.println("<div class=\"listDiv2\"><p>" + goalsList.get(i).getDate() + "</p></div>");
                                        out.println("<div class=\"listDiv2\"><p>" + df.format(goalsList.get(i).calculatePercentageAchieved()) + "%</p></div>");
                                    }

                                    out.println("</div>");
                                }
                                j++;
                            }

                        }
                    }
                %>




            </div> 


            <div id="activeGoalListFooter"></div>

            <div id="completedGoalListHeader">
                <div id="sectionTitle" style="padding:10px;">Completed Goals</div>
                <div id="listTitleWrapper">
                    <!--Name-->
                    <div class="listCompTitle"><p>Name</p></div>
                    <!--Type-->
                    <div class="listCompTitle"><p>Type</p></div>
                    <!--Distance-->
                    <div class="listCompTitle"><p>Exercise</p></div>
                    <!--Duration-->
                    <div class="listCompTitle"><p>Target</p></div>
                    <!--Notes-->
                    <div class="listCompTitle"><p>Date</p></div>   

                </div>
            </div>

            <div id="completedGoalList">                
                <%                    boolean completedGoal = false;
                    j = 0;
                    for (int i = goalsList.size() - 1; i >= 0; i--) {
                        if (goalsList.get(i).getCompleted() == 1) {
                            if (j % 2 == 0) {
                                out.println("<div class=\"listWrapper1\">");
                                out.println("<div class=\"listCompDiv1\"><p>" + goalsList.get(i).getName() + "</p></div>");
                                out.println("<div class=\"listCompDiv1\"><p>" + goalsList.get(i).getGoalType() + "</p></div>");
                                out.println("<div class=\"listCompDiv1\"><p>" + DatabaseAccessor.getExNameFromId(goalsList.get(i).getExType()) + "</p></div>");
                                out.println("<div class=\"listCompDiv1\"><p>" + goalsList.get(i).getStartTargetVal() + "</p></div>");

                                out.println("<div class=\"listCompDiv1\"><p>" + goalsList.get(i).getDate() + "</p></div>");
                                out.println("</div>");
                            } else {
                                out.println("<div class=\"listWrapper2\">");
                                out.println("<div class=\"listCompDiv2\"><p>" + goalsList.get(i).getName() + "</p></div>");
                                out.println("<div class=\"listCompDiv2\"><p>" + goalsList.get(i).getGoalType() + "</p></div>");
                                out.println("<div class=\"listCompDiv2\"><p>" + DatabaseAccessor.getExNameFromId(goalsList.get(i).getExType()) + "</p></div>");
                                out.println("<div class=\"listCompDiv2\"><p>" + goalsList.get(i).getStartTargetVal() + "</p></div>");
                                out.println("<div class=\"listCompDiv2\"><p>" + goalsList.get(i).getDate() + "</p></div>");
                                out.println("</div>");
                            }
                            completedGoal = true;
                            j++;
                        }
                    }
                    if (!completedGoal) {
                        out.println("<div class=\"noGoalContent\">No Goals completed yet</div>");
                    }
                %>
            </div> 
            <div id="completedGoalListFooter"></div>


        </div>



        <%//                        //message to say added or not.. 
            //                        String message = (String) request.getAttribute("exMessage");
            //                        if (message != null) {
            //                            out.println(message);
            //                        }
        %>


        <%
            /*      ArrayList<Goal> goalsList = DatabaseAccessor.getUserGoalsList(user.getID());

             for (Goal g : goalsList) {
             out.println("<br>" + g.toString() + "</br>");

             } */

        %> 

        <script type="text/javascript">
            $(document).ready(function() {
                $('#GoalType').bind('change', function() {
                    var elements = $('div.distanceAndDurationContainer').children().hide(); // hide all the elements
                    var value = $(this).val();

                    if (value === "distance" || value === "duration") { // if somethings' selected
                        $('div.distanceAndDurationContainer').children().show(); // show the ones we want
                    }
                }).trigger('change');
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#targetDateyesno').bind('change', function() {
                    var elements = $('div.container2').children().hide(); // hide all the elements
                    var value = $(this).val();

                    if (value.length) { // if somethings' selected
                        elements.filter('.' + value).show(); // show the ones we want
                    }
                }).trigger('change');
            });
        </script>
        <script type="text/javascript">

            (function($) {
                $(".type").hover(function(e) {
                    e.stopPropagation();
                    $('.type')
                            .stop()
                            .animate(
                                    {"margin-top": (
                                                $(this).parent().outerHeight() -
                                                $(this).outerHeight()
                                                )},
                            250
                                    );
                    return false;
                }, function(e) {
                    e.stopPropagation();
                    console.log("hover out");
                    $('.type').stop().animate(
                            {"margin-top": "30px"},
                    250
                            );
                    return false;
                });
            }(jQuery));

            $(document).ready(function() {
                $('.listDiv1').hover(
                        function() {
                            $("#test").show();

                        },
                        function() {
                            $("#test").hide();
                        }
                );
            });


            $(document).ready(function() {

                $('.listDiv2').hover(
                        function() {
                            $("#test2").show();
                        },
                        function() {
                            $("#test2").hide();
                        }
                );
            });

            $(".button").click(function() {

                $("#bg").toggle();
            });

        </script>
    </body>
</html>
