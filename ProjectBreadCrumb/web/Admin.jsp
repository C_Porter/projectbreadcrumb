<%@page import="java.util.ArrayList"%>
<%@page import="db.DatabaseAccessor"%>
<%@page import="models.Exercise"%>
<%@page import="models.Goal"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="models.User" %>
<%@page import="models.Account" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" media="all" href="Main.css">
        <link rel="stylesheet" type="text/css" media="all" href="AdminPage.css">
        <title>Administration</title>
    </head>
    <%
        String email = (String) request.getSession().getAttribute("email");
        User user = DatabaseAccessor.getUserByEmail(email);
        Account acc = DatabaseAccessor.getAccountByEmail(user.getEmail());
    %>
    <body>
        <div id="bannerWrapper">
            <div id="bannerContent">        
                <div style="float:left">
                    <a href="Profile.jsp"><h1>Health Tracker Project</h1></a>
                </div>
                <div id="logoutPos">
                    <form action="LogOutController" method="POST">
                        <input type ="submit" value="Log Out" id="LogoutButton" />
                    </form>
                </div>
                <div id="searchBoxPos">
                    <form action="SearchController" method="POST">                
                        <input id="searchBox" name="query" placeholder="Search"/>
                    </form>
                </div>
            </div>
        </div>
      
        <div id="AdminWrapper">
            
            <div id="userListHeader">
                <div id="sectionTitle" style="padding:10px;">Users</div> 
                <div id="listTitleWrapper">
                    <!--ID-->
                    <div class="listTitle"><p>ID</p></div>
                    <!--Email-->
                    <div class="listTitle"><p>Email</p></div>
                    <!--First Name-->
                    <div class="listTitle"><p>First Name</p></div>
                    <!--Last Name-->
                    <div class="listTitle"><p>Last Name</p></div>                      
                    <!--Delete User-->
                     <div class="listTitle"><p>Delete</p></div>
                </div>
            </div>
            
            <div id="userList">  
                <%
                    if(acc.getAdmin() == true) {
                        ArrayList<User>users = DatabaseAccessor.getUserList();      
                        
                        if(users.size() == 0) {
                            out.println("<div class=\"noGoalContent\">No users added yet</div>");
                        }
                        
                        int j = 0;
                        for(int i = users.size()-1; i >= 0; i--) {
                            if(users.get(i).getID() != user.getID()) {
                                if(j % 2 == 0) {
                                    out.println("<div class=\"listWrapper1\">");
                                    out.println("<div class=\"listDiv1\"><p>"+users.get(i).getID()+"</p></div>");
                                    out.println("<div class=\"listDiv1\"><p>"+users.get(i).getEmail()+"</p></div>");
                                    out.println("<div class=\"listDiv1\"><p>"+users.get(i).getFirstName()+"</p></div>");
                                    out.println("<div class=\"listDiv1\"><p>"+users.get(i).getLastName()+"</p></div>");
                                    out.println("<div class=\"listDiv1\">");                           
                                    out.println("<form action=\"AdministrationController\" method=\"POST\">"
                                            + "<input type =\"text\" class=\"inputbox\" name=\"userID\" placeholder=\"User ID\""
                                            + "value=\"" + users.get(i).getID() + "\"/>"
                                            + "<div class=\"buttonWrapper\">"
                                            + "<input type =\"submit\" value=\"Remove\" class=\"removeButton\"/>"
                                            + "</div>"
                                            + "</form></div>");
                                    out.println("</div>");
                                }else {
                                    out.println("<div class=\"listWrapper2\">");
                                    out.println("<div class=\"listDiv2\"><p>"+users.get(i).getID()+"</p></div>");
                                    out.println("<div class=\"listDiv2\"><p>"+users.get(i).getEmail()+"</p></div>");
                                    out.println("<div class=\"listDiv2\"><p>"+users.get(i).getFirstName()+"</p></div>");
                                    out.println("<div class=\"listDiv2\"><p>"+users.get(i).getLastName()+"</p></div>");
                                    out.println("<div class=\"listDiv2\">");
                                    out.println("<form action=\"AdministrationController\" method=\"POST\">"
                                            + "<input type =\"text\" class=\"inputbox\" name=\"userID\" placeholder=\"User ID\""
                                            + "value=\"" + users.get(i).getID() + "\"/>"
                                            + "<div class=\"buttonWrapper\">"
                                            + "<input type =\"submit\" value=\"Remove\" class=\"removeButton\"/>"
                                            + "</div>"
                                            + "</form></div>");
                                    
                                    out.println("</div>");
                                }
                                j++;
                            }
                        }
                    }
                    else
                        out.println("<iframe width=\"420\" height=\"315\" src=\"https://www.youtube.com/embed/BROWqjuTM0g?autoplay=1\" frameborder=\"0\" allowfullscreen></iframe>");
                %>
            </div>               
        </div>
    </body>
</html>
