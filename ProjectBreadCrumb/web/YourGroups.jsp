<%-- 
    Document   : ResultsPage
    Created on : 20-Apr-2015, 11:22:25
    Author     : qrk13cdu
--%>

<%@page import="models.UserGroups"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" media="all" href="Main.css">
        <link rel="stylesheet" type="text/css" media="all" href="YourGroupsPage.css">
      
        <!-- JavaScript for text area counter-->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="//www.chartjs.org/assets/Chart.min.js">
        </script>
        <title>Groups</title>
    </head>
    <body>      
        <div id="bannerWrapper">
            <div id="bannerContent">        
                <div style="float:left">
                    <a href="Profile.jsp"><h1>Health Tracker Project</h1> </a>
                </div>

                <div id="logoutPos">
                    <form action="LogOutController" method="POST">
                        <input type ="submit" value="Log Out" id="LogoutButton" />
                    </form>
                </div>
                <div id="searchBoxPos">
                    <form action="SearchController" method="POST">                
                        <input id="searchBox" name="query" placeholder="Search"/>
                    </form>
                </div>

            </div>
        </div>
        
        <div id="contentWrapper">
            
            <div id="section1">
                <div id="groupListHeader">
                <div id="sectionTitle" style="padding:10px;">Your Groups</div>
                <div id="listTitleWrapper">
                    <!--Group ID-->
                    <div class="listTitle1"><p>ID</p></div>
                    <!--Group Name-->
                    <div class="listTitle2"><p>Group Name</p></div>
                </div> 
            </div>
            <div id="groupList"> 
                <%
                ArrayList<UserGroups> groups = (ArrayList<UserGroups>) request.getSession().getAttribute("groupResults");

                //for (UserGroups ug : groups) {
                //     out.print("name" + ug.getGroupName());
                // }
                if (groups.size() == 0) {
                    out.println("<div class=\"noGroupContent\">No results found</div>");
                }

                int j = 0;
                for (int i = groups.size() - 1; i >= 0; i--) {
                    out.println("<form action=\"ProcessSearchResult"
                            + "\" method=\"get\">");
                    if (j % 2 == 0) {
                        out.println("<div class=\"listWrapper1\">");
                        out.println("<div class=\"listDiv1\"><p>" + groups.get(i).getGroupID() + "</p></div>");
                        out.println("<div class=\"listDiv11\"><button name=\"selectedResult\" type=\"submit\" value=\"" + i + "\" class=\"button\">" + groups.get(i).getGroupName() + "</button></div>");

                        out.println("</form>");                      

                        out.println("</div>");

                    } else {
                        out.println("<div class=\"listWrapper2\">");
                        out.println("<div class=\"listDiv2\"><p>" + groups.get(i).getGroupID() + "</p></div>");
                        out.println("<div class=\"listDiv22\"><button name=\"selectedResult\" type=\"submit\" value=\"" + i + "\" class=\"button\">" + groups.get(i).getGroupName() + "</button></div>");

                        out.println("</form>");
                        
                        out.println("</div>");
                    }
                    j++;
                }

                %>
            </div>
            </div>
            <div id="section2">
                <div id="createGroup"> 
                    <div id="sectionTitle">Create Group</div>
                    <form action="CreateGroupController" method="POST">                
                        <div class="inputRegisterInfo">
                            <input class="inputBox" type ="text" id="groupName" name ="groupName" placeholder="Group Name" required/>
                        </div>
                        <div class="inputRegisterInfo">
                            <input class="inputBox" type ="text" id="groupDetails" name ="groupDetails" placeholder="Group Details" required/>
                        </div>            
                        <div class="inputRegisterInfo">
                            <input id="addGroupButton" type ="submit" value="Update" />   
                        </div>
                        <%                        
                            String error = (String) request.getAttribute("errorGroupCreationMessage");
                            if (error != null) {
                                out.println(error);
                            }
                        %>
                    </form>
                </div>
            </div>
        </div>

    </body>
    <script type="text/javascript">
        $("#editAccountBut").click(function() {
            $("#bg").fadeToggle(300);
        });
    </script>
</html>
