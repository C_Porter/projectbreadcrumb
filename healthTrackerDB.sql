DROP TABLE account CASCADE; 
DROP TABLE siteUser CASCADE;
DROP TABLE goals CASCADE;
DROP TABLE exercises CASCADE;
DROP TABLE healthOverview CASCADE;
DROP TABLE consumable CASCADE;
DROP TABLE meals CASCADE;
DROP TABLE userGroups CASCADE;
DROP TABLE groupMembers CASCADE;
DROP TABLE messages CASCADE;
DROP TABLE exerciseList CASCADE;
DROP TABLE weightHist CASCADE;
DROP TABLE groupMemberGoalWeight CASCADE;
DROP TABLE notifications CASCADE;

CREATE TABLE account
(
email VARCHAR(100) PRIMARY KEY,
password VARCHAR(100),
isAdmin INT
);

CREATE TABLE siteUser
(
userId SERIAL PRIMARY KEY,
email VARCHAR(100) REFERENCES account(email) ON DELETE CASCADE,
firstName VARCHAR(40),
surname VARCHAR(40),
bio VARCHAR(100),
picturePath VARCHAR(100)
);
CREATE TABLE userGroups
(
groupId SERIAL PRIMARY KEY,
groupName VARCHAR(20),
groupDetails VARCHAR(100),
groupAdminUserId INT
);



CREATE TABLE goals
(
goalId SERIAL PRIMARY KEY,
userId INT REFERENCES siteUser(userId) ON DELETE CASCADE,
groupId INT REFERENCES userGroups(groupId) ON DELETE CASCADE,
goalName VARCHAR(100),
goalType VARCHAR(100),
targetValue FLOAT,
targetDate DATE,
targetType VARCHAR(20),
completed INT,
dateCreated TIMESTAMP,
currentWeight FLOAT,
startTargetValue FLOAT
);

CREATE TABLE exercises
(
exerciseId SERIAL PRIMARY KEY,
userId INT REFERENCES siteUser(userId) ON DELETE CASCADE,
exName VARCHAR(100),
exType VARCHAR(100),
distance FLOAT,
completionTime VARCHAR(20),
caloriesBurnt FLOAT,
exDetails VARCHAR(1000),
met FLOAT, 
dateAdded DATE,
checked INT
);

CREATE TABLE healthOverview
(
healthOverviewId SERIAL PRIMARY KEY,
userId INT REFERENCES siteUser(userId) ON DELETE CASCADE,
height FLOAT,
weight FLOAT,
BMI FLOAT,
conversion VARCHAR(15),
weightChecked INT
);

CREATE TABLE meals
(
mealId SERIAL PRIMARY KEY,
userId INT REFERENCES siteUser(userId) ON DELETE CASCADE,
dateConsumed TIMESTAMP
);

CREATE TABLE consumable 
(
consId SERIAL PRIMARY KEY,
mealId INT REFERENCES meals(mealId) ON DELETE CASCADE,
name VARCHAR(100),
calories FLOAT,
protein FLOAT,
fat FLOAT,
carbs FLOAT
);



CREATE TABLE groupMembers 
(
memberLisTId SERIAL PRIMARY KEY,
groupId INT REFERENCES userGroups(groupId) ON DELETE CASCADE,
userId INT REFERENCES siteUser(userId) ON DELETE CASCADE
);

CREATE TABLE messages
(
messageId SERIAL PRIMARY KEY,
senderId INT,
recieverId INT,
messageContent VARCHAR(1000),
dateSent VARCHAR(20)
);

CREATE TABLE exerciseList
(
exerciseId SERIAL PRIMARY KEY,
exName VARCHAR(100),
met FLOAT
);

CREATE TABLE weightHist
(
histId SERIAL PRIMARY KEY,
userId INT REFERENCES siteUser(userId) ON DELETE CASCADE,
weight FLOAT,
timeAdded TIMESTAMP
);

CREATE TABLE groupMemberGoalWeight
(
entryId SERIAL PRIMARY KEY,
userId INT REFERENCES siteUser(userId) ON DELETE CASCADE,
goalId INT REFERENCES goals(goalId) ON DELETE CASCADE,
creationWeight FLOAT
);

CREATE TABLE notifications
(
notificationId SERIAL PRIMARY KEY,
recieverId INT REFERENCES siteUser(userId) ON DELETE CASCADE,
senderId INT REFERENCES siteUser(userId) ON DELETE CASCADE,
groupId INT REFERENCES userGroups(groupId) ON DELETE CASCADE,
content VARCHAR(100)

);



INSERT INTO exerciseList 
VALUES('-3','Weight Lifting vigorous','6.0');
INSERT INTO exerciseList 
VALUES('-4','Weight Lifting light','3.0');

INSERT INTO exerciseList 
VALUES('-5','Rowing moderate','7.0');
INSERT INTO exerciseList 
VALUES('-6','Rowing vigorous','8.5');

INSERT INTO exerciseList 
VALUES('-7','Dancing','4.8');

INSERT INTO exerciseList 
VALUES('-8','Aerobics light','5.0');
INSERT INTO exerciseList 
VALUES('-9','Aerobics vigorous','7.0');

INSERT INTO exerciseList 
VALUES('-10','Walking moderate','2.8');
INSERT INTO exerciseList 
VALUES('-11','Speed Walking','6.0');

INSERT INTO exerciseList 
VALUES('-12','Jogging','7.0');

INSERT INTO exerciseList 
VALUES('-13','Running','9.0');

INSERT INTO exerciseList 
VALUES('-14','Other - Light','2.0');

INSERT INTO exerciseList 
VALUES('-15','Other - Moderate','4.5');

INSERT INTO exerciseList 
VALUES('-16','Other - Vigorous','7.0');


INSERT INTO exerciseList 
VALUES('0','Not selected','0');



INSERT INTO userGroups 
VALUES('-1','TEST GROUP','Group bio', '1');
INSERT INTO userGroups 
VALUES('-2','TEST GROUP 2','Group bio 2', '1');
INSERT INTO userGroups 
VALUES('-3','CALLUM','Group bio 2', '1');
